<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Image;
use App\Traits\ImageTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AjaxController extends CommonController
{
    use ImageTrait;

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {

            // ini_set('memory_limit', '512M');     // OK - 512MB
            // ini_set('upload_max_filesize', '200M');     // OK - 200MB
            ini_set('memory_limit', 512000000);  // OK - 512MB
            ini_set('upload_max_filesize', 10000000);  // OK - 10MB

            return $next($request);
        });
    }

    public function index(Request $request, $action)
    {
        if (method_exists($this, $action)) {

            return $this->$action($request);
        } else {

            abort(404);
        }
    }

    // Поиск по базе
    public function searching(Request $request) {
        $this->entity = $request->input('db');
        $search = $request->input('s');
        $columns = config("common.{$this->entity}.searching", []);

        $query = DB::table($this->entity)->select($columns);

        if($request->input('byID')) {

            $query->where('id', '=', $search)->whereNull('deleted_at');
        } else {

            $i = 1;
            foreach ($columns as $column) {
                if($i == 1) {
                    $query->where($column, 'like', '%' . $search . '%')->whereNull('deleted_at');
                } else {
                    $query->orWhere($column, 'like', '%' . $search . '%')->whereNull('deleted_at');
                }
                $i++;
            }
        }

        $model = $query->get();

        $response = ['data' => $model, 'entity' => $this->entity, 'type' => '/edit'];

        if($this->entity === 'questionnaires') {
            $response['types'] = config("common.{$this->entity}.types");
        }

        return response()->json($response);
    }

    // Добавление одного фото
    public function uploadImage(Request $request)
    {
        $result = [];

        try {

            if ($request->hasFile('files')) {

                $id = $request->input('id');
                $this->entity = $request->input('entity');
                $columnName = $request->input('columnName');
                $modelFullName = $request->input('modelFullName');

                $model = (new $modelFullName())->find($id);

                if ($model) {

                    $pathImage = "{$this->imagePath}/{$this->entity}/{$id}";

                    $this->miniatures = $this->miniatures($model->getTable());

                    foreach ($this->miniatures as $miniatureKey => $miniatureValue) {

                        $this->{$miniatureKey} = $miniatureValue;
                    }

                    $this->createDirectory($pathImage);

                    $file = $request->file('files')[0];
                    $path = public_path($pathImage);
                    $fileName = $this->createImage($file, $path);
                    $imageFilePath = "/" . trim($pathImage, "/") . "/{$fileName}";

                    $model->{$columnName} = $imageFilePath;
                    $model->save();

                    $result['imageHeader'] = false;

                    if ($model instanceof \App\Models\User && (int) auth()->id() === (int) $id) {

                        $result['imageHeader'] = true;
                    }

                    $result['imageFilePath'] = $imageFilePath;
                    $result['miniature'] = image_path($imageFilePath, 'thumbnail');
                    $result['success'] = true;
                    $result['message'] = __('common.file_successfully_created_and_written_to_database');
                } else {

                    $result['success'] = false;
                    $result['message'] = __('common.entry_missing', ['id' => $id]);
                }
            } else {

                $result['success'] = false;
                $result['message'] = __('common.no_file');
            }
        } catch (\Exception $e) {

            $result['success']  = false;
            $result['message'] = $e->getMessage();
        }

        return response()->json($result);
    }

    // Удаление одного фото
    public function deleteImage(Request $request)
    {
        $result = [];

        try {

            $id = $request->input('id');
            $this->entity = $request->input('entity');
            $columnName = $request->input('columnName');
            $modelFullName = $request->input('modelFullName');
            $imageFilePath = $request->input('imageFilePath');

            $model = (new $modelFullName())->find($id);

            if ($model) {

                $this->miniatures = $this->miniatures($model->getTable());

                if (!empty($imageFilePath) && file_exists(public_path($imageFilePath))) {

                    $this->removeLink($imageFilePath);
                }

                if ($model->methodExists('entities')) {

                    $modelI18n = $model
                        ->entities()
                        ->firstOrNew([
                            'locale' => get_current_locale(),
                        ])
                    ;

                    $modelI18n
                        ->fill(['value' => null])
                        ->save()
                    ;
                } else {

                    $model->{$columnName} = null;
                    $model->save();
                }

                $result['imageHeader'] = false;

                if ($model instanceof \App\Models\User && (int) auth()->id() === (int) $id) {

                    $result['imageHeader'] = true;
                }

                $result['success'] = true;
                $result['message'] = __('common.file_successfully_deleted');
            } else {

                $result['success'] = false;
                $result['message'] = __('common.entry_missing', ['id' => $id]);
            }
        } catch (\Exception $e) {

            $result['success']  = false;
            $result['message'] = $e->getMessage();
        }

        return response()->json($result);
    }

    // Переключение статуса
    public function activeToggle(Request $request)
    {
        $result = [];

        try {

            $id = (int) $request->input('id');
            $isActive = (int) $request->input('is_active');
            $name = $request->input('name');
            $entity = $request->input('entity');
            $modelFullName = $request->input('modelFullName');

            $model = (new $modelFullName())->find($id);

//            $model = DB::table($entity)->where('id', $id)->first();

            if ($model) {

                $model->{$name} = $isActive;
                $model->save();

//                DB::table($entity)->where('id', $id)->update(["{$name}" => $isActive]);

                $result['success'] = true;
                $result['message'] = "Статус у записи с id {$id} успешно изменен!";
            } else {

                $result['success'] = false;
                $result['message'] = "Запись с id {$id} отсутствует!";
            }
        } catch (\Exception $e) {

            $result['success']  = false;
            $result['message'] = $e->getMessage();
        }

        return response()->json($result);
    }

    public function multiExplode ($delimiters, $string) {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);

        return  collect($launch)
            ->filter()
            ->values()
            ->toArray()
        ;
    }

    public function stdClass(Request $request)
    {
        $requestData = $this->requestData($request);
        $stdClass = new \stdClass();

        foreach ($requestData as $key => $requestDatum) {

            $stdClass->{$key} = $requestDatum;
        }

        return $stdClass;
    }

    public function getArticles($mailing)
    {
        return Article::active()->where('date', Carbon::now(env('TIME_ZONE'))->subDays($mailing->last_days)->toDateString())->get();
    }

    // Добавление множества фото
    public function uploadImages(Request $request): \Illuminate\Http\JsonResponse
    {
        $result = [];
        $images = [];

        try {

            if ($request->hasFile('files')) {

                $id = $request->input('id');
                $morphClass = $request->input('morphClass');

                $model = (new $morphClass())->find($id);

                if ($model) {

                    $pathImage = "/{$this->imagePath}/{$model->entity()}/{$id}";

                    $this->miniatures = $this->miniatures($model->getTable());

                    foreach ($this->miniatures as $miniatureKey => $miniatureValue) {

                        $this->{$miniatureKey} = $miniatureValue;
                    }

                    $this->createDirectory($pathImage);

                    foreach ($request->file('files') as $file) {

                        $path = public_path($pathImage);
                        $fileName = $this->createImage($file, $path);
                        $imageFilePath = "/" . trim($pathImage, "/") . "/{$fileName}";

                        $image = $model->images()->firstOrNew([
                            'path' => $imageFilePath,
                        ]);

                        $image->position = 999;
                        $image->save();

                        $images[] = [
                            'id' => $image->id,
                            'position' => $image->position,
                            'path' => asset(image_path("{$imageFilePath}", "thumbnail")),
                        ];
                    }

                    $result['id'] = $id;
                    $result['images'] = $images;
                    $result['morphClass'] = $morphClass;
                    $result['success'] = true;
                    $result['message'] = __('common.file_successfully_created_and_written_to_database');
                } else {

                    $result['success'] = false;
                    $result['message'] = __('common.entry_missing', ['id' => $id]);
                }
            } else {

                $result['success'] = false;
                $result['message'] = __('common.no_file');
            }
        } catch (\Exception $e) {

            $result['success']  = false;
            $result['message'] = $e->getMessage();
        }

        return response()->json($result);
    }

    // Удаление фото множества
    public function removeImages(Request $request): \Illuminate\Http\JsonResponse
    {
        $result = [];

        try {

            $id = $request->input('id');
            $imageId = $request->input('imageId');
            $morphClass = $request->input('morphClass');

            $model = (new $morphClass())->find($id);

            if ($model) {

                $this->miniatures = $this->miniatures($model->getTable());

                $image = $model->images()->where('id', $imageId)->first();

                if ($image && file_exists(public_path("{$image->path}"))) {

                    $this->removeLink($image->path);
                }

                $model->images()->where('id', $image->id)->delete();

                $result['success'] = true;
                $result['message'] = 'Файл успешно удален!';
            } else {

                $result['success'] = false;
                $result['message'] = __('common.entry_missing', ['id' => $id]);
            }

        } catch (\Exception $e) {

            $result['success']  = false;
            $result['message'] = $e->getMessage();
        }

        return response()->json($result);
    }

    // Смена позиции у фото
    public function changePositionImages(Request $request)
    {
        $result = [];

        try {

            $images = $request->input('images');

            foreach ($images as $key => $image) {

                Image::where('id', $image['imageId'])->update(['position' => $key]);
            }

            $result['success'] = true;
            $result['message'] = 'Позиция успешно поменена!';
        } catch (\Exception $e) {

            $result['success']  = false;
            $result['message'] = $e->getMessage();
        }

        return response()->json($result);
    }

    // Получаем миниатюры по названию сущности
    public function miniatures($entity)
    {
        $this->miniatures = config("common.{$entity}.miniatures", []);

        if (empty($this->miniatures)) {

            $this->miniatures = config("common.miniatures", []);
        }

        return $this->miniatures;
    }

    /**
     * Рекомендуемые статьи (начало)
     */

//    public function articles(Request $request)
//    {
//        $search = $request->input('search');
//
//        $articles = Article::whereHas('entities', function ($entity) use ($search) {
//
//            $entity
//                ->where('name', 'LIKE', '%' . $search . '%')
//            ;
//        })
//            ->get()
//            ->map(function ($value){
//
//                return [
//                    'label' => $value->name,
//                    'value' => $value->name,
//                    'id' => $value->id,
//                    'success' => true,
//                ];
//            })
//            ->toArray();
//
//        if (!count($articles)) {
//
//            $articles = [
//                [
//                    'label' => 'Поиск не дал результатов',
//                    'value' => 'Поиск не дал результатов',
//                    'message' => 'Поиск не дал результатов!',
//                    'success' => false,
//                ]
//            ];
//        }
//
//        return response()->json($articles);
//    }
//
//    public function setRecommendedArticle(Request $request)
//    {
//        $articleId = $request->input('article_id');
//        $recommendedId = $request->input('recommended_id');
//
//        $article = Article::find($articleId);
//
//        if ($article) {
//
//            $article->recommended()->attach($recommendedId);
//
//            $result['success'] = true;
//            $result['message'] = "Рекомендованная статья успешно прикреплена!";
//        } else {
//
//            $result['success'] = false;
//            $result['message'] = "Не удалось прикрепить рекомендованную статью!";
//        }
//
//        return response()->json($result);
//    }
//
//    public function deleteRecommendedArticle(Request $request)
//    {
//        $articleId = $request->input('article_id');
//        $recommendedId = $request->input('recommended_id');
//
//        $article = Article::find($articleId);
//
//        if ($article) {
//
//            $article->recommended()->detach($recommendedId);
//
//            $result['success'] = true;
//            $result['message'] = "Рекомендованная статья успешно откреплена!";
//        } else {
//
//            $result['success'] = false;
//            $result['message'] = "Не удалось открепить рекомендованную статью!";
//        }
//
//        return response()->json($result);
//    }

    /**
     * Рекомендуемые статьи (конец)
     */
}

<?php

namespace App\Http\Controllers;

use App\Models\Premise;
use App\Models\RepairPremise;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Route;

class PremiseController extends CommonController
{
    public function repair(Request $request, $id, RepairPremise $repair)
    {
        //Route::back();
        $repairDates = $request->get('repair-dates');
        if(!$repairDates){
            return back()->withErrors(['msg' => 'Введите дату']);
        }
        $date = explode('-' ,$repairDates);
        if(!array_key_exists('1', $date)){
            $date[1] = Carbon::createFromFormat('d.m.Y', $date[0])->addDay()->format('d.m.Y');
        }
        $repair->create([
            'premise_id' => $id,
            'check_in' => $date[0],
            'check_out' => $date[1],
            'is_active' => 1,
        ]);

        return back()->withInput();
    }

    public function update($id, RepairPremise $repair)
    {
        $repair = $repair->where('is_active', 1)->where('id', $id)->first();

        $repair->update([
            'is_active' => 0,
        ]);

        return back()->withInput();
    }
}

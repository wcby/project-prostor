<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\CommonController;
use App\Models\Application;
use App\Models\Premise;
use App\Models\Status;
use Illuminate\Http\Request;

class PremiseController extends CommonController
{

    public function status(Request $request)
    {
        $status = Status::where('slug', $request->get('value'))
            ->first();
        if ($status) {
            $application = Application::find($request->get('id'));
            $application->status_id = $status->id;
            $application->save();
            // Если требуется отправить ответ в формате JSON
            return response()->json(['success' => true, 'message' => $request->get('value')]);
        } else {
            // Если статус с именем "Архив" не найден
            return response()->json(['success' => false, 'message' => 'Статус не найден']);
        }
    }

    public function getPremise(Request $request)
    {
        $category = $request->get('category');
        $premises = Premise::select('id','cost', 'name', 'type_premise_id')
            ->where('category_premise_id',$category)
            ->where('is_active', 1)->get();
        $response = response()->json($premises);
        if(count($response->original) > 0) {
            return $response;
        } else {
            return [];
        }

    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Premise $premise)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Premise $premise)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Premise $premise)
    {
        //
    }
}

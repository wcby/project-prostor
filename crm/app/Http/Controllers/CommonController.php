<?php

namespace App\Http\Controllers;

use App\Extensions\MenuBuilder;
use App\Models\Application;
use App\Models\Status;
use App\Traits\QueryBuilderTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;
use JetBrains\PhpStorm\NoReturn;

class CommonController extends Controller
{
    use QueryBuilderTrait;

    private $collect;

    protected $required;

    protected $directions = ['asc', 'desc'];

    protected $columnDefault = 'id';

    protected $directionDefault = 'desc';

    protected $total = 20;

    protected $entity = '';

    protected $miniatures = [];

    protected $imagePath = "uploads/images";

    public function __construct()
    {
        $this->middleware(function ($request, $next) {

            View::share('user', optional(auth()->user()));
            View::share('sidebar', app('config')->get("app.common.menus.sidebar"));
            View::share('directions', collect($this->directions));
            View::share('columnDefault', $this->columnDefault);
            View::share('directionDefault', $this->directionDefault);

//          тоже присутствует в каждом контроллере
            View::share('entity', $this->entity);
//            стандартные заголовки подменяются в каждой сущности
            $this->setCollect([
                'titleIndex' => config('app.entity.'.$this->entity.'.title_index'),
                'titleRestore' => config('app.entity.'.$this->entity.'.title_restore'),
                'titleCreate' => config('app.entity.'.$this->entity.'.title_create'),
                'titleEdit' => config('app.entity.'.$this->entity.'.title_edit'),
            ]);

            $this->setCollect([
                'breadcrumbs' => [
                    [
                        'name' => config('app.entity.common.main'),
                        'url' => route('start')
                    ],
                ],

            ]);

            return $next($request);
        });
    }

    /**
     *
     * Метод для получения элементов коллекции (для отправки в шаблон)
     *
     * @param null $key
     * @return mixed
     */
    public function getCollect($key = null)
    {
        if (!$this->collect) {

            $this->collect = [];
        }

        if ($key instanceof Collection) {

            return array_intersect_key($this->collect, array_flip($key->toArray()));
        } elseif (is_array($key)) {

            return array_intersect_key($this->collect, array_flip($key));
        } elseif ($key) {

            return $this->collect[$key];
        } else {

            return $this->collect;
        }
    }

    /**
     * Метод для добавления элементов в коллекцию (для отправки в шаблон)
     *
     * @param $key
     * @param null $value
     * @return CommonController
     */
    public function setCollect($key, $value = null)
    {
        if (!$this->collect) {

            $this->collect = [];
        }

        if ($key instanceof Collection) {

            $this->collect = array_merge($this->collect, $key->toArray());
        } elseif (is_array($key)) {

            $this->collect = array_merge($this->collect, $key);
        } else {

            $this->collect[$key] = $value;
        }

        return $this;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function filter(Request $request)
    {
        // Инициализируем переменную
        $path = '';

        // Если есть параметр 'page' в request или 'page' в сессии
        if ($request->has('page') || session()->has("{$this->entity}.page")) {

            // Если есть параметр 'page' в request
            if ($request->has('page')) {

                // Пишем его в сессию
                session(["{$this->entity}.page" => $request->input('page')]);
            }

            if (!$request->filled('pagination')) {

                session(["{$this->entity}.page" => 1]);
            }

            // Если 'page' в сессии есть и не null и начиная со второй
            if (session()->has("{$this->entity}.page") && (int)session("{$this->entity}.page") > 1) {

                // Получаем гет параметр page (если локали нету то гет параметр page стоит первым)
                $path = '?page=' . session("{$this->entity}.page");
            }
        }

        // Если метод отправки get
        if ($request->isMethod('GET')) {

            // и сброс
            if ($request->has('reset')) {

                // Очищаем сессию
                session()->forget("{$this->entity}");
            }

            // Если page присутствует в сессии и не была передана
            if (session()->has("{$this->entity}.page") && !$request->has('page')) {

                // Очищаем page в сессии
                session(["{$this->entity}.page" => null]);
            }

            return redirect(route("{$this->entity}.{$request->input('method')}") . $path);

            // Если метод отправки post
        } else {

            // Удаляем токен из данных
            $data = collect($request->except(['_token']))->filter(function ($value, $key) {

                return !in_array($value, [null, [], '']);
            });

            // Пишем данные в сессию
            session(["{$this->entity}" => $data]);

            return redirect(route("{$this->entity}.{$request->input('method')}") . $path);
        }
    }

    public function setCommonData($model)
    {
        $modelFullName = $model->getMorphClass();

        $searching = config("app.{$this->entity}.fields_type.searching", []);
        $fieldsLinks = config("app.{$this->entity}.fields_type.links", []);
        $fieldsImages = config("app.{$this->entity}.fields_type.images", []);
        $fieldsConfig = config("app.{$this->entity}.fields_type.config", []);
        $fieldsCheckbox = config("app.{$this->entity}.fields_type.checkbox", []);
        $fieldsRelationships = config("app.{$this->entity}.fields_type.relationships", []);
        $fieldsFull = config("app.{$this->entity}.fields_type.full", []);
        $fieldsSorting = config("app.{$this->entity}.fields_type.sorting", []);

        $this
            ->setCollect('modelFullName', $modelFullName)
            ->setCollect('filter', optional(session("{$this->entity}")))
            ->setCollect('fieldsFull', $fieldsFull)
            ->setCollect('searching', $searching)
            ->setCollect('fieldsLinks', $fieldsLinks)
            ->setCollect('fieldsConfig', $fieldsConfig)
            ->setCollect('fieldsImages', $fieldsImages)
            ->setCollect('fieldsSorting', $fieldsSorting)
            ->setCollect('fieldsCheckbox', $fieldsCheckbox)
            ->setCollect('fieldsRelationships', $fieldsRelationships)
            ->setCollect('column', $model->columnSorting)
            ->setCollect('direction', $model->directionSorting);
    }

    public function destroy_entity($model, $ids): array
    {
        try {
            if (is_array($ids) && !empty($ids)) {

                foreach ($ids as $k => $id) {

                    $foundModel = $model->find($id);

                    if ($foundModel) {

                        $foundModel->delete();

                        $result['data'][] = [
                            'success' => true,
                            'id' => $id,
                            'message' => __("common.entry_successfully_deleted", ['id' => $id]),
                        ];
                    } else {

                        $result['data'][] = [
                            'success' => false,
                            'id' => $id,
                            'message' => __("common.entry_missing", ['id' => $id]),
                        ];
                    }
                }
            } else {

                $result['data'][] = [
                    'success' => false,
                    'message' => __("common.no_data_was_sent_for_deletion"),
                ];
            }
        } catch (\Exception $e) {

            $result['data'][] = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }

        return $result;
    }

    public function restore_entity($model, $ids): array
    {

        try {

            if (is_array($ids) && !empty($ids)) {

                foreach ($ids as $k => $id) {

                    $foundModel = $model
                        ->onlyTrashed()
                        ->find($id);

                    if ($foundModel) {

                        $foundModel->restore();

                        $result['data'][] = [
                            'success' => true,
                            'id' => $id,
                            'message' => __("common.entry_successfully_restored", ['id' => $id]),
                        ];
                    } else {

                        $result['data'][] = [
                            'success' => false,
                            'id' => $id,
                            'message' => __("common.entry_missing", ['id' => $id]),
                        ];
                    }
                }
            } else {

                $result['data'][] = [
                    'success' => false,
                    'message' => __("common.no_data_was_sent_for_restoration"),
                ];
            }
        } catch (\Exception $e) {

            $result['data'][] = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
        }

        return $result;
    }

    #[NoReturn] public function notificationNewApplication()
    {
        return Status::where('id',1)->first()->applications;
    }
}

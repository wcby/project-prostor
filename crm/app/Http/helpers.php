<?php

if (!function_exists('make_slug_from_name')) {

    function make_slug_from_name($str)
    {
        $converter = [

            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
            'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
            'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',

        ];

        $str = strtr($str, $converter);
        $str = strtolower($str);
        $str = preg_replace('~[^-a-z0-9_]+~ui', '-', $str);
        $str = trim($str, "-");
        return $str;
    }
}

if (!function_exists('image_path')) {

    function image_path($path, $miniatureName = null)
    {
        if ($miniatureName) {

            if (file_exists(public_path(dirname($path) . "/{$miniatureName}_" . basename($path)))) {

                return dirname($path) . "/{$miniatureName}_" . basename($path);
            } else {

                return $path;
            }
        } else {

            return $path;
        }
    }
}

if (!function_exists('has_cookie')) {

    function has_cookie($name)
    {
        return isset($_COOKIE[$name]) ? true : false;
    }
}

if (!function_exists('get_cookie')) {

    function get_cookie($name, $default = null)
    {
        return isset($_COOKIE[$name]) ? json_decode($_COOKIE[$name], true) : $default;
    }
}

if (!function_exists('set_cookie')) {

    function set_cookie($name, $value = "", $expire = 0, $path = "", $domain = "", $secure = false, $httponly = false)
    {
        setcookie($name, $value, $expire, $path, $domain, $secure, $httponly);
    }
}

if (!function_exists('delete_cookie')) {

    function delete_cookie($name, $value = "", $expire = -1, $path = "", $domain = "", $secure = false, $httponly = false)
    {
        setcookie($name, $value, $expire, $path, $domain, $secure, $httponly);

        return !has_cookie($name);
    }
}

if (!function_exists('url_frontend')) {

    function url_frontend($path = null)
    {
        $urlFrontendConfig = config('common.url_frontend', '');

        $url = implode('/', [
            trim($urlFrontendConfig, '/'),
            trim($path, '/'),
        ]);

        return $url;
    }
}

if (!function_exists('set_permissions')) {

    function set_permissions($model, $output, $actions, $counter = 0)
    {
        $tableName = $model->getTable();

        foreach ($actions as $action) {

            $type = 'core';
            $group = \Illuminate\Support\Str::singular($tableName);

            $name = __("{$tableName}.permissions.name.{$action}", []);
            $description = __("{$tableName}.permissions.descriptions.{$action}");

            $permission = \App\Models\Permission::firstOrNew([
                'type' => $type,
                'group' => $group,
                'action' => $action,
            ]);

            if (!$permission->exists) {

                $permission->name = $name;
                $permission->description = $description;
                $permission->save();
                $counter++;
            }
        }

        if ($counter) {

            $output->writeln("<info>Установлены новые разрешения:</info>  {$counter}");
        } else {

            $output->writeln("<info>Новые разрешения отсутствуют.</info>");
        }
    }
}

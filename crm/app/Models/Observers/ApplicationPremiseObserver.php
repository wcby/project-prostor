<?php

namespace App\Models\Observers;

use App\Models\ApplicationPremise;

class ApplicationPremiseObserver
{
    /**
     * Handle the ApplicationPremise "created" event.
     */
    public function created(ApplicationPremise $applicationPremise): void
    {
        //
    }


    public function updating(ApplicationPremise $applicationPremise): void
    {
        dd($applicationPremise);
    }


    /**
     * Handle the ApplicationPremise "updated" event.
     */
    public function updated(ApplicationPremise $applicationPremise): void
    {
        dd($applicationPremise);
    }

    /**
     * Handle the ApplicationPremise "deleted" event.
     */
    public function deleted(ApplicationPremise $applicationPremise): void
    {
        //
    }

    /**
     * Handle the ApplicationPremise "restored" event.
     */
    public function restored(ApplicationPremise $applicationPremise): void
    {
        //
    }

    /**
     * Handle the ApplicationPremise "force deleted" event.
     */
    public function forceDeleted(ApplicationPremise $applicationPremise): void
    {
        //
    }
}

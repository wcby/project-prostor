<?php

namespace App\Models\Observers;

use App\Models\Application;
use App\Models\Status;

class ApplicationObserver
{
    /**
     * Handle the ApplicationPremise "created" event.
     */
    public function created(Application $application): void
    {
        //
    }


    /**
     * Handle the ApplicationPremise "updated" event.
     */
    public function updated(Application $application): void
    {
        //
    }

    /**
     * Handle the ApplicationPremise "deleted" event.
     */
    public function deleted(Application $application): void
    {
        //
    }

    /**
     * Handle the ApplicationPremise "restored" event.
     */
    public function restored(Application $application): void
    {
        //
    }

    /**
     * Handle the ApplicationPremise "force deleted" event.
     */
    public function forceDeleted(Application $application): void
    {
        //
    }
}


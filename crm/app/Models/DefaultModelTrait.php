<?php

namespace App\Models;

use Illuminate\Support\Str;

trait DefaultModelTrait
{
    public function scopeActive($query): mixed
    {
        return $query->where('is_active', 1);
    }

    public function scopeOrderDesc($query)
    {
        return $query->orderBy('id', 'desc');
    }

    public function scopeOrder($query)
    {
        return $query->orderBy('id', 'asc');
    }

    public function fieldsSelected(): array
    {
        if (has_cookie("fields_{$this->entity()}")) {

            $fields = collect(get_cookie("fields_{$this->entity()}", []))->unique()->toArray();
        } else {

            $fields = collect(config("app.{$this->entity()}.fields_selected_default", []))
                ->unique()
                ->toArray();
        }

        return $fields;
    }

    public function fieldsForShowing(): array
    {
        return collect(config("app.{$this->entity()}.fields_for_showing", []))
            ->unique()
            ->toArray();
    }

    public function getColumnSorting(): void
    {
        $sorting = get_cookie("sorting_{$this->entity()}");

        $column = $sorting['sortColumn'] ?? $this->columnDefault;
    }

    public function getDirectionSorting(): void
    {

    }

    public function entity(): string
    {
        return (string)Str::of($this->getTable())->replace('_', '-');
    }

    public function methodExists($method): bool
    {
        return method_exists($this, $method);
    }

}

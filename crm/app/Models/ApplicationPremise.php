<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicationPremise extends Model
{
    use SoftDeletes;

    protected $table = 'application_premises';
    protected $with = ['premise'];

    protected $fillable = [
        'application_id',   // Заявка
        'premise_id',       // Помещение
    ];

    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\ApplicationPremiseObserver::class);
    }

    public function application()
    {
        return $this->belongsTo(Application::class, 'application_id');
    }

    public function premise()
    {
        return $this->belongsTo(Premise::class, 'premise_id');
    }

    public function getCountDaysAttribute()
    {
        $now = Carbon::now();
        $startOfWeek = $now->startOfWeek()->format('d.m.Y');
        $current = Carbon::parse($this->check_in)->format('d.m.Y');
        $future = Carbon::parse($this->check_out)->format('d.m.Y');
        if ($current > $startOfWeek) {
            $percent = Carbon::pase($this->check_in)->diffInDays(Carbon::parse($this->check_out)) * 100;
        } else if ($current < $startOfWeek) {
            $percent = $now->startOfWeek()->diffInDays(Carbon::parse($this->check_out)) * 100;
        }
        return $percent;
    }

    public function insertDate($date)
    {
        ;
        return Carbon::parse($date)->toDateTimeString();
    }

    public function enterDate($date)
    {
        if ($this->insertDate($this->check_in) <= $this->insertDate($date) &&
            $this->insertDate($this->check_out) >= $this->insertDate($this['date'])) {
            return true;
        }
        return false;
    }

    public function classInterval()
    {
        $now = Carbon::now();
        $startOfWeek = $now->startOfWeek()->toDateTimeString();
        $second = Carbon::parse($this->check_in)->toDateTimeString();
        if ($startOfWeek > $second) {
            return true;
        }
        return false;
    }
}

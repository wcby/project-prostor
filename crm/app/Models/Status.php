<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class Status extends DefaultModel
{
    use HasFactory, Sluggable, SoftDeletes;

    protected $table = 'statuses';

    protected $fillable = [
        'id',               // ID
        'name',             // Название
        'slug',             // Slug
    ];

    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\StatusObserver::class);
    }


    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function setDateAttribute($date)
    {
        $this->attributes['date'] = Carbon::parse($date);
    }

    public function applications()
    {
        return $this->hasMany(Application::class, 'status_id', 'id');
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}

<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\Premise;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class CategoryPremise extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'category_premises';

    protected $fillable = [
        'name',             // Название
        'position',         // Позиция
        'is_active',        // Статус
        'color',            // Цвет
    ];

    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\CategoryPremiseObserver::class);
    }

    public function premises()
    {
        return $this->hasMany(Premise::class, 'category_premise_id');
    }

    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function setDateAttribute($date)
    {
        $this->attributes['date'] = Carbon::parse($date);
    }


    public function getRestoreModels()
    {
        return false;
    }

}

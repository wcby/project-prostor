<?php

namespace App\Models;

use Carbon\Carbon;

use App\Models\Premise;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypePremise extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'type_premises';

    protected $fillable = [
        'id',               // ID
        'name',             // Название
        'is_active',        // Статус
    ];

    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\TypePremiseObserver::class);
    }

    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function setDateAttribute($date)
    {
        $this->attributes['date'] = Carbon::parse($date);
    }

}

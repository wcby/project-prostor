<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;

class DefaultUserModel extends Authenticatable
{
    /**
     * ---------------------------------
     * Поле для сортировки по умолчанию
     * ---------------------------------
     *
     * @var string
     */
    public $columnDefault = 'id';

    /**
     * ----------------------------------------
     * Направление для сортировки по умолчанию
     * ----------------------------------------
     *
     * @var string
     */
    protected $directionDefault = 'desc';

    /**
     * -------------------------------------
     * Количество выбранных записей из базы
     * -------------------------------------
     *
     * @var int
     */
    protected $total = 20;

    /**
     * ---------------------
     * Статус is_active = 1
     * ---------------------
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    /**
     * --------------------------------
     * Поля которые выводятся в списке
     * --------------------------------
     *
     * @return array
     */
    public function fieldsSelected()
    {
        if (has_cookie("fields_{$this->entity()}")) {

            $fields = collect(get_cookie("fields_{$this->entity()}", []))->unique()->toArray();
        } else {

            $fields = collect(config("app.{$this->entity()}.fields_selected_default", []))
                ->unique()
                ->toArray()
            ;
        }

        return $fields;
    }

    /**
     * ------------------
     * Название сущности
     * ------------------
     *
     * @return string
     */
    public function entity()
    {
        return (string) Str::of($this->getTable())->replace('_', '-');
    }

    /**
     * ------------------------------
     * Поля для отображения в списке
     * ------------------------------
     *
     * @return array
     */
    public function fieldsForShowing()
    {
        return collect(config("app.{$this->entity()}.fields_for_showing", []))
            ->unique()
            ->toArray()
        ;
    }

    public function getSorting($key = null)
    {
        return $key ? get_cookie("sorting_{$this->entity()}.{$key}") : get_cookie("sorting_{$this->entity()}");
    }

    public function getColumnSortingAttribute()
    {
        return $this->getSorting('sortColumn') ?? $this->columnDefault;
    }

    public function getDirectionSortingAttribute()
    {
        $sorting = get_cookie("sorting_{$this->entity()}");
        $direction = isset($sorting['sortDirection']) ? $sorting['sortDirection'] : $this->directionDefault;

        return $direction;
    }

    public function methodExists($method)
    {
        return method_exists($this, $method);
    }


    public function getTotalRecordsAttribute()
    {
        return $this->total;
    }
}

<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'payments';

    protected $fillable = [
        'id',               // ID
        'name',             // Название
        'color',            // Цвет
    ];

    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\PaymentObserver::class);
    }

    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function setDateAttribute($date)
    {
        $this->attributes['date'] = Carbon::parse($date);
    }

}

<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'applications';

    protected $fillable = [
        'type_premises_id',
        'client_id',
        'adult',            // Взрослые
        'child',            // Дети
        'check_in',         // Дата заезда
        'check_out',        // Дата выезда
        'comment',          // Комментарий
        'total',            // Всего
        'pay',              // Оплата
        'payment_id',       // Способ Оплата
        'is_view',          // Способ Оплата
        'is_active',        // Способ Оплата
        'is_canceled',      // Способ Оплата
        'status_id',        // Способ Оплата
    ];

    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\ApplicationObserver::class);
    }


    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function payment(): BelongsTo
    {
        return $this->belongsTo(Payment::class, 'payment_id');
    }

    public function premises(): HasMany
    {
        return $this->hasMany(ApplicationPremise::class, 'application_id', 'id');
    }

    public function getCheckInAttribute($date): string
    {
        return Carbon::parse($date)->format('d.m.Y');
    }

    public function getCreatedAtAttribute($date): string
    {
        return Carbon::parse($date)->format('d.m.Y');
    }

    public function getCheckOutAttribute($date): string
    {
        return Carbon::parse($date)->format('d.m.Y');
    }

    public function setCheckInAttribute($date): void
    {
        $this->attributes['check_in'] = Carbon::parse($date)->format('Y-m-d');
    }

    public function setCheckOutAttribute($date): void
    {
        $this->attributes['check_out'] = Carbon::parse($date)->format('Y-m-d');
    }

    public function widthBlock($today, $startDay, $endDay)
    {
        if (strtotime($this->check_in) === strtotime($today)) {
            if (strtotime($this->check_out) <= strtotime($endDay)) {
                return (Carbon::parse($this->check_in)->diffInDays(Carbon::parse($this->check_out))) * 100;
            } else if (strtotime($this->check_out) > strtotime($endDay)) {
                return (Carbon::parse($this->check_in)->diffInDays(Carbon::parse($endDay))+ 0.5) * 100;
            }
        } elseif (strtotime($this->check_in) < strtotime($startDay) && strtotime($startDay) === strtotime($today)) {
            return (Carbon::parse($startDay)->diffInDays(\Carbon\Carbon::parse($this->check_out)) + 0.5) * 100;
        }
    }

    public function getDiffInDaysAttribute(): int
    {
        return Carbon::parse($this->check_in)->diffInDays(Carbon::parse($this->check_out));
    }

    // Фильтрация
    public function filtering()
    {
        $queryBuilder = $this;

        $sessionData = session("{$this->entity()}", []);

        $filter = collect($sessionData)->except('page', 'method', 'sort_column', 'sort_direction')->toArray();

        $filter['used'] = false;

        $fields = collect($sessionData)
            ->keys()
//            ->intersect($this->getFillable()) // убрал исключение по полям
            ->toArray()
        ;
        if (in_array('fio', $fields)) {
            $filter['used'] = true;
            $clients = Client::select('id')
                ->where('name', 'LIKE', '%'.session("{$this->entity()}.fio").'%')
                ->pluck('id');
            $queryBuilder = $queryBuilder->whereIn('client_id', $clients );
        }
        if (in_array('check_in', $fields)) {
            $filter['used'] = true;
            $queryBuilder = $queryBuilder->where('check_in', Carbon::parse(session("{$this->entity()}.check_in")));
        }
        if (in_array('c_premise', $fields) && session("{$this->entity()}.c_premise") != '---') {
            $filter['used'] = true;
            $queryBuilder = $queryBuilder->whereHas('premises.premise', function ($query) {
                $query->where('type_premise_id', session("{$this->entity()}.c_premise"));
            });
        }
        if (in_array('period', $fields) && session("{$this->entity()}.period") != '---') {
            $filter['used'] = true;
            $queryBuilder = $queryBuilder
                ->whereBetween('check_in', [
                    Carbon::now()->subMonths(session("{$this->entity()}.period"))->format('Y-m-d'),
                    Carbon::now()->endOfMonth()
                ]);
        }

        $sessionData['filter'] = $filter;

        // Пишем его в сессию
        session([$this->entity() => $sessionData]);

        return $queryBuilder;
    }
}

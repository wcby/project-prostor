<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\CategoryPremise;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Premise extends Model
{
    use SoftDeletes;

    protected $table = 'premises';
    protected $with = ['category'];

    protected $fillable = [
        'id',                   // ID
        'name',                 // Название
        'type_premise_id',      // Тип
        'category_premise_id',  // Категория
        'cost',                 // Стоимость
        'is_active'             // Статус
    ];

    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\PremiseObserver::class);
    }

    public function category()
    {
        return $this->belongsTo(CategoryPremise::class, 'category_premise_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'category_premise_id');
    }

    public function repairs()
    {
        return $this->hasMany(RepairPremise::class, 'premise_id', 'id');
    }

    // Фильтрация
    public function filtering()
    {
        $queryBuilder = $this;

        $sessionData = session("{$this->entity()}", []);

        $filter = collect($sessionData)->except('page', 'method', 'sort_column', 'sort_direction')->toArray();

        $filter['used'] = false;

        $fields = collect($sessionData)
            ->keys()
            ->intersect($this->getFillable())
            ->toArray()
        ;


        if (in_array('is_active', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('is_active', session("{$this->getTable()}.is_active"));
        }

        $sessionData['filter'] = $filter;

        // Пишем его в сессию
        session([$this->entity() => $sessionData]);

        return $queryBuilder;
    }
}

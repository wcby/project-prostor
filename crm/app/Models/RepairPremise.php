<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RepairPremise extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'repair_premises';

    protected $fillable = [
        'id',                   // ID
        'premise_id',           // Помещение
        'check_in',             // Дата начала ремонта
        'check_out',            // Дата окончаний ремонта
        'is_active'             // Активно
    ];

    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\RepairPremiseObserver::class);
    }

    public function getCheckInAttribute($date): string
    {
        return Carbon::parse($date)->format('d.m.Y');
    }

    public function getCheckOutAttribute($date): string
    {
        return Carbon::parse($date)->format('d.m.Y');
    }

    public function setCheckInAttribute($date): void
    {
        $this->attributes['check_in'] = Carbon::parse($date)->format('Y-m-d');;
    }

    public function setCheckOutAttribute($date): void
    {
        $this->attributes['check_out'] = Carbon::parse($date)->format('Y-m-d');;
    }

    public function premise()
    {
        return $this->belongsTo(Premise::class, 'premise_id');
    }
}

<?php

use App\Http\Controllers\Api\PremiseController;
use App\Http\Controllers\ApplicationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/premise-category', [PremiseController::class, 'getPremise']);
Route::put('/application/{id}/update-status', [ApplicationController::class,'updateStatus']);
Route::put('/application/status', [PremiseController::class,'status']);
Route::put('/application/zips-all', [ApplicationController::class,'zipsAll']);
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



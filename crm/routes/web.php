<?php

use App\Http\Controllers\AjaxController;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\PremiseController;
use App\Http\Controllers\StartController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'auth'], static function ($router) {
    // Authentication Routes...
    $router->get('login', [LoginController::class, 'showLoginForm'])->name('login');
    $router->post('login', [LoginController::class, 'login']);
    $router->any('logout', [LoginController::class, 'logout'])->name('logout');

    // Password Reset Routes...
    // Register the typical reset password routes for an application.
    $router->get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
    $router->post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
    $router->get('password/reset/{token}', [ForgotPasswordController::class, 'showResetForm'])->name('password.reset');
    $router->post('password/reset', [ForgotPasswordController::class, 'reset'])->name('password.update');
});

Route::group(['prefix' => '', 'middleware' => ['auth', 'active']], static function ($router) {
    $router->get('/rooms', [ApplicationController::class, 'rooms'])->name('rooms');
    $router->get('/gazebos', [ApplicationController::class, 'gazebos'])->name('gazebos');
    $router->get('/all', [ApplicationController::class, 'all'])->name('all');

    //Заявки
    $router->get('/', ApplicationController::class)->name('start');
    $router->any('/', ApplicationController::class)->name('applications.__invoke');
    $router->any('/applications/filter', [ApplicationController::class, 'filter'])->name('applications.filter');

    $router->get('/applications?search', [ApplicationController::class, 'search']);
    $router->get('/applications/create', [ApplicationController::class, 'create'])->name('application.create');
    $router->post('/applications/create', [ApplicationController::class, 'store'])->name('application.store');
    $router->get('/applications/{id}', [ApplicationController::class, 'show'])->name('application.show');
    $router->put('/applications/{id}', [ApplicationController::class, 'update'])->name('application.update');
    $router->post('/application/{id}', [ApplicationController::class, 'zip'])->name('application.zip');
    $router->post('/premise/repair/{id}', [PremiseController::class, 'repair'])->name('premise.repair');
    $router->put('/premise/repairOff/{id}', [PremiseController::class, 'update'])->name('repair.off');

    $router->group(['prefix' => 'ajax'], function ($router) {
        $router->any('/{action}', [AjaxController::class, 'index']);
    });
});


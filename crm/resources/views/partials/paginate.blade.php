<ul class="pagination">
    @if ($paginator->onFirstPage())
        <li class="pagination__item">
            <span class="pagination__link disabled">
                <svg class="pagination__arrow pagination__arrow--prev" role="img">
                    <use xlink:href="{{ asset('assets/img/icons.svg#arrow') }}"></use>
                </svg>
            </span>
        </li>
    @else
        <li class="pagination__item">
            <a class="pagination__link" href="{{ $paginator->previousPageUrl() }}" title="Предыдущая страница">
                <svg class="pagination__arrow pagination__arrow--prev" role="img">
                    <use xlink:href="{{ asset('assets/img/icons.svg#arrow') }}"></use>
                </svg>
            </a>
        </li>
    @endif

    @foreach ($elements as $element)
        @if (is_string($element))
            <li class="pagination__item">
                <span class="pagination__link disabled">{{ $element }}</span>
            </li>
        @endif

        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <li class="pagination__item current">
                        <span class="pagination__link">{{ $page }}</span>
                    </li>
                @else
                    <li class="pagination__item">
                        <a class="pagination__link" href="{{ $url }}" title="Страница {{ $page }}">{{ $page }}</a>
                    </li>
                @endif
            @endforeach
        @endif
    @endforeach

    @if ($paginator->hasMorePages())
        <li class="pagination__item">
            <a class="pagination__link" href="{{ $paginator->nextPageUrl() }}" title="Следующая страница">
                <svg class="pagination__arrow pagination__arrow--next" role="img">
                    <use xlink:href="{{ asset('assets/img/icons.svg#arrow') }}"></use>
                </svg>
            </a>
        </li>
    @else
        <li class="pagination__item">
            <span class="pagination__link disabled">
                <svg class="pagination__arrow pagination__arrow--next" role="img">
                    <use xlink:href="{{ asset('assets/img/icons.svg#arrow') }}"></use>
                </svg>
            </span>
        </li>
    @endif
</ul>

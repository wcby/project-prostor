<button class="rooms__day-item-inner dropdown__button" type="button">
    <div class="name">{{ $model->client->name }}</div>
    <div class="price">{{ $model->total }}₽</div>
</button>
<div class="dropdown__menu" data-direction="horizontal">
    <div class="book-info">
        <a href="{{ route('application.show', $model->id) }}">
            <div class="book-info__room">Бронь: № {{$model->id}} / {{$room->premise->category->name}}</div>
        </a>
        <div class="book-info__date">{{ $model->check_in }}&nbsp;— {{ $model->check_out }}</div>
        <div class="book-info__number">Заявка № {{$model->id}} от&nbsp;{{$model->created_at}}</div>
        <hr>
        <div class="book-info__name">{{ $model->client->name }}</div>
        <div class="book-info__phone">{{ $model->client->phone }}</div>
        <div class="book-info__people">
            <div class="book-info__people-item">
                <svg role="img">
                    <use xlink:href="{{ asset('assets/img/icons.svg#smiley') }}"></use>
                </svg>
                2
            </div>
            <div class="book-info__people-item">
                <svg role="img">
                    <use xlink:href="{{ asset('assets/img/icons.svg#baby') }}"></use>
                </svg>
                2
            </div>
        </div>
        <hr>
        <div class="book-info__payment">К&nbsp;оплате: {{ $model->total }}₽</div>
        <div class="book-info__payment book-info__payment--green">Оплачено: {{ $model->pay }}₽</div>
        <div class="form__group-item" style="width: 100%; display:flex; align-items: center; justify-content: space-around;">
            <div class="button button--medium-square button--primary form__status archive"
                 data-value="zip"
                 data-id="{{$model->id}}"
                 onclick="handleClick(this)"
                 title="В Архив"
            >
                <svg class="button__icon button__icon--left" role="img">
                    <use xlink:href="{{ asset('assets/img/icons.svg#archive') }}"></use>
                </svg>
            </div>
            @if($model->status->name != 'Заселены')
                <div class="button button--medium-square button--success form__status shop"
                     data-value="live"
                     data-id="{{$model->id}}"
                     onclick="handleClick(this)"
                     title="Заселить"
                >
                    <svg class="button__icon button__icon--left" role="img">
                        <use xlink:href="{{ asset('assets/img/icons.svg#shop') }}"></use>
                    </svg>
                </div>
            @endif
        </div>
    </div>
</div>
<script>
    function handleClick(element) {
        let value = element.getAttribute('data-value');
        let id = element.getAttribute('data-id');
        let dropdownButton = element.closest('.rooms__day-item ');
        let data = {
            value: value,
            id: id
        };

        fetch('/api/application/status', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(response => response.json())
            .then(data => {
                if(data.message === 'live'){
                    element.style.display = 'none';
                } else if (data.message === 'zip'){
                    dropdownButton.style.display = 'none';
                };
            })
            .catch(error => {
                console.log(error);
            });
    }
</script>

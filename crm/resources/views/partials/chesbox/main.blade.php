@foreach($dates as $key => $date)
    <div data-date="{{$date['date']}}" class="rooms__day
         @if($date['v'] == 1) rooms__day--weekend @endif
         @if($models) dropdown @endif"
    >
        @forelse($models as $model)
            @foreach($model->premises as $room)
                @if($room->premise_id === $premise->id)
                    {{-- Если дата начала периода брони равна дате в календаре --}}
                    @if(strtotime($model->check_in) === strtotime($date['date']))
                        {{--   Если дата окончания периода равна дате окончания периода - т/е/ на 1 день--}}
{{--                        @if(strtotime($model->check_in) === strtotime($model->check_out))--}}
                            {{--                        переписал по типу заявки чтобы беседки были вертикальными --}}
                        @if($model->type_premises_id  === 1)
                            <style>
                                #id{{$model->id}}:before { background: {{$model->payment->color}};}
                                #id{{$model->id}}:after {
                                    background: {{$model->payment->color}};
                                    transform: inherit;
                                    -webkit-transform:inherit;
                                    }
                            </style>

                            @if(\Carbon\Carbon::parse($model->check_in)->diffInDays(\Carbon\Carbon::parse($model->check_out)) > \Carbon\Carbon::parse($model->check_in)->diffInDays(\Carbon\Carbon::parse($dates[6]['date'])))
                                <div
                                    id="id{{$model->id}}"
                                    class="rooms__day-item rooms__day-item--start rooms__day-item--left rooms__day-item--book"
                                    style="width: {{ \Carbon\Carbon::parse($model->check_in)->diffInDays(\Carbon\Carbon::parse($dates[6]['date'])) * 100}}%"
                                >
                            @else
                                <div
                                    id="id{{$model->id}}"
                                    class="rooms__day-item rooms__day-item--start rooms__day-item--left rooms__day-item--book"
                                    style="width: {{ (\Carbon\Carbon::parse($model->check_in)->diffInDays(\Carbon\Carbon::parse($model->check_out)) + 1 ) * 100}}%"
                                >
                            @endif

                        {{--   Если дата окончания периода меньше или равна последней дате шахматки--}}
                        @elseif(strtotime($model->check_out) <= strtotime($dates[array_key_last($dates)]['date']))
                            <style>
                                #id{{$model->id}}:before { background: {{$model->payment->color}};}
                                #id{{$model->id}}:after { background: {{$model->payment->color}};}
                            </style>
                            <div
                                id="id{{$model->id}}"
                                class="rooms__day-item rooms__day-item--center rooms__day-item--book"
                                 style="width:{{ (\Carbon\Carbon::parse($model->check_in)->diffInDays(\Carbon\Carbon::parse($model->check_out)) ) * 100}}%"
                            >
                        @elseif(strtotime($model->check_out) > strtotime($dates[array_key_last($dates)]['date']))
                                    <style>
                                        #id{{$model->id}}:before { background: {{$model->payment->color}};}
                                        #id{{$model->id}}:after { background: {{$model->payment->color}};}
                                    </style>
                            <div
                                id="id{{$model->id}}"
                                class="rooms__day-item rooms__day-item--center rooms__day-item--right rooms__day-item--book"
                                 style="width:{{ (\Carbon\Carbon::parse($model->check_in)->diffInDays(\Carbon\Carbon::parse($dates[6]['date'])) + 0.5 ) * 100}}%"
                            >
                        @endif
                                @includeIf('partials.chesbox.room')
                            </div>
                        {{--             Если начало меньше первой даты календаря--}}
                    @elseif(strtotime($model->check_in) < strtotime($dates[0]['date']) and strtotime($dates[0]['date']) === strtotime($date['date']))
                                    <style>
                                        #id{{$model->id}}:before { background: {{$model->payment->color}};}
                                        #id{{$model->id}}:after { background: {{$model->payment->color}};}
                                    </style>
                        <div
                            id="id{{$model->id}}"
                            class="rooms__day-item rooms__day-item--start rooms__day-item--left rooms__day-item--book"
                             style="width: {{ (\Carbon\Carbon::parse($dates[0]['date'])->diffInDays(\Carbon\Carbon::parse($model->check_out)) + 0.5 ) * 100}}%"
                        >
                            @includeIf('partials.chesbox.room')
                        </div>
                    @endif
                @endif
            @endforeach
        @empty
            <div class="rooms__day"></div>
        @endforelse
        @foreach($repairs as $repair)
            @if($repair->premise_id === $premise->id)
                @if(strtotime($repair->check_in) === strtotime($date['date']))
                    @if(strtotime($repair->check_out) <= strtotime($dates[6]['date']))
                        <div class="rooms__day-item rooms__day-item--repair"
                             style="width:{{ (\Carbon\Carbon::parse($repair->check_in)->diffInDays(\Carbon\Carbon::parse($repair->check_out)) + 1 ) * 100}}%"
                        >
                    @elseif(strtotime($repair->check_out) > strtotime($dates[6]['date']))
                        <div class="rooms__day-item rooms__day-item--repair"
                             style="width:{{ (\Carbon\Carbon::parse($repair->check_in)->diffInDays(\Carbon\Carbon::parse($dates[6]['date'])) + 1 ) * 100}}%"
                        >
                    @endif
                            Ремонт
                            @includeIf('partials.chesbox.repair')
                        </div>
                @elseif(strtotime($repair->check_in) < strtotime($dates[0]['date']) and strtotime($dates[0]['date']) === strtotime($date['date']))
                    <div class="rooms__day-item rooms__day-item--repair"
                         style="width:{{(\Carbon\Carbon::parse($dates[0]['date'])->diffInDays(\Carbon\Carbon::parse($repair->check_out)) + 1 ) * 100}}%"
                         data-check-in="{{$repair->check_in}}" data-check-out="{{$repair->check_out}}">
                        Ремонт
                        @includeIf('partials.chesbox.repair')
                    </div>
                @elseif(strtotime($repair->check_out) < strtotime($dates[6]['date']) and strtotime($dates[0]['date']) === strtotime($date['date']))
                    <div class="rooms__day-item rooms__day-item--repair"
                         style="width:{{(\Carbon\Carbon::parse($dates[0]['date'])->diffInDays(\Carbon\Carbon::parse($repair->check_out)) + 1 ) * 100}}%"
                    data-check-in="{{$repair->check_in}}" data-check-out="{{$repair->check_out}}">
                        Ремонт
                        @includeIf('partials.chesbox.repair')
                    </div>
                @endif
            @endif
        @endforeach
    </div>
@endforeach


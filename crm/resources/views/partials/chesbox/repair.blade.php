<div class="rooms__day-remove">
    <div class="dropdown">
        <button class="rooms__day-remove-button dropdown__button" type="button" aria-label="Завершить ремонт">
            <svg role="img">
                <use xlink:href="{{ asset('assets/img/icons.svg#close') }}"></use>
            </svg>
        </button>
        <div class="dropdown__menu" data-direction="horizontal">
            <div class="dropdown__menu-title">
                Вы&nbsp;уверены, что хотите снять номер с&nbsp;ремонта?</div>
            <form class="form" method="post" action="{{ route('repair.off', $repair->id) }}">
                @csrf
                {{ method_field('PUT') }}
                <button class="button button--large button--primary button--full" type="submit">
                    <span class="button__text button__text--left">Ремонт окончен</span>
                </button>
            </form>
        </div>
    </div>
</div>

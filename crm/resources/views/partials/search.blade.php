<div class="table-list__head">
    <div class="table-list__search">
        <button class="table-list__search-button" type="submit" aria-label="Искать">
            <svg role="img">
                <use xlink:href="{{ asset('assets/img/icons.svg#search') }}"></use>
            </svg>
        </button>
        <div class="table-list__search-text">
            <input class="table-list__search-field" type="search"
                   name="fio"
                   placeholder="Найти по ФИО гостя"
                   value="@if (!!$filter['fio']){{ $filter['fio'] }}@endif">
            <button class="table-list__search-reset" type="button" data-reset="" aria-label="Очистить поле поиска">
                <svg role="img">
                    <use xlink:href="{{ asset('assets/img/icons.svg#close') }}"></use>
                </svg>
            </button>
        </div>
        <div class="table-list__search-sep"></div>
        <div class="table-list__search-date">
            <input class="table-list__search-field" type="text" data-calendar="cancel,apply"
                   name="check_in"
                   value="@if (!!$filter['check_in']){{ $filter['check_in'] }}@endif"
                   placeholder="Дата заезда" readonly>
            <button class="table-list__search-calendar" type="button" data-calendar-icon aria-label="Показать/скрыть календарь">
                <svg role="img">
                    <use xlink:href="{{ asset('assets/img/icons.svg#calendar') }}"></use>
                </svg>
            </button>
        </div>
    </div>
    <div class="table-list__filter dropdown">
        <button class="button button--medium-square button--gradient-blue dropdown__button" type="button" aria-label="Показать/скрыть фильтры">
            <svg class="button__icon button__icon--left" role="img">
                <use xlink:href="{{ asset('assets/img/icons.svg#filter') }}"></use>
            </svg>
        </button>
        <div class="table-list__filter-form dropdown__menu" data-direction="vertical">
            <div class="table-list__filter-title">Фильтр</div>
            <div class="field">
                <label class="field__label">Заявка</label>
                <select class="field__select  field__select--default" name="c_premise" data-relative="data-relative">
                    <option value="---" @if (!$filter['c_premise']){{ 'selected' }}@endif>Все заявки</option>
                    @forelse($category_premise as $c_premise)
                        <option value="{{ $c_premise->id }}"
                            @if ($filter['c_premise'] == $c_premise->id){{ 'selected' }}@endif>
                            {{$c_premise->name}}</option>
                    @empty
                    @endforelse
                </select>
            </div>
            <div class="field">
                <label class="field__label">Период</label>
                <select class="field__select  field__select--default" name="period" data-relative="data-relative">
                    <option value="---" @if (!$filter['period']){{ 'selected' }}@endif>Весь период</option>
                    <option value="12" @if ($filter['period'] == 12){{ 'selected' }}@endif>Весь год</option>
                    <option value="6" @if ($filter['period'] == 6){{ 'selected' }}@endif>Полгода</option>
                    <option value="3" @if ($filter['period'] == 3){{ 'selected' }}@endif>Три месяца</option>
                    <option value="1" @if ($filter['period'] == 1){{ 'selected' }}@endif>Месяц</option>
                </select>
            </div>
            <div class="table-list__filter-buttons">
                <a class="button button--large button--outline" title="Сбросить" href="{{ route("{$entity}.filter") }}?method=__invoke&reset">
                    Сбросить
                </a>
                <button class="button button--large button--primary" type="submit">
                    <span class="button__text button__text--left">Применить</span>
                </button>
            </div>
        </div>
    </div>
</div>

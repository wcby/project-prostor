@foreach($dates as $key => $date)
    <div data-date="{{$date['date']}}" class="rooms__day
                            @if($key === 5 || $key === 6) rooms__day--weekend @endif
                            @if(!$models) dropdown @endif
                            ">
        @if($models)
            @foreach($models as $model)
                @if($model->premise->id === $premise->id)
                    @if($model->enterDate($date['date']))
                        <div data-date="{{$date['date']}}" data-in="{{$model->check_in}}"
                             class="rooms__day-item rooms__day-item--{{$model->classInterval() ? 'start rooms__day-item--left' : 'center'}} rooms__day-item--book"
                             style="width: {{$model->count_days}}%">
                            <button class="rooms__day-item-inner dropdown__button" type="button">
                                <div class="name">{{ $model->application->client->name }}</div>
                                <div class="price">{{ $model->premise->cost }}₽</div>
                            </button>
                            <div class="dropdown__menu" data-direction="horizontal">
                                <div class="book-info">
                                    <div class="book-info__room">Бронь: № 2 / Стандарт</div>
                                    <div class="book-info__date">19 мая — 20 мая (1 ночь)</div>
                                    <div class="book-info__number">Заявка № НО-125 от 01.05.2023</div>
                                    <hr>
                                    <div class="book-info__name">{{ $model->application->client->name }}</div>
                                    <div class="book-info__phone">{{ $model->application->client->phone }}</div>
                                    <div class="book-info__people">
                                        <div class="book-info__people-item">
                                            <svg role="img">
                                                <use xlink:href="{{ asset('assets/img/icons.svg#smiley') }}"></use>
                                            </svg>2
                                        </div>
                                        <div class="book-info__people-item">
                                            <svg role="img">
                                                <use xlink:href="{{ asset('assets/img/icons.svg#baby') }}"></use>
                                            </svg>2
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="book-info__payment">К оплате: {{ $model->application->total }}₽</div>
                                    <div class="book-info__payment book-info__payment--green">Оплачено: {{ $model->application->pay }}₽</div>
                                </div>
                            </div>
                        </div>
                        @break
                    @endif
                @endif
            @endforeach
        @endif
    </div>
@endforeach

@extends('layouts.app')
@section('title'){{ 'Заявки на размещение' }}@endsection
@section('content')
    <div class="wrapper__main">
        <h1 class="reset-mt">Заявки на размещение</h1>
        <ul class="tabs-nav">
            <li class="tabs-nav__item @if($activeStatus === '')current @endif">
                <a class="tabs-nav__link" href="{{ route('start') }}" title="Все">Все</a>
            </li>
            @foreach($statuses as $status)
                <li class="tabs-nav__item @if($status->id === $activeStatus)current @endif">
                    <a class="tabs-nav__link"
                       href="/?status={{$status->id}}"
                       title="{{ $status->name }}">
                        {{ $status->name }}
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="main main--table">
{{--            <form class="table-list" method="get" action="{{ route('start') }}">--}}
            <form class="table-list" action="{{ route("{$entity}.filter") }}" method="post">
                @csrf
                <input type="hidden" name="page" value="{{ request()->input('page') }}">
                <input type="hidden" name="method" value="{{ $redirectRouteName }}">
                @includeIf('partials.search')
                <div class="table-list__body">
                    <table class="table-list__table">
                        @if($models->first())
                            <tr>
                                <th class="table-list__check">
                                    <label class="checkbox checkbox--default">
                                        <input class="checkbox__input" type="checkbox" data-check-all="orders"/>
                                        <span class="checkbox__icon checkbox__icon--default">
                                            <svg role="img">
                                              <use xlink:href="{{ asset('assets/img/icons.svg#check') }}"></use>
                                            </svg>
                                        </span>
                                    </label>
                                </th>
                                <th class="table-list__status">Статус</th>
                                <th class="table-list__name">ФИО гостя</th>
                                <th class="table-list__number">№</th>
                                <th class="table-list__check-in">Заезд</th>
                                <th class="table-list__check-out">Выезд</th>
                                <th class="table-list__people">
                                    <svg role="img"><use xlink:href="{{ asset('assets/img/icons.svg#smiley') }}"></use></svg>
                                </th>
                                <th class="table-list__people">
                                    <svg role="img"><use xlink:href="{{ asset('assets/img/icons.svg#baby') }}"></use></svg>
                                </th>
                            </tr>
                        @endif
                        @forelse($models as $model)
                            <tr>
                                <td class="table-list__check">
                                    <label class="checkbox checkbox--default">
                                        <input class="checkbox__input" type="checkbox" name="orders[]" value="{{ $model->id }}" data-check="orders"/>
                                        <span class="checkbox__icon checkbox__icon--default">
                                        <svg role="img">
                                          <use xlink:href="{{ asset('assets/img/icons.svg#check') }}"></use>
                                        </svg>
                                    </span>
                                    </label>
                                </td>
                                <td class="table-list__status" data-model-id="{{$model->id}}">{{ $model->status->name }}</td>
                                <td class="table-list__name">{{ $model->client->name }}</td>
                                <td class="table-list__number">
                                    @foreach($model->premises as $application)
                                        <div class="table-list__number-item">
                                            <b>{{$application->premise->name ?? ''}}</b>
                                            <span>{{ $application->premise->category?->name ?? '' }}</span>
                                        </div>
                                    @endforeach
                                </td>
                                <td class="table-list__check-in"><b>{{ $model->check_in }}</b></td>
                                <td class="table-list__check-out"><b>{{ $model->check_out }}</b></td>
                                <td class="table-list__people">{{ $model->adult }}</td>
                                <td class="table-list__people">{{ $model->child }}</td>
                                <td class="table-list__payment @if($model->payment->name === 'Оплачено') table-list__payment--red @endif">
                                    {{ $model->payment->name }}
                                </td>
                                <td class="table-list__actions">
                                    <div class="table-list__buttons">
                                        <div class="table-list__buttons-item">
                                            <a class="button button--small-square button--clear-gray"
                                               title="Редактировать" href="{{ route('application.show', $model->id) }}">
                                                <svg class="button__icon button__icon--left" role="img">
                                                    <use xlink:href="{{ asset('assets/img/icons.svg#pen') }}"></use>
                                                </svg>
                                            </a>
                                        </div>
                                        <div class="table-list__buttons-item">
                                            <button class="button button--small-square button--clear-gray"
                                                    type="button" data-tippy-template="order-info-0">
                                                <svg class="button__icon button__icon--left" role="img">
                                                    <use xlink:href="{{ asset('assets/img/icons.svg#info') }}"></use>
                                                </svg>
                                            </button>
                                            <div class="hidden" id="order-info-0">
                                                <div class="tippy-content">
                                                    <div class="tippy-content__head">
                                                        <svg role="img">
                                                            <use xlink:href="{{ asset('assets/img/icons.svg#info') }}"></use>
                                                        </svg>Дополнительная информация
                                                    </div>
                                                    <div class="tippy-content__body">
                                                        {!! $model->comment !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-list__buttons-item">
                                            <button class="button button--small-square button--clear-gray application-zip"
                                                    type="button" data-to-archive="{{$model->id}}" aria-label="Добавить в архив">
                                                <svg class="button__icon button__icon--left" role="img">
                                                    <use xlink:href="{{ asset('assets/img/icons.svg#folder-open') }}"></use>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <div style="display: flex;align-items: center;justify-content: center;">Пока заявок нет</div>
                        @endforelse
                    </table>
                </div>
                <div class="table-list__foot">
                    <button class="table-list__archive sticky" id="zips_all" type="button" data-to-archive>
                        Добавить в архив
                        <span class="table-list__archive-icon">
                            <svg role="img">
                                <use xlink:href="{{ asset('assets/img/icons.svg#archive') }}"></use>
                            </svg>
                        </span>
                    </button>
                    <div class="form form--quantity">Показано
                        <div class="field">
                            <select class="field__select  field__select--small-gray" name="quantity" onChange="this.form.submit()">
                                <option value="10" @if ($filter['quantity'] == 10){{ 'selected' }}@endif>10</option>
                                <option value="15" @if ($filter['quantity'] == 15){{ 'selected' }}@endif>15</option>
                                <option value="25" @if ($filter['quantity'] == 25){{ 'selected' }}@endif>25</option>
                                <option value="50" @if ($filter['quantity'] == 50){{ 'selected' }}@endif>50</option>
                                <option value="100" @if ($filter['quantity'] == 100){{ 'selected' }}@endif>100</option>
                            </select>
                        </div>из  {{ $count }}
                    </div>
                    {{ $models->links('partials.paginate') }}
                    @includeIf('partials.add-premise')
                </div>
            </form>
        </div>
    </div>
        <script>
            document.querySelectorAll('.application-zip').forEach(function(button) {
                button.addEventListener('click', function() {
                    let requestId = this.getAttribute('data-to-archive');
                    let statusElement = document.querySelector('.table-list__status[data-model-id="' + requestId + '"]');

                    // Отправка асинхронного запроса к API
                    fetch('/api/application/' + requestId + '/update-status', {
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            status_name: 'Архив',
                            id: requestId,
                        })
                    })
                    .then(response => response.json())
                    .then(data => {
                        // Обработка ответа от API
                        if (data.success) {
                            statusElement.innerText = 'Архив';
                        }
                    })
                    .catch(error => {
                            // Обработка ошибки отправки запроса
                    });
                });
            });

            const ordersCheckboxes = document.querySelectorAll('.table-list input[type="checkbox"]');
            const zipsAllButton = document.querySelector('#zips_all');
            let statusElement = document.querySelectorAll('.table-list__status[data-model-id]');

            // Обработчик нажатия на кнопку
            zipsAllButton.addEventListener('click', () => {
                // Создание пустого массива для хранения выбранных значений
                const selectedOrders = [];

                // Перебор всех флажков
                ordersCheckboxes.forEach(checkbox => {
                    if (checkbox.checked) { // Проверка состояния флажка
                        selectedOrders.push(checkbox.value); // Добавление значения активного флажка в массив
                    }
                });

                // Отправка массива выбранных значений на сервер
                fetch('/api/application/zips-all', {
                    method: 'PUT',
                    body: JSON.stringify({
                        orders: selectedOrders
                    }),
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                .then(response => response.json())
                .then(data => {
                    if (data.success) {
                        location.reload();
                    }
                })
                .catch(error => {
                    // Обработка ошибок
                    console.error('Ошибка при отправке запроса:', error);
                });
            });
        </script>
@endsection


@extends('layouts.app')
@section('title'){{ $title }} @endsection
@section('content')
    <div class="wrapper__main">
        <h1 class="reset-mt">{{ $title }}</h1>
        <div class="rooms">
            <div class="rooms__head">
                <div class="rooms__today">{{ $currentDate }}
                    <svg role="img">
                        <use xlink:href="{{ asset('assets/img/icons.svg#calendar') }}"></use>
                    </svg>
                </div>
                <div class="rooms__period">
                    <a class="rooms__period-prev" href="?delta={{ $delta - 1 }}" title="Предыдущий период">
                        <svg role="img">
                            <use xlink:href="{{ asset('assets/img/icons.svg#expand') }}"></use>
                        </svg>
                    </a>
                    <div class="rooms__period-current">{{ $weekDateNext }}</div>
                    <a class="rooms__period-next" href="?delta={{ $delta + 1 }}" title="Следующий период">
                        <svg role="img">
                            <use xlink:href="{{ asset('assets/img/icons.svg#expand') }}"></use>
                        </svg>
                    </a>
                </div>
            </div>

            <div class="rooms__body"  style="--rooms-count: {{ $count_days_table }}">
                <div class="rooms__row rooms__row--week">
                    <div class="rooms__category rooms__category--title">Категории</div>
                    @foreach($dates as $key => $date)
                        <div class="rooms__day @if($date['v'] == 1) rooms__day--weekend @endif ">
                            {{ mb_strtoupper($date['date_top']) }}
                        </div>
                    @endforeach
                </div>
                {{--Категории--}}
                @foreach($categories as $category)
                    <div class="rooms__section">
                        {{--вывод строк заголовков категорий и пустых ячеек под них--}}
                        <div class="rooms__row">
                            <div class="rooms__category rooms__category--expand" style="background: {{ $category->color }}">
                                <b>{{ $category->name }}</b>
                                <svg class="expand" role="img">
                                    <use xlink:href="{{ asset('assets/img/icons.svg#expand') }}"></use>
                                </svg>
                            </div>
                            @foreach($dates as $key => $date)
                                <div class="rooms__day @if($date['v'] == 1) rooms__day--weekend @endif ">
                                    <div class="rooms__day-people"></div>
                                    <div class="rooms__day-price"></div>
                                </div>
                            @endforeach

                        </div>

                        {{--Номер категории--}}

                        @foreach($category->premises as $premise)
                            <div class="rooms__row">
                                <div class="rooms__category" style="background: {{ $category->color }}">
                                    {{$premise->name}}
                                    <div class="rooms__category-more dropdown">
                                        <button class="rooms__category-more-button dropdown__button" type="button" aria-label="Поставить на ремонт">
                                            <svg role="img">
                                                <use xlink:href="{{ asset('assets/img/icons.svg#more-vertical') }}"></use>
                                            </svg>
                                        </button>
                                        <div class="dropdown__menu" data-direction="horizontal">
                                            <div class="dropdown__menu-title">Ремонт номера</div>
                                            <form class="form add__repair" method="post" action="{{ route('premise.repair', $premise->id) }}">
                                                @csrf
                                                <div class="form__fields">
                                                    <div class="field">
                                                        <div class="field__group">
                                                            <input class="field__input field__input--icon-right" type="text" data-calendar="" data-range="true" data-relative="true" data-position="bottom right" name="repair-dates" placeholder="22.05.2023-25.05.2023" readonly="readonly" required/>
                                                            <button class="field__button field__button--calendar field__button--right" type="button" data-calendar-icon="data-calendar-icon" aria-label="Показать/скрыть календарь">
                                                                <svg role="img">
                                                                    <use xlink:href="{{ asset('assets/img/icons.svg#calendar') }}"></use>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form__button form__button--small">
                                                    <button class="button button--large button--primary button--full" type="submit">
                                                        <span class="button__text button__text--left">Назначить ремонт</span>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                {{-- Вывод шахматки --}}
                                @includeIf('partials.chesbox.main')
                            </div>
                        @endforeach
                    @endforeach
                </div>
            @includeIf('partials.add-premise')
        </div>
    </div>
@endsection

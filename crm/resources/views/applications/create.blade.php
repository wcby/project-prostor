@extends('layouts.app')
@section('title'){{ 'Новая заявка' }}@endsection
@section('content')
    <div class="wrapper__main">
        <ol class="breadcrumbs">
            <li class="breadcrumbs__item">
                <a class="breadcrumbs__link" href="{{ route('start') }}" title="Заявки">
                    Заявки
                </a>
            </li>
            <li class="breadcrumbs__item">
                <span>Новая заявка @if($type == 1) беседки @else номера@endif</span>
            </li>
        </ol>
        <h1 class="reset-mt">Новая заявка @if($type == 1) беседки @else номера@endif</h1>
        <div class="main main--form">
            <form class="form" method="POST" action="{{ route('application.store') }}">
                <div class="form__section">
                    @if ($errors->any())
                        <div class="alert alert-danger" style="color: red">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                        @csrf
                        <input type="hidden" value="{{ url()->previous() }}" name="prev_url">
                        <input type="hidden" value="{{ $type }}" name="type_premises_id">
                    <div class="form__divider">Гость</div>
                    <div class="grid">
                        <div class="col col--lg-4">
                            <div class="field">
                                <label class="field__label">ФИО клиента*</label>
                                <input class="field__input field__input--default"
                                       name="name" type="text"
                                       placeholder="Иванов Иван Иваныч"
                                       required="required"
                                       value="{{old('name')}}"
                                />
                            </div>
                        </div>
                        <div class="col col--lg-4">
                            <div class="field">
                                <label class="field__label">Телефон*</label>
                                <input class="field__input field__input--default"
                                       name="phone" type="tel"
                                       placeholder="+7 (___) ___-__-__"
                                       required="required"
                                       value="{{old('phone')}}"
                                />
                            </div>
                        </div>
                        <div class="col col--lg-2">
                            <div class="field">
                                <label class="field__label">Взрослые*</label>
                                <input class="field__input field__input--default"
                                       name="adult"
                                       type="number"
                                       placeholder="00"
                                       required="required"
                                       value="{{old('adult')}}"
                                />
                            </div>
                        </div>
                        <div class="col col--lg-2">
                            <div class="field">
                                <label class="field__label">Дети*</label>
                                <input class="field__input field__input--default"
                                       name="child"
                                       type="number"
                                       placeholder="00"
                                       required="required"
                                       value="{{old('child')}}"
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form__section">
                    <div class="form__divider">Аренда</div>
                    <div class="grid">
                        <div class="col col--lg-3">
                            <div class="field">
                                <label class="field__label">Заезд*</label>
                                <div class="field__group">
                                    <input class="field__input field__input--icon-right" type="text"
                                           name="check-in" data-calendar=""
                                           placeholder="22.05.2023" readonly="readonly"
                                           value="{{ old('check-in') }}"
                                    />
                                    <button class="field__button field__button--calendar field__button--right" type="button" data-calendar-icon="data-calendar-icon" aria-label="Показать/скрыть календарь">
                                        <svg role="img"><use xlink:href="{{ asset('assets/img/icons.svg#calendar') }}"></use></svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col col--lg-3">
                            <div class="field">
                                <label class="field__label">Выезд*</label>
                                <div class="field__group">
                                    <input class="field__input field__input--icon-right"
                                           type="text" name="check-out" data-calendar="" placeholder="25.05.2023" readonly="readonly"
                                           value="{{ old('check-out') }}"
                                    />
                                    <button class="field__button field__button--calendar field__button--right" type="button" data-calendar-icon="data-calendar-icon" aria-label="Показать/скрыть календарь">
                                        <svg role="img"><use xlink:href="{{ asset('assets/img/icons.svg#calendar') }}"></use></svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col col--lg-4">
                            <div class="field">
                                <label class="field__label">Статус*</label>
                                <select class="field__select field__select--default" name="status_id" data-relative="true" required>
                                    @foreach($statused as $status)
                                        <option value="{{$status->id}}" @if($status->first) selected @endif>{{$status->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="dynamic-rows">
                        <div class="form__row">
                            <div class="form__row-fields grid">
                                <div class="col col--lg-6">
                                    <div class="field">
                                        <label class="field__label">Категория @if($type == 1) беседки @else номера@endif*</label>
                                        <select class="field__select field__select--default" name="rents[0][category]" onchange="categorySelected(this)" data-relative="true" required>
                                            <option value="" data-placeholder="true">Не назначена</option>
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col col--lg-6">
                                    <div class="field">
                                        <label class="field__label">Номер*</label>
                                        <select class="field__select field__select--default"
                                                name="rents[0][premise]"
                                                data-select="room"
                                                data-cost="0"
                                                data-disabled="true"
                                                disabled required
                                                onchange="roomSelected(this)"
                                                data-relative="true">
                                            <option value="" data-placeholder="true">Не назначен</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <button class="form__row-remove dynamic-rows__remove" type="button" aria-label="Удалить бронь">
                                <svg role="img"><use xlink:href="{{ asset('assets/img/icons.svg#close') }}"></use></svg>
                            </button>
                        </div>
                        <button class="button button--large button--link dynamic-rows__add" type="button">
                            <svg class="button__icon button__icon--left" role="img">
                                <use xlink:href="{{ asset('assets/img/icons.svg#house-tree') }}"></use>
                            </svg>
                            <span class="button__text button__text--left">Добавить аренду</span>
                        </button>
                    </div>
                </div>
                <div class="form__section">
                    <div class="form__divider">Дополнительно</div>
                    <div class="grid">
                        <div class="col col--lg-9">
                            <div class="field">
                                <label class="field__label">Комментарий к заявке</label>
                                <textarea class="field__textarea field__textarea--default"
                                          name="comment"
                                          placeholder="Например “необходимы вилы”">{{ old('comment') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form__section">
                    <div class="form__divider">Сумма и оплата</div>
                    <div class="grid grid--jc-end">
                        <div class="col col--lg-3">
                            <div class="field">
                                <label class="field__label">Общая сумма</label>
                                <input class="field__input field__input--default"
                                       name="total"
                                       type="number"
                                       required
                                       placeholder="0 ₽"
                                       value="{{ old('total') }}"
                                />
                            </div>
                        </div>
                        <div class="col col--lg-3">
                            <div class="field">
                                <label class="field__label">Оплачено</label>
                                <input class="field__input field__input--default"
                                       name="pay" type="number"
                                       placeholder="0 ₽"
                                       value="{{old('pay')}}"
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form__button form__group">
                    <div class="form__group-item">
                        <a class="button button--large button--secondary" title="Назад" href="{{ url()->previous() }}">
                            <svg class="button__icon button__icon--left" role="img">
                                <use xlink:href="{{ asset('assets/img/icons.svg#arrow-long') }}"></use>
                            </svg>
                            <span class="button__text button__text--left">Назад</span>
                        </a>
                    </div>
                    <div class="form__group-item">
                        <button class="button button--large button--primary" type="submit">
                            <svg class="button__icon button__icon--left" role="img">
                                <use xlink:href="{{ asset('assets/img/icons.svg#check') }}"></use>
                            </svg>
                            <span class="button__text button__text--left">Создать заявку</span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

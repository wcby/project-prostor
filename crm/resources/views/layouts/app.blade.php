<!DOCTYPE html>
<html lang="ru">
@include('layouts.blocks._head')
<body>
    <div class="wrapper wrapper--dashboard">
        <!-- Header-->
        @include('layouts.blocks.header')

        <!-- SideBar-->
        @include('layouts.blocks.sidebar')

        <!-- Content-->
        @yield('content')

        <!-- Footer-->
        @include('layouts.blocks.footer')
    </div>
    <script src="{{ asset("assets/js/app.js?ver=" . hash_file('md5', 'assets/js/app.js')) }}"></script>
    <script src="{{ asset("assets/js/form.js?ver=" . hash_file('md5', 'assets/js/form.js')) }}"></script>
</body>
</html>

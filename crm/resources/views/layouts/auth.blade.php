<!DOCTYPE html>
<html lang="ru">
@include('layouts.blocks._head')
<body>
<div class="wrapper">
    @yield('content')
</div>
<script src="{{ asset("assets/js/app.js?ver=" . hash_file('md5', 'assets/js/app.js')) }}"></script>
</body>
</html>

<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link rel="stylesheet" href="{{ asset('assets/css/style.min.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" type="img/png" href="{{ asset('assets/img/favicon.png') }}">
    <link rel="icon" href="{{ asset('assets/img/favicon.svg') }}" type="image/svg+xml">
    <!-- Fonts-->
    <link rel="preload" href="{{ asset('assets/fonts/Inter-Regular.woff2') }}" type="font/woff2" as="font" crossorigin="anonymous">
    <link rel="preload" href="{{ asset('assets/fonts/Inter-Medium.woff2') }}" type="font/woff2" as="font" crossorigin="anonymous">
    <link rel="preload" href="{{ asset('assets/fonts/Inter-SemiBold.woff2') }}" type="font/woff2" as="font" crossorigin="anonymous">
    <link rel="preload" href="{{ asset('assets/fonts/Inter-Bold.woff2') }}" type="font/woff2" as="font" crossorigin="anonymous">

    @if(config('app.env') == 'production')
        <script type="text/javascript" >
            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                m[i].l=1*new Date();
                for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
                k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

            ym(94723009, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
            });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/94723009" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    @endif
</head>

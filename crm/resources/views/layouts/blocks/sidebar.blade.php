<div class="wrapper__sidebar">
    <aside class="sidebar">
        <nav class="sidebar__nav">
            <ul class="sidebar__menu">
                <li class="sidebar__menu-item {{ (request()->is('rooms')) ? 'current' : '' }}">
                    <a class="sidebar__menu-link" href="{{ route('rooms') }}" title="Номера">
                        <svg role="img">
                            <use xlink:href="{{ asset('assets/img/icons.svg#categories') }}"></use>
                        </svg>
                        Номера
                    </a>
                </li>
                <li class="sidebar__menu-item {{ (request()->is('gazebos')) ? 'current' : '' }}">
                    <a class="sidebar__menu-link" href="{{ route('gazebos') }}" title="Беседки">
                        <svg role="img">
                            <use xlink:href="{{ asset('assets/img/icons.svg#shop') }}"></use>
                        </svg>
                        Беседки
                    </a>
                </li>
                <li class="sidebar__menu-item {{ (request()->is('all')) ? 'current' : '' }}">
                    <a class="sidebar__menu-link" href="{{ route('all') }}" title="Все типы">
                        <svg role="img">
                            <use xlink:href="{{ asset('assets/img/icons.svg#shop') }}"></use>
                        </svg>
                        Все типы
                    </a>
                </li>
                <li class="sidebar__menu-item {{ (request()->is('/')) ? 'current' : '' }}">
                    <a class="sidebar__menu-link" href="{{ route('start') }}" title="Заявки">
                        <svg role="img">
                            <use xlink:href="{{ asset('assets/img/icons.svg#file') }}"></use>
                        </svg>
                        Заявки
                    </a>
                </li>
                <li class="sidebar__menu-item">
                    <a class="sidebar__menu-link" href="{{ route('logout') }}" title="Выход">
                        <svg role="img">
                            <use xlink:href="{{ asset('assets/img/icons.svg#logout-door') }}"></use>
                        </svg>
                        Выход
                    </a>
                </li>
            </ul>
        </nav>
    </aside>
</div>

<header>
</header>
<div class="wrapper__header">
    <header class="header">
        <div class="header__left">
            <button class="header__hamburger"
                    type="button"
                    aria-label="Свернуть/развернуть меню"
            >
                <span class="header__hamburger-icon"></span>
            </button>
            <img class="header__logo"
                 src="{{ asset('assets/img/logo.svg') }}"
                 alt="Родные просторы"
                 width="172" height="68"
            >
        </div>
        <div class="header__right">
{{--            <div class="header__notifications dropdown">--}}
{{--                <button class="header__notifications-bell dropdown__button active" type="button">--}}
{{--                    <svg role="img">--}}
{{--                        <use xlink:href="{{ asset('assets/img/icons.svg#bell') }}"></use>--}}
{{--                    </svg>--}}
{{--                    <span class="header__notifications-count">{{ $notifications ? $notifications->count() : 0 }}</span>--}}
{{--                </button>--}}
{{--                @if($notifications)--}}
{{--                    <ul class="dropdown__menu header__notifications-menu" data-direction="vertical">--}}
{{--                        @forelse($notifications as $notification)--}}
{{--                            <li class="dropdown__menu-item">--}}
{{--                                <a class="dropdown__menu-link"--}}
{{--                                   href="{{route('application.show', $notification->id)}}"--}}
{{--                                   title="Новая заявка №{{$notification->id}} от {{$notification->created_at}}">--}}
{{--                                    Новая заявка №{{$notification->id}} от {{$notification->created_at}}--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        @empty--}}
{{--                        @endforelse--}}
{{--                    </ul>--}}
{{--                @endif--}}
{{--            </div>--}}
            <div class="header__profile dropdown">
                <button class="dropdown__button header__profile-name">
                    {{ auth()->user()->email }}
                    <svg role="img">
                        <use xlink:href="{{ asset('assets/img/icons.svg#more-vertical') }}"></use>
                    </svg>
                </button>
                <ul class="dropdown__menu" data-direction="vertical">
                    <li class="dropdown__menu-item">
                        <a class="dropdown__menu-link"
                           href="{{ route('logout') }}"
                           title="Выйти из личного кабинета"
                        >
                            <svg role="img">
                                <use xlink:href="{{ asset('assets/img/icons.svg#logout') }}"></use>
                            </svg>
                            Выйти
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
</div>

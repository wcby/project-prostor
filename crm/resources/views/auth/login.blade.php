@extends('layouts.auth')
@section('title'){{ 'Вход в личный кабинет' }}@endsection
@section('content')
    <div class="auth">
        <div class="auth__left">
            <form class="auth__form form" method="POST" action="{{ route('login') }}">
                @csrf
                <img class="auth__logo" src="{{ asset('assets/img/logo.svg') }}" alt="Родные просторы" width="172" height="68">
                <div class="auth__title">
                    <h2 class="reset">Вход в личный кабинет</h2>
                </div>
                <div class="field">
                    <label class="field__label">Логин*</label>
                    <input class="field__input field__input--default @error('email') field__input--error @enderror"
                           name="email"
                           type="email"
                           placeholder="example@example.com"
                           required="required"
                    />
                    @error('email')
                        <span class="field__message field__message--error" role="alert">{{ $message }}</span>
                    @enderror
                </div>
                <div class="field">
                    <label class="field__label">Пароль*</label>
                    <div class="field__group">
                        <input class="field__input field__input--icon-right @error('password') field__input--error @enderror"
                               name="password"
                               type="password"
                               placeholder="Введите пароль"
                               required="required"
                        />
                        <button class="field__button field__button--right"
                                type="button"
                                data-toggle-password=""
                                aria-label="Показать/скрыть пароль"
                        >
                            <svg role="img">
                                <use xlink:href="{{ asset('assets/img/icons.svg#eye') }}"></use>
                            </svg>
                        </button>
                        @error('password')
                            <span class="form__message form__message--error" role="alert">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form__group form__group--small">
                    <div class="form__group-item">
                        <label class="checkbox checkbox--small">
                            <input class="checkbox__input"
                                   type="checkbox"
                                {{ old('remember') ? 'checked' : '' }}
                            />
                            <span class="checkbox__icon checkbox__icon--small">
                                <svg role="img">
                                  <use xlink:href="{{ asset('assets/img/icons.svg#check') }}"></use>
                                </svg>
                            </span>
                            <span class="checkbox__text">Запомнить меня</span>
                        </label>
                    </div>
                    <div class="form__group-item">
                        <a class="form__link form__link--small"
                           href="#password-modal"
                           title="Я забыл(а) пароль"
                           data-modal
                        >
                            Я забыл (а) пароль
                        </a>
                    </div>
                </div>
                <div class="form__button">
                    <button class="button button--large button--primary button--full" type="submit">
                        <span class="button__text button__text--left">Войти в кабинет</span>
                    </button>
                </div>
            </form>
            <div class="modal modal--center" id="password-modal">
                <div class="modal__overlay" data-modal-close=""></div>
                <div class="modal__inner modal__inner--center modal__inner--small">
                    <div class="modal__head">
                        <div class="modal__title">Сбросить пароль</div>
                        <button class="modal__close modal__close--center" aria-label="Закрыть модальное окно" data-modal-close=""></button>
                    </div>
                    <div class="modal__body">
                        <div class="modal__text">Введите свой email и мы вышлем ссылку на сброс пароля.</div>
                        <form class="form" method="POSt" action="#">
                            @csrf
                            <div class="field">
                                <label class="field__label">Email*</label>
                                <input class="field__input field__input--default"
                                       name="email"
                                       type="email"
                                       placeholder="example@example.com"
                                       required="required"
                                />
                            </div>
                            <div class="form__button">
                                <button class="button button--large button--primary button--full" type="submit">
                                    <span class="button__text button__text--left">Выслать ссылку</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="auth__right"></div>
    </div>
@endsection

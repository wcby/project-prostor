document.addEventListener('DOMContentLoaded', function() {
    var forms = document.getElementsByClassName('add__repair');

    Array.prototype.forEach.call(forms, function(form) {
        form.addEventListener('submit', function(e) {
            var repairDatesField = form.querySelector('input[name="repair-dates"]');

            if (repairDatesField.value === '') {
                e.preventDefault(); // Останавливаем отправку формы
                repairDatesField.focus();
            }
        })
    })
})

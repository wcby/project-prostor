<?php

namespace App\Listeners;

use App\Events\TelegramSendMessageEvents;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class SendMessageListener
{
    /**
     * @throws GuzzleException
     */
    public function handle(TelegramSendMessageEvents $event): void
    {
        $url = config('app.api.telegram.url');
        $group_id = config('app.api.telegram.group_id');

        // Отправка сообщения в API
        $client = new Client();
        $client->post($url, [
            'json' => [
                'msg' => $event->message,
                'id' => $group_id,
            ],
        ]);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class Gazebo extends Model
{
    use SoftDeletes, DefaultModel, HasFactory;

    protected $table = 'gazebos';

    protected $fillable = [
        'id',
        'name',                 // Название
        'count',                // Количество мест
        'seat_cost',            // Доплата за место
        'price',                // Цена
        'type_premise_id',      // тип помещения
        'is_active'
    ];

    public function typePremise(): BelongsTo
    {
        return $this->belongsTo(TypePremise::class, 'type_premise_id');
    }

    public function objectAdditionals(): HasMany
    {
        return $this->hasMany(ObjectsAdditional::class, 'gazebo_id', 'id');
    }

    public function additionType() : HasMany
    {
        return $this->hasMany(AdditionalType::class, 'type_premise_id', 'type_premise_id');
    }

    public function category(): HasOne
    {
        return $this->hasOne(CategoryPremise::class,'id', 'category_premise_id');
    }

    public function images()
    {
        return $this
            ->hasMany(Image::class, 'parent_id', 'id')
            ->where('type', 'gazebo')
            ->orderBy('position');
    }

    public function getImageAttribute()
    {
        return Cache::remember($this->cacheKey() . 'image_attribute',  config('app.cache_time_short'), function () {
            return $this->images->isNotEmpty() ? $this->images->first()->path : null;
        });
    }
}

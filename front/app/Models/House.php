<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;


class House extends Model
{
    use SoftDeletes, DefaultModel, HasFactory;

    protected $table = 'houses';

    protected $fillable = [
        'id',
        'name',                 // Название
        'count',                // количество мест
        'price',                // стоимость
        'category_premise_id',  // категория
        'type_premise_id',      // тип помещения
        'is_active'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function images()
    {
        return $this
            ->hasMany(Image::class, 'parent_id', 'id')
            ->where('type', 'house')
            ->orderBy('position', 'asc')
            ;
    }

    public function getImageAttribute()
    {
        return Cache::remember($this->cacheKey() . 'image_attribute',  config('app.cache_time_short'), function () {
            return $this->images->isNotEmpty() ? $this->images->first()->path : null;
        });
    }

    public function category(): HasOne
    {
        return $this->hasOne(CategoryPremise::class,'id', 'category_premise_id');
    }

    public function objectAdditionals(): HasMany
    {
        return $this->hasMany(ObjectsAdditional::class, 'house_id', 'id');
    }

    public function additionType() : HasMany
    {
        return $this->hasMany(AdditionalType::class, 'type_premise_id', 'type_premise_id');
    }
}

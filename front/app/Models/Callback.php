<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Callback extends Model
{
    use SoftDeletes, DefaultModel, HasFactory;

    protected $table = 'callbacks';

    protected $fillable = [
        'id',
        'title',                // Название заявки
        'name',                 // ФИО
        'phone',                // Телефон
        'type',                 // Тип
        'adult',                // Взрослые
        'children',                // Дети
        'check_in',             // Заезд
        'check_out',            // Выезд
        'status'                // Статус
    ];


    public function getCheckInAttribute($date): string
    {
        return Carbon::parse($date)->format('d.m.Y');
    }

    public function getCreatedAtAttribute($date): string
    {
        return Carbon::parse($date)->format('d.m.Y');
    }

    public function getCheckOutAttribute($date): string
    {
        return Carbon::parse($date)->format('d.m.Y');
    }

    public function setCheckInAttribute($date): void
    {
        $this->attributes['check_in'] = Carbon::parse($date)->format('Y-m-d');
    }

    public function setCheckOutAttribute($date): void
    {
        $this->attributes['check_out'] = Carbon::parse($date)->format('Y-m-d');
    }

}

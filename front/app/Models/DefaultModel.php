<?php

namespace App\Models;

trait DefaultModel
{

    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }

    public function scopeDefault($query)
    {
        return $query->where('is_default', 1);
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use SoftDeletes;

    protected $table = 'sliders';

    protected $fillable = [
        'id',
        'name',
        'position',
        'is_active'
    ];

    protected $with = [
        'images',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected static function boot()
    {
        parent::boot();
    }

    public function images()
    {
        return $this
            ->hasMany(Image::class, 'parent_id', 'id')
            ->where('type', 'slider')
            ->orderBy('position');
    }

    public function getImageAttribute()
    {
        return $this->images->first();
    }

    public function getImagePathAttribute()
    {
        return $this->image?->path;
    }

}


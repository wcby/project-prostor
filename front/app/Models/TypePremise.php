<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypePremise extends Model
{
    use SoftDeletes, DefaultModel, HasFactory;

    protected $table = 'type_premises';

    protected $fillable = [
        'id',               // ID
        'name',             // Название
        'is_active',        // Статус
    ];
}

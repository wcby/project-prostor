<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdditionalType extends Model
{
    use SoftDeletes, DefaultModel, HasFactory;

    protected $table = 'additional_types';

    protected $fillable = [
        'id',
        'name',              // Название
        'type_premise_id',   // тип помещения
        'is_active'
    ];

    public function additionals(): HasMany
    {
        return $this->hasMany(Additional::class, 'additional_type_id');
    }

    public function typePremise(): BelongsTo
    {
        return $this->belongsTo(TypePremise::class, 'type_premise_id');
    }
}

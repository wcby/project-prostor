<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DownPhoto extends Model
{
    use SoftDeletes;

    protected $table = 'down_photos';

    protected $fillable = [
        'id',
        'name',
        'position',
        'count',
        'description',
        'is_active'
    ];

    protected $with = [
        'images',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected static function boot()
    {
        parent::boot();
    }

    public function images()
    {
        return $this
            ->hasMany(Image::class, 'parent_id', 'id')
            ->where('type', 'down_photos')
            ->orderBy('position');
    }

    public function getImageAttribute()
    {
        return $this->images->first();
    }

    public function getImagePathAttribute()
    {
        return $this->image?->path;
    }

}


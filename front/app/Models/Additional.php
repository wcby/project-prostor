<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Additional extends Model
{
    use SoftDeletes, DefaultModel, HasFactory;

    protected $table = 'additionals';

    protected $fillable = [
        'id',
        'name',                 // Название
        'icon',                 // Иконка
        'additional_type_id',   // тип доп услуги
        'is_active'
    ];

    public function additionalType(): BelongsTo
    {
        return $this->belongsTo(AdditionalType::class, 'additional_type_id', 'id');
    }
}

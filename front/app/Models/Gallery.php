<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class Gallery extends Model
{
    use SoftDeletes, DefaultModel, HasFactory;

    protected $table = 'galleries';

    protected $fillable = [
        'id',
        'path',              // Путь
        'type_gallery_id',   // Тип галереи
        'is_active'
    ];

    protected $with = [
        'images',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function images()
    {
        return $this
            ->hasMany(Image::class, 'parent_id', 'id')
            ->where('type', 'gallery')
            ->orderBy('position', 'asc')
            ;
    }

    public function getImageAttribute()
    {
        return Cache::remember($this->cacheKey() . 'image_attribute',  config('app.cache_time_short'), function () {
            return $this->images->isNotEmpty() ? $this->images->first()->path : null;
        });
    }
}

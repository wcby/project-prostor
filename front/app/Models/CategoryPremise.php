<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryPremise extends Model
{
    use SoftDeletes, DefaultModel, HasFactory;

    protected $table = 'category_premises';

    protected $fillable = [
        'name',             // Название
        'is_active',        // Статус
        'color',            // Цвет
    ];

    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function setDateAttribute($date)
    {
        $this->attributes['date'] = Carbon::parse($date);
    }

    public function filtering(): CategoryPremise
    {
        $queryBuilder = $this;

        $sessionData = session($this->entity(), []);

        $filter = collect($sessionData)->except('page', 'method', 'sort_column', 'sort_direction')->toArray();

        $filter['used'] = false;

        $fields = collect($sessionData)
            ->keys()
            ->intersect($this->getFillable())
            ->toArray();

        if (in_array('id', $fields, true)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder::where('id', session("{$this->getTable()}.id"));
        }

        if (in_array('name', $fields, true)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder::where('name', 'like', '%' . session("{$this->getTable()}.name") . '%');
        }

        if (in_array('is_active', $fields, true)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder::where('is_active', session("{$this->getTable()}.is_active"));
        }

        $sessionData['filter'] = $filter;

        // Пишем его в сессию
        session([$this->entity() => $sessionData]);

        return $queryBuilder;
    }

    public function getRestoreModels()
    {
        return false;
    }

}

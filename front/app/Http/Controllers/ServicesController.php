<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServicesController extends CommonController
{
    public function services()
    {
        $entity = 'services';
        $meta = $this->metaTags($entity);
        return view('services.index', compact('meta'));
    }

    public function fishing()
    {
        $entity = 'fishing';
        $meta = $this->metaTags($entity);
        return view('services.fishing', compact('meta'));
    }

    public function birthday()
    {
        $entity = 'birthday';
        $meta = $this->metaTags($entity);
        return view('services.birthday', compact('meta'));
    }

    public function family()
    {
        $entity = 'family';
        $meta = $this->metaTags($entity);
        return view('services.family-holiday', compact('meta'));
    }

    public function pool()
    {
        $entity = 'pool';
        $meta = $this->metaTags($entity);
        return view('services.pool', compact('meta'));
    }

    public function corporate()
    {
        $entity = 'corporate';
        $meta = $this->metaTags($entity);
        return view('services.corporate', compact('meta'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Collection;

class CommonController extends Controller
{

    /**
     *
     * Метод для получения элементов коллекции (для отправки в шаблон)
     *
     * @param null $key
     * @return mixed
     */

    public function metaTags($entity): Array
    {
        return $meta = [
            'title' => config('app.meta.'.$entity.'.meta_title'),
            'description' => config('app.meta.'.$entity.'.meta_description'),
            'keywords' => config('app.meta.'.$entity.'.meta_keywords'),
        ];
    }

    public function getCollect($key = null)
    {
        if (!$this->collect) {

            $this->collect = [];
        }

        if ($key instanceof Collection) {

            return array_intersect_key($this->collect, array_flip($key->toArray()));
        } elseif (is_array($key)) {

            return array_intersect_key($this->collect, array_flip($key));
        } elseif ($key) {

            return $this->collect[$key];
        } else {

            return $this->collect;
        }
    }

    /**
     * Метод для добавления элементов в коллекцию (для отправки в шаблон)
     *
     * @param $key
     * @param null $value
     * @return CommonController
     */
    public function setCollect($key, $value = null)
    {
        if (!$this->collect) {

            $this->collect = [];
        }

        if ($key instanceof Collection) {

            $this->collect = array_merge($this->collect, $key->toArray());
        } elseif (is_array($key)) {

            $this->collect = array_merge($this->collect, $key);
        } else {

            $this->collect[$key] = $value;
        }

        return $this;
    }

    public function getBreadcrumbsData()
    {
        // Здесь вы можете возвратить массив данных для breadcrumbs
        return [
            ['name' => 'Главная', 'url' => '/'],
            ['name' => 'Домики', 'url' => '/houses'],
            ['name' => 'Беседки', 'url' => '/gazebos'],
            ['name' => 'Услуги', 'url' => '/services'],
            ['name' => 'Правила проживания', 'url' => '/rules'],
            ['name' => 'Галерея', 'url' => '/gallery'],
            ['name' => 'Контакты', 'url' => '/contacts'],
            ['name' => 'Бронирование', 'url' => '/booking'],
            ['name' => 'Страница не найдена', 'url' => '/404'],
            ['name' => 'Рыбная ловля', 'url' => '/services/fishing'],
            ['name' => 'Открытый бассейн', 'url' => '/services/pool'],
            ['name' => 'Семейный отдых', 'url' => '/services/family-holiday'],
            ['name' => 'День рождения', 'url' => '/services/birthday'],
            ['name' => 'Корпоративы на природе', 'url' => '/services/corporate'],
        ];
    }
}

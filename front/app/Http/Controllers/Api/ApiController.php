<?php

namespace App\Http\Controllers\Api;

use App\Events\TelegramSendMessageEvents;
use App\Models\Callback;
use Illuminate\Http\Request;

class ApiController
{
    public $header = [
        'Content-Type' => 'application/json; charset=UTF-8',
        'charset' => 'utf-8'
    ];

    public function callback(Request $request, Callback $model)
    {
        $url = url('/');
        $data = $request->all();
        $msg = "
        Запрос на обратный звонок с сайта bazarp.ru
        Имя: ".$data['name']."
        Телефон:".$data['phone']."
        ";
        $model->create([
            'title' => 'Обратный звонок',
            'name' => $data['name'],
            'phone' => $data['phone'],
        ]);
        event(new TelegramSendMessageEvents($msg));
        return response()->json(['success' => true,'message' => 'Форма успешно отправлена!']);
    }

    public function booking(Request $request, Callback $model)
    {
        $url = url('/');
        $data = $request->all();
        $checkIn = $data['checkIn'];
        $checkOut = $data['checkOut'];
        $houseType = $data['type-house'] ?? "Домик";
        $adults = $data['adults'] ?? 0;
        $children = $data['children'] ?? 0;
        $msg = "
        Заявка на бронирование с сайта bazarp.ru
        Имя: ".$data['name']."
        Телефон:".$data['phone']."
        Тип помещения:".$houseType."
        Дата заезда:". $checkIn ."
        Дата выезда:". $checkOut ."
        Взрослые:". $adults ."
        Дети:". $children ."
        ";
        $model->create([
            'title' => 'Бронирование',
            'name' => $data['name'],
            'phone' => $data['phone'],
            'type' => $houseType,
            'adult' => $adults,
            'children' => $children,
            'check_in' => $checkIn,
            'check_out' => $checkOut,
        ]);
        event(new TelegramSendMessageEvents($msg));
        return response()->json(['success' => true,'message' => 'Форма успешно отправлена!']);
    }


}

<?php

namespace App\Http\Controllers;

use App\Models\Gazebo;
use App\Models\House;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PremisesController extends CommonController
{

    private function getBreadcrumbs()
    {
        $breadcrumbs = [];

        // Добавьте первую крошку (например, главную страницу)
        $breadcrumbs[] = [
            'label' => 'Главная',
            'url' => route('home')
        ];

        // Добавьте остальные крошки в зависимости от текущего пути запроса
        $path = Request::path();
        $pathSegments = explode('/', $path);
        $currentUrl = '';

        foreach ($pathSegments as $segment) {
            $currentUrl .= '/' . $segment;
            $breadcrumbs[] = [
                'label' => ucfirst($segment), // Преобразуйте название сегмента в заглавную букву, если нужно
                'url' => $currentUrl
            ];
        }

        return $breadcrumbs;
    }

    public function houses()
    {
        $models = House::active()->get();
        $entity = 'houses';
        $meta = $this->metaTags($entity);
        return view('premises.houses', compact('models', 'meta'));
    }

    public function gazebos()
    {
        $models = Gazebo::active()->get();
        $entity = 'gazebos';
        $meta = $this->metaTags($entity);
        return view('premises.gazebos', compact('models', 'meta'));
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\DownPhoto;
use App\Models\Gallery;
use App\Models\Slider;
use Illuminate\Http\Request;

class StartController extends CommonController
{
    public function start()
    {
        $entity = 'home';
        $meta = $this->metaTags($entity);
        $sliders = Slider::where('is_active', 1)->orderBy('position', 'ASC')->get();
        $down_photos = DownPhoto::where('is_active', 1)->orderBy('position', 'ASC')->get();
        return view('site.home', compact('meta', 'sliders', 'down_photos'));
    }

    public function gallery()
    {
        $models = Gallery::active()->get();
        $entity = 'gallery';
        $meta = $this->metaTags($entity);
        return view('site.gallery', compact('models', 'meta'));
    }

    public function booking()
    {
        $entity = 'booking';
        $class = 'static';
        $meta = $this->metaTags($entity);
        return view('site.booking', compact('meta', 'class'));
    }

    public function rules()
    {
        $entity = 'rules';
        $meta = $this->metaTags($entity);
        return view('site.rules', compact('meta'));
    }

    public function contacts()
    {
        $entity = 'contacts';
        $meta = $this->metaTags($entity);
        return view('site.contacts', compact('meta'));
    }
}

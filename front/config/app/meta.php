<?php
return [
    'home' => [
        'meta_title' => 'Родные просторы',
        'meta_description' => 'Главная страница Родные просторы',
        'meta_keywords' => 'отдых, проживание, рыбалка',
    ],
    'gallery' => [
        'meta_title' => 'Галерея',
        'meta_description' => '',
        'meta_keywords' => '',
    ],
    'booking' => [
        'meta_title' => 'Бронь',
        'meta_description' => '',
        'meta_keywords' => '',
    ],
    'rules' => [
        'meta_title' => 'Правила проживания',
        'meta_description' => '',
        'meta_keywords' => '',
    ],
    'contacts' => [
        'meta_title' => 'Контакты',
        'meta_description' => '',
        'meta_keywords' => '',
    ],
    'services' => [
        'meta_title' => 'Услуги',
        'meta_description' => '',
        'meta_keywords' => '',
    ],
    'fishing' => [
        'meta_title' => 'Услуги — Рыбная ловля',
        'meta_description' => '',
        'meta_keywords' => '',
    ],
    'birthday' => [
        'meta_title' => 'Услуги — День рождения',
        'meta_description' => '',
        'meta_keywords' => '',
    ],
    'family' => [
        'meta_title' => 'Услуги — Семейный отдых',
        'meta_description' => '',
        'meta_keywords' => '',
    ],
    'pool' => [
        'meta_title' => 'Услуги — Открытый бассейн',
        'meta_description' => '',
        'meta_keywords' => '',
    ],
    'corporate' => [
        'meta_title' => 'Услуги — Корпоративы на природе',
        'meta_description' => '',
        'meta_keywords' => '',
    ],
    'houses' => [
        'meta_title' => 'Домики',
        'meta_description' => '',
        'meta_keywords' => '',
    ],
    'gazebos' => [
        'meta_title' => 'Беседки',
        'meta_description' => '',
        'meta_keywords' => '',
    ],
    '404' => [
        'meta_title' => 'Страница не найдена',
    ],
];

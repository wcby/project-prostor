@extends('layouts.app')
@section('content')
    <section class="section section--pb">
        <div class="booking container grid">
            <form class="form form--booking booking__form ajax-form" id="booking-form" action="{{ route('bookingSend') }}" method="post">
                <h2 class="reset-mt booking__title">Бронирование</h2>
                <h5 class="booking__subtitle">Выберите даты проживания</h5>
                <div class="booking__field">
                    <div class="field">
                        <button class="field__button field__button--calendar field__button--right" type="button" data-calendar-icon="data-calendar-icon" aria-label="Показать/скрыть календарь">
                            <svg role="img">
                                <use xlink:href="{{ asset('assets/img/icons.svg#calendar') }}"></use>
                            </svg>
                        </button>
                        <input class="field__input field__input--default  field__input--icon-right field__input--gray" id="booking-check-in" type="text" name="rents[check-in]" readonly="readonly" data-calendar=""/>
                        <label class="field__label field__label--gray" for="booking-check-in">Дата заезда</label>
                    </div>
                    <div class="field">
                        <button class="field__button field__button--calendar field__button--right" type="button" data-calendar-icon="data-calendar-icon" aria-label="Показать/скрыть календарь">
                            <svg role="img">
                                <use xlink:href="{{ asset('assets/img/icons.svg#calendar') }}"></use>
                            </svg>
                        </button>
                        <input class="field__input field__input--default  field__input--icon-right field__input--gray" id="booking-check-out" type="text" name="rents[check-out]" readonly="readonly" data-calendar=""/>
                        <label class="field__label field__label--gray" for="booking-check-out">Дата выезда</label>
                    </div>
                </div>
                <h5 class="booking__subtitle">Размещение</h5>
                <div class="booking__radios">
                    <label class="radio">
                        <input class="radio__input" type="radio" name="type-house" value="Домик" checked><span class="radio__text">Домик</span>
                    </label>
                    <label class="radio">
                        <input class="radio__input" type="radio" name="type-house" value="Беседка"><span class="radio__text">Беседка</span>
                    </label>
                </div>
                <div class="booking__quantity">
                    <div class="booking__quantity-item"><span class="booking__quantity-label">Взрослые</span>
                        <div class="quantity">
                            <button class="quantity__button quantity__button--minus" type="button" aria-label="Уменьшить количество">-</button>
                            <input class="quantity__input" id="booking-adults" type="number" value="2" name="booking-quantity-adults" min="0" max="10"/>
                            <button class="quantity__button quantity__button--plus" type="button" aria-label="Увеличить количество">+</button>
                        </div>
                    </div>
                    <div class="booking__quantity-item"><span class="booking__quantity-label">Дети</span>
                        <div class="quantity">
                            <button class="quantity__button quantity__button--minus" type="button" aria-label="Уменьшить количество" disabled="disabled">-</button>
                            <input class="quantity__input" id="booking-children" type="number" value="0" name="booking-quantity-children" min="0" max="10"/>
                            <button class="quantity__button quantity__button--plus" type="button" aria-label="Увеличить количество">+</button>
                        </div>
                    </div>
                </div>
                <h5 class="booking__subtitle">Контактные данные</h5>
                <div class="booking__field">
                    <div class="field">
                        <input class="field__input field__input--default  field__input--gray" id="booking-name" type="text" name="name" required="required"/>
                        <label class="field__label field__label--gray" for="booking-name">ФИО*</label>
                    </div>
                    <div class="field">
                        <input class="field__input field__input--default  field__input--gray" id="booking-phone" type="tel" name="phone" required="required"/>
                        <label class="field__label field__label--gray" for="booking-phone">Телефон*</label>
                    </div>
                </div>
                <button class="button button--large button--green button--full booking__button" type="submit">
                    <span class="button__text">Отправить заявку</span>
                </button>
            </form>
        </div>
    </section>
@endsection

@extends('layouts.app')
@section('content')
    @include('partials.top',
    [
        'image' => 'contacts-bg',
        'title' => 'Контакты',
        'top_text' => null
    ])
    <section class="section contacts">
        <div class="container">
            <div class="section__title section--pt contacts__grid">
                <div class="contacts__title">
                    <h2 class="reset">Контакты</h2>
                    <div class="section__wave">
                        <img class="section__wave-img" src="{{ asset('assets/img/waves.svg') }}" alt="decor" width="50" height="30" loading="lazy">
                    </div>
                </div>
                <div class="contacts__contact">
                    <div class="offers__item offers__item--contacts"><span class="offers__item-img">
                    <svg class="offers__item-icon" role="img">
                      <use xlink:href="assets/img/icons.svg?v=1695281016298#phone"></use>
                    </svg></span></div>
                    <div class="contacts__contact-content">
                        <h5 class="h5 reset-mt contacts__contact-title">Бронирование</h5><a class="contacts__contact-link" href="tel:89883429798" title="Позвонить нам">+7 988 342-97-98</a><a class="contacts__contact-link contacts__contact-link--underline" href="#callback-modal" title="Заказать обратный звонок" data-modal>Заказать звонок</a>
                    </div>
                </div>
                <div class="contacts__contact">
                    <div class="offers__item offers__item--contacts">
                        <span class="offers__item-img">
                            <svg class="offers__item-icon" role="img">
                              <use xlink:href="{{ asset('assets/img/icons.svg#envelope') }}"></use>
                            </svg>
                        </span>
                    </div>
                    <div class="contacts__contact-content">
                        <h5 class="h5 reset-mt contacts__contact-title">По всем вопросам</h5>
                        <a class="contacts__contact-link" href="mailto:r.prostori@mail.ru" title="Написать нам">r.prostori@mail.ru</a>
                    </div>
                </div>
                <div class="contacts__contact">
                    <div class="offers__item offers__item--contacts">
                        <span class="offers__item-img">
                            <svg class="offers__item-icon" role="img">
                              <use xlink:href="{{ asset('assets/img/icons.svg#envelope') }}"></use>
                            </svg>
                        </span>
                    </div>
                    <div class="contacts__contact-content">
                        <h5 class="h5 reset-mt contacts__contact-title">Адрес</h5>
                        <div class="contacts__contact-text">Краснодарский край<br>г-к Анапа,<br>станица Гостагаевская</div>
                    </div>
                </div>
            </div>
            <div class="section--pb grid">
                <div class="contacts__content">
                    <h2 class="h2 reset-mt">Как нас найти</h2>
                    <div class="section__subtitle">
                        <div class="section__subtitle-icon">
                            <svg role="img">
                                <use xlink:href="{{ asset('assets/img/icons.svg#compass') }}"></use>
                            </svg>
                        </div><span class="section__subtitle-text">GPS координаты: 45.027679, 37.445618</span>
                    </div>
                    <h6 class="h6">При движении со стороны г-к Анапа:</h6>
                    <p>База находится на въезде в ст. Гостагаевскую.<br>Сразу после дорожного знака, обозначающего въезд в населенный пункт, следует перекресток, на котором необходимо выполнить поворот налево и еще раз налево, таким образом Вы попадете на грунтовую дорогу ведущую к базе.</p>
                    <h6 class="h6">При движении со стороны г. Краснодара</h6>
                    <p class="reset-m">Необходимо продолжить движение через ст. Гостагаевскую по дорожным указателям ведущим на г-к Анапа. На выезде из станицы (на последнем перекрестке) необходимо выполнить поворот направо, а потом налево, таким образом Вы попадете на грунтовую дорогу ведущую к базе.</p>
                </div>
                <div class="contacts__map" id="map"></div>
            </div>
        </div>
    </section>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru-RU&amp;onload=mapInit" async></script>
    <script>
        function mapInit() {
            let coords = [45.027679, 37.445618],
                map = new ymaps.Map(
                    'map',
                    {
                        center: coords,
                        zoom: 16,
                        controls: ['zoomControl'],
                    },
                    {
                        searchControlProvider: 'yandex#search',
                    },
                ),
                placemark = new ymaps.Placemark(
                    coords,
                    {},
                    {
                        iconLayout: 'default#image',
                        iconImageHref: 'assets/img/marker.svg',
                        iconImageSize: [66, 88],
                        iconImageOffset: [-33, -88],
                    },
                )
            map.geoObjects.add(placemark)
            map.behaviors.disable('scrollZoom')
        }
    </script>
@endsection

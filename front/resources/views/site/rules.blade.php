@extends('layouts.app')
@section('content')
    @include('partials.top',
    [
        'image' => 'rules-bg',
        'title' => 'Правила проживания',
        'top_text' => null
    ])
    <section class="section section--pb">
        <div class="container">
            <div class="grid">
                <div class="rules">
                    <article class="rules__article">
                        <div class="section__subtitle">
                            <div class="section__subtitle-icon">
                                <svg role="img">
                                    <use xlink:href="{{ asset("/assets/img/icons.svg?v=1693225697529#!") }}"></use>
                                </svg>
                            </div><span class="section__subtitle-text">Гость, находящийся на территории рыболовной базы, обязан соблюдать настоящий перечень правил</span>
                        </div>
                        <ol class="rules__list">
                            <li>Все услуги, оказываемые на территории базы отдыха, предоставляются согласно прейскуранту цен на оказываемые услуги и графику работы.</li>
                            <li>График работы круглосуточный</li>
                            <li>При входе на территорию взимается плата согласно прейскуранту цен</li>
                            <li>Оплатив услуги, гость соглашается с настоящим перечнем правил и условий</li>
                            <li>После оплаты, гостю выдается квитанция об оплате, которая должна быть им сохранена до окончания пребывания на территории</li>
                            <li>Приобретая входной билет, гость не приобретает конкретное место вдоль береговой линии на территории базы отдыха. Приобретая входной билет, гость должен самостоятельно убедиться в наличии подходящего для него места ловли на береговой линии</li>
                            <li>Пребывание на территории базы ограниченно временем и зависит от приобретенного входного билета</li>
                            <li>Входной билет подразделяется на два типа: билет на дневное время и билет на сутки</li>
                            <li>Входной билет на дневное время действует с 08:00 часов утра до 20:00 часов вечера того дня, в который он приобретен. В зимний период времени, действие дневного билета сокращается до 18:00 часов</li>
                            <li>Входной билет на сутки предоставляет возможность нахождения на территории базы отдыха 24 часа с момента его приобретения при условии, что билет был приобретен не ранее 08:00 часов утра и не позднее 20:00 часов вечера, в противном случае действие билета будет сокращаться в зависимости от времени его приобретения:
                                <div class="rules__example"><span class="rules__example-title">Пример № 1</span><span>В случае если билет приобретен в 09:00 часов утра, либо 18:00 часов вечера, то его действие распространяется до 09:00 часов утра и 18:00 часов вечера следующего дня, то есть 24 часа</span></div>
                                <div class="rules__example"><span class="rules__example-title">Пример № 2</span><span>В случае если билет приобретен в 21:00 час, либо 05:00 утра, то его действие распространяется до ближайших 20:00 часов</span></div>
                            </li>
                            <li>Парковка автомобилей осуществляется в строго отведённых для того местах</li>
                            <li>Въезд на территорию базы, для разгрузки (загрузки) осуществляется по строго отведенной для этого дороге и предоставляется сроком до 10 минут. Подъезд на автомобиле к воде, а также передвижение на автомобиле по траве, запрещены. После осуществления разгрузки автомобиль необходимо перегнать на парковку</li>
                            <li>Бронировать номера для проживания нужно заранее, расселение происходит после оплаты</li>
                            <li>Гости могут разместиться на базе отдыха, предъявив любой документ, удостоверяющий личность (паспорт или водительское удостоверение), и заполнив анкету на заселение. Договор о предоставлении основных услуг базой отдыха и договор материальной ответственности отдыхающего считается заключенными после оформления документов на проживание (заполнение анкеты, регистрации)</li>
                            <li>База отдыха предназначена для временного размещения и отдыха в течение согласованного срока</li>
                            <li>Заселение на базе отдыха осуществляется с 13:00 часов, выселение до 11:00 часов</li>
                            <li>При заселение гости проводят визуальную оценку дома/номера на предмет чистоты, комплектности и исправности бытовых приборов. При наличии замечаний гости имеют право высказать требования администратору для исправления замечаний, либо потребовать заселения в иной аналогичный дом/номер</li>
                            <li>По окончании срока проживания, гости обязаны освободить дом/номер и предъявить его к сдаче администрации в надлежащем состоянии. Администратор проводит визуальную оценку на предмет порчи имущества, если таковое имеет место быть.</li>
                            <li>В случае небрежной эксплуатации оборудования, приведшей к его полной или частичной утери, порчи, потери имущества базы отдыха гости обязаны компенсировать нанесенный ущерб, по максимальной рыночной стоимости</li>
                            <li id="rule-21">На территории ведется видеонаблюдение</li>
                            <li>Администрация базы отдыха не несет ответственность за сохранность личных вещей клиентов, оставленных без присмотра</li>
                            <li>По окончании пребывания на территории базы гостю необходимо представить выловленную им рыбу контролеру для взвешивания. В случае возникновения подозрений в том, что намеренно скрыл выловленную им сверх нормы рыбу с целью ее хищения, контролер имеет право вызвать сотрудников полиции, либо группу быстрого реагировании частной охранной организации</li>
                        </ol>
                    </article>
                    <article class="rules__article">
                        <div class="section__subtitle">
                            <div class="section__subtitle-icon">
                                <svg role="img">
                                    <use xlink:href="{{ asset("/assets/img/icons.svg?v=1693225697529#!") }}"></use>
                                </svg>
                            </div><span class="section__subtitle-text">На территории базы отдыха действуют следующие ограничения</span>
                        </div>
                        <ol class="rules__list">
                            <li>Не допускается рассадка рыбаков напротив и в радиусе 3-х метров от беседки, находящейся у водоема, за исключение случаев ее аренды</li>
                            <li>На одного рыбака должно быть не более 3-х удилищ, либо 1-го ротпода</li>
                            <li>Занимаемое место на берегу одним рыбаком не должно превышать 5-7 метров</li>
                            <li>Ловля рыбы осуществляется вдоль дамбы и береговой линии, прилегающей к территории базы</li>
                            <li>Ловля рыбы в местах нереста (берега, покрытые камышом) запрещена</li>
                        </ol>
                    </article>
                    <article class="rules__article">
                        <div class="section__subtitle">
                            <div class="section__subtitle-icon">
                                <svg role="img">
                                    <use xlink:href=""{{ asset("/assets/img/icons.svg?v=1693225697529#!") }}"></use>
                                </svg>
                            </div><span class="section__subtitle-text">Правила и условия рыбной ловли</span>
                        </div>
                        <ol class="rules__list">
                            <li>
                                В случае если клиентом поймана рыба (Карп, Сазан) свыше 5 кг он ОБЯЗАН отпустить ее обратно в водоем, гостю необходимо сообщить
                                об этом администратору, чтобы администратор убедился в том, что рыба не травмирована и может быть отпущена
                            </li>
                            <li>Хранение (Карпа, Сазана) свыше 5 кг разрешается только в случае спортивной ловли в специальных карповых мешках</li>
                            <li>В случае если травма рыбы несовместима с ее дальнейшей жизнью в водоеме, гость ОБЯЗАН оплатить стоимость рыбы в соответствии с прейскурантом цен</li>
                        </ol>
                    </article>
                    <article class="rules__article">
                        <div class="section__subtitle">
                            <div class="section__subtitle-icon">
                                <svg role="img">
                                    <use xlink:href=""{{ asset("/assets/img/icons.svg?v=1693225697529#!") }}"></use>
                                </svg>
                            </div><span class="section__subtitle-text">На территории базы отдыха запрещается</span>
                        </div>
                        <ol class="rules__list">
                            <li>Вход на территорию с животными</li>
                            <li>Купание в озере</li>
                            <li>Вылавливать малька карпа, белого амура, толстолобика и иной не сорной рыбы</li>
                            <li>Выпускать выловленную рыбу обратно в воду из садка (за исключением случаев спортивной ловли специально подготовленными для этого метода ловли снастями)</li>
                            <li>Использовать не традиционные (браконьерские) орудия ловли</li>
                            <li>Разводить огонь на земле (траве)</li>
                            <li>Нарушать общественный порядок, выражаться нецензурной бранью, находиться в состоянии опьянения, оскорбляющем человеческое достоинство и общественную нравственность</li>
                        </ol>
                    </article>
                    <article class="rules__article">
                        <div class="section__subtitle">
                            <div class="section__subtitle-icon">
                                <svg role="img">
                                    <use xlink:href=""{{ asset("/assets/img/icons.svg?v=1693225697529#!") }}"></use>
                                </svg>
                            </div><span class="section__subtitle-text">Отдыхающие обязаны соблюдать морально-этические нормы</span>
                        </div>
                        <ol class="rules__list">
                            <li>Воздерживаться в местах массового отдыха гостей от чрезмерного употребления алкоголя</li>
                            <li>Не мешать другим гостям отдыхать</li>
                            <li>Оплачивать стоимость рыбы в соответствии с прейскурантом цен</li>
                            <li>Беречь мебель, технику, постельное белье, предметы интерьера, приусадебные постройки, спортивный инвентарь и декоративные насаждения на территории базы отдыха</li>
                        </ol>
                    </article>
                    <div class="section__subtitle reset">
                        <div class="section__subtitle-icon">
                            <svg role="img">
                                <use xlink:href=""{{ asset("/assets/img/icons.svg?v=1693225697529#!") }}"></use>
                            </svg>
                        </div>
                        <span class="section__subtitle-text">
                            При несоблюдении вышеперечисленных правил, администрация базы отдыха оставляет за собой право выселить гостей и удалить с территории в кратчайшие сроки без возврата оплаты
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

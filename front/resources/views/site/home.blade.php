@extends('layouts.app')
@section('content')
    <div class="full-slider swiper">
        <div class="full-slider__content container">
            <div class="full-slider__logo">
                <img class="full-slider__logo-img" src="{{ asset("assets/img/full-slider-logo.svg") }}" alt="Родные просторы" width="132" height="86" loading="lazy">
            </div>
            <h1 class="reset full-slider__title">Идеальное место <br>для отдыха на лоне природы</h1>
        </div>
        <div class="swiper-wrapper">
            @forelse($sliders as $slider)
                <div class="swiper-slide full-slider__item">
                    <picture class="full-slider__picture">
                        <source srcset="{{ asset(image_path("{$slider->image->path}", "large")) }}" media="(min-width: 768px)">
                        <img class="full-slider__img" src="{{ asset(image_path("{$slider->image->path}", "large")) }}" alt="{{ $slider->name }}" width="720" height="1200">
                    </picture>
                </div>
            @empty
                <div class="swiper-slide full-slider__item">
                    <picture class="full-slider__picture">
                        <source srcset="{{ asset("assets/img/full-slide-1.jpg") }}" media="(min-width: 768px)">
                        <img class="full-slider__img" src="{{ asset("assets/img/full-slide-1--xs.jpg") }}" alt="Идеальное место для отдыха на лоне природы" width="720" height="1200">
                    </picture>
                </div>
            @endforelse
        </div>
        <div class="swiper-control swiper-control--absolute">
            <div class="swiper-pagination"></div>
        </div>
    </div>
    <section class="section section--pt">
        <div class="container">
            <div class="section__title section__title--flex section__title--mb">
                <h2 class="reset section__title-text">Безупречный отдых</h2>
                <div class="section__wave-text">
                    <div class="section__wave"><img class="section__wave-img" src="{{ asset("assets/img/waves.svg") }}" alt="decor" width="50" height="30" loading="lazy"></div>
                    <p class="section__text">Отдых — это не обязательно изнурительное путешествие в далекие края, это то, что реально принесет Вам удовольствие, поможет восстановить душевное равновесие, поправить здоровье, или просто приятно провести досуг. Работа базы отдыха ориентирована на полное удовлетворение потребности в активном и спокойном, интересном и увлекательном отдыхе для каждого.</p>
                </div>
            </div>
            <div class="offers"><a class="offers__item" href="{{route('premises.houses')}}#guest-houses" title="Гостевые дома"><span class="offers__item-img">
                  <svg class="offers__item-icon" role="img">
                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#green-house") }}"></use>
                  </svg></span><span class="h5 offers__item-text">Гостевые дома</span></a><a class="offers__item" href="{{route('premises.houses')}}#houses" title="Домики"><span class="offers__item-img">
                  <svg class="offers__item-icon" role="img">
                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#house") }}"></use>
                  </svg></span><span class="h5 offers__item-text">Домики</span></a><a class="offers__item" href="{{route('premises.gazebos')}}" title="Беседки"><span class="offers__item-img">
                  <svg class="offers__item-icon" role="img">
                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#gazebo") }}"></use>
                  </svg></span><span class="h5 offers__item-text">Беседки</span></a><a class="offers__item" href="{{route('services.fishing')}}" title="Рыбалка"><span class="offers__item-img">
                  <svg class="offers__item-icon" role="img">
                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#fishing") }}"></use>
                  </svg></span><span class="h5 offers__item-text">Рыбалка</span></a><a class="offers__item" href="{{route('services.pool')}}" title="Бассейн"><span class="offers__item-img">
                  <svg class="offers__item-icon" role="img">
                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#swimming-pool") }}"></use>
                  </svg></span><span class="h5 offers__item-text">Бассейн</span></a>
            </div>
        </div>
    </section>
    <section class="section section--p advantages">
        <div class="container">
            <h2 class="reset-mt text-center">Вас ждет роскошь комфортного отдыха</h2>
        </div>
        <picture class="advantages__picture">
            <source srcset="{{ asset("assets/img/advantage.jpg") }}" media="(min-width: 768px)"><img class="advantages__img" src="{{ asset("assets/img/advantage--xs.jpg") }}" alt="Вас ждет роскошь комфортного отдыхаы" width="1074" height="504">
        </picture>
        <div class="advantages__content">
            <div class="advantages__item"><span class="advantages__item-num">1000+</span><span class="advantages__item-text">Довольных гостей возвращаются к нам каждый сезон</span></div>
            <div class="advantages__item"><span class="advantages__item-num">10+</span><span class="advantages__item-text">Уютных беседок на любой вкус</span></div>
            <div class="advantages__item"><span class="advantages__item-num">15+</span><span class="advantages__item-text">Гостиничных номеров различных категорий</span></div>
            <div class="advantages__item"><span class="advantages__item-num">33</span><span class="advantages__item-text">М2 площадь нового бассейна на территории</span></div>
            <div class="section__wave-text">
                <div class="section__wave"><img class="section__wave-img" src="{{ asset("assets/img/waves.svg") }}" alt="decor" width="50" height="30" loading="lazy"></div>
                <div class="section__text">
                    <p>Наша база — отдыха идеальное место для проведения семейного отпуска. Доступные цены и удобный формат бронирования, современные развлечения и домашний уют никого не оставят равнодушным.
                        <br> Удобное расположение в станице Гостагаевской города-курорта Анапа.</p>
                    <a href="#" title="Забронировать номер"><span>Забронировать номер</span>
                        <svg role="img">
                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow") }}"></use>
                        </svg></a>
                </div>
            </div>
        </div>
    </section>
    <section class="section section--default section--ovh section--blue">
        <div class="container">
            <div class="section__title section__title--flex section__title--mb">
                <h2 class="reset section__title-text">Комфортное размещение в объятиях природы доступно для каждого</h2>
                <div class="section__wave-text">
                    <div class="section__text">
                        <p>Мы предлагаем различные варианты проживания, чтобы наши гости чувствовали себя комфортно и наслаждались отдыхом</p>
                        <a class="slider__link-all" href="{{route('premises.houses')}}#houses" title="Все варианты размещения"><span>Все варианты размещения</span>
                            <svg role="img">
                                <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow") }}"></use>
                            </svg></a>
                    </div>
                </div>
            </div>
            <div class="slider tabs" data-tabs>
                <ul class="tabs__links tabs__links--full" data-tabs-links>
                    @forelse($down_photos as $photo)
                    <li class="tabs__links-item">
                        <a class="tabs__links-link current" href="#slider-{{$photo->id}}" data-link="{{route('premises.houses')}}#{{ $photo->id }}" title="{{ $photo->name }}">{{ $photo->name }}</a></li>
                    @empty

                    @endforelse
                </ul>
                <div class="tabs__items" data-tabs-items>
                    @forelse($down_photos as $photos)
                        <div class="tabs__item @if($loop->first)current @endif" id="slider-{{$photos->id}}">
                            <div class="slider__swiper swiper">
                                <div class="swiper-wrapper">
                                    @forelse($photos->images as $image)
                                        <div class="swiper-slide slider__item">
                                            <a class="slider__item-picture" href="{{route('premises.houses')}}#houses-0" title="{{ $photos->description }}">
                                                <img class="slider__item-img" src="{{ asset(image_path("{$image->path}", "large")) }}" alt="{{ $photos->description }}" width="4096" height="2731">
                                            </a>
                                            <div class="slider__item-desc"><span class="slider__item-type">{{ $photos->description }}</span>
                                                <div class="slider__item-value">
                                                    <svg class="slider__item-icon" role="img">
                                                        <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                    </svg><span class="slider__item-residents">{{ $photos->count }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    @empty

                                    @endforelse
                                </div>
                                <div class="slider__scrollbar">
                                    <div class="swiper-scrollbar"></div>
                                </div>
                                <div class="slider__control">
                                    <button class="swiper-button swiper-button--prev swiper-button--round" type="button" aria-label="Предыдущий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                    <button class="swiper-button swiper-button--next swiper-button--round" type="button" aria-label="Следующий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    @empty

                    @endforelse
                </div>
            </div>
        </div>
    </section>
    <section class="section section--default">
        <div class="container">
            <div class="section__title section__title--flex section__title--mb">
                <h2 class="reset section__title-text">От семейного отдыха до корпоратива</h2>
                <div class="section__wave-text">
                    <div class="section__wave"><img class="section__wave-img" src="{{ asset("assets/img/waves.svg") }}" alt="decor" width="50" height="30" loading="lazy"></div>
                    <p class="section__text">Наша команда поможет с организацией любого вашего праздника, будь то День рождения, корпоративный выезд или свадьба</p>
                </div>
            </div>
            <div class="offers">
                <a class="offers__item" href="{{route('services.family')}}" title="Семейный отдых">
                    <span class="offers__item-img">
                        <svg class="offers__item-icon" role="img">
                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#family") }}"></use>
                        </svg>
                    </span>
                    <span class="h5 offers__item-text">Семейный отдых</span>
                </a>
                <a class="offers__item" href="{{route('services.birthday')}}" title="День рождения">
                    <span class="offers__item-img">
                        <svg class="offers__item-icon" role="img">
                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#birthday-cake") }}"></use>
                        </svg>
                    </span>
                    <span class="h5 offers__item-text">День рождения</span>
                </a>
                <a class="offers__item" href="{{route('services.corporate')}}" title="Корпоратив">
                    <span class="offers__item-img">
                        <svg class="offers__item-icon" role="img">
                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#briefcase") }}"></use>
                        </svg>
                    </span>
                    <span class="h5 offers__item-text">Корпоратив</span>
                </a>
            </div>
        </div>
    </section>
@endsection

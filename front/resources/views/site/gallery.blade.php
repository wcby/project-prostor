@extends('layouts.app')
@section('content')
    @include('partials.top',
    [
        'image' => 'gallery-bg',
        'title' => 'Галерея',
        'top_text' => null
    ])
    <section class="section section--gallery">
        <div class="container">
            <div class="gallery tabs" data-tabs>
                <ul class="tabs__links tabs__links--center" data-tabs-links>
                    @forelse($models as $model)
                        @if($model->images->first())
                            <li class="tabs__links-item">
                                <a class="tabs__links-link @if($loop->iteration === 1) current @endif" href="#gallery-{{ $model->id }}" title="{{ $model->name }}">
                                    {{ $model->name }}
                                </a>
                            </li>
                        @endif
                    @empty

                    @endforelse
                </ul>
                <div class="tabs__items" data-tabs-items>
                    @forelse($models as $model)
                        @if($model->images->first())
                            <div class="tabs__item @if($loop->iteration === 1) current @endif" id="gallery-{{ $model->id }}">
                                <div class="grid">
                                    @foreach($model->images as $image)
                                        @if (file_exists(public_path("{$image->path}")))
                                            <a class="gallery__item" href="{{ asset(image_path("{$image->path}", "large")) }}" data-fancybox="gallery-{{ $model->id }}" title="{{ $model->name }}">
                                                <img class="gallery__item-img" src="{{ asset(image_path("{$image->path}", "medium")) }}" alt="{{ $model->name }}" loading='lazy'>
                                                <div class="gallery__item-thumb">
                                                    <div class="gallery__item-icon">
                                                        <svg role="img">
                                                            <use xlink:href="{{ asset("assets/img/icons.svg#search") }}"></use>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </a>
                                        @else
                                            <a class="gallery__item" href="{{ asset($image->path) }}" data-fancybox="gallery-{{ $model->id }}" title="{{ $model->name }}">
                                                <img class="gallery__item-img" src="{{ asset($image->path) }}" alt="{{ $model->name }}">
                                                <div class="gallery__item-thumb">
                                                    <div class="gallery__item-icon">
                                                        <svg role="img">
                                                            <use xlink:href="{{ asset("assets/img/icons.svg#search") }}"></use>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </a>
                                        @endif

                                    @endforeach
                                </div>
                            </div>
                        @endif
                    @empty
                        <span>Изображений нет</span>
                    @endforelse
                </div>
            </div>
        </div>
    </section>

@endsection

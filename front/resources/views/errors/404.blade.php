@extends('layouts.errors')

@section('content')
    <div class="not-found">
        <div class="container not-found__container section--default">
            <h1 class="h1 reset-mt">Страница не найдена</h1>
            <p>Извините, запрашиваемая вами страница не существует</p>
            <a class="button button--large button--yellow button--full"
               title="Вернуться на главную"
               href="{{ route('home') }}">
                <span class="button__text">Вернуться на главную</span>
            </a>
        </div>
    </div>
@endsection

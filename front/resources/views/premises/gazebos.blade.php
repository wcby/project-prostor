@extends('layouts.app')
@section('content')
    @include('partials.top',
    [
        'image' => 'houses-bg',
        'title' => 'Беседки',
        'top_text' => null
    ])
    <section class="section houses">
        <div class="container">
            <div class="section__title section__title--flex section--pt">
                <h2 class="reset section__title-text">Беседки</h2>
                <div class="section__wave-text">
                    <div class="section__wave">
                        <img class="section__wave-img" src="{{ asset("assets/img/waves.svg") }}" alt="decor" width="50" height="30" loading="lazy">
                    </div>
                    <p class="section__text">
                        Мы рады предложить вам комфортабельные деревянные беседки, расположенные вдоль
                        водоема на территории базы. <br> Все беседки защищены от дождя и оборудованы мангалом,
                        на котором вы можете приготовить свежепойманную рыбку или любое другое блюдо на ваш
                        вкус
                    </p>
                </div>
            </div>
            <div class="section--p">
                <div class="houses__items" id="gazebos">
                    @forelse($models as $model)
                        <div class="houses-item" id="gazebos-{{ $model->id }}">
                            <h2 class="reset-mt houses-item__title houses-item__title--xs">
                                {{ $model->name }}
                            </h2>
                            <div class="houses-item__slider">
                                <div class="houses-item__images">
                                    <div class="swiper" data-swiper="images">
                                        <div class="swiper-wrapper">
                                            @foreach($model->images as $image)
                                                <div class="swiper-slide houses-item__slide">
                                                    <a class="houses-item__picture" href="{{ asset(image_path("{$image->path}", "large")) }}" data-fancybox="gallery-houses-{{$model->id}}" title="{{ $model->name }}">
                                                        <img class="houses-item__img" src="{{ asset(image_path("{$image->path}", "medium")) }}" loading='lazy'>
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="houses-item__thumbs">
                                    <div class="swiper" data-swiper="thumbs">
                                        <div class="swiper-wrapper">
                                            @foreach($model->images as $image)
                                                <div class="swiper-slide houses-item__thumbs-slide">
                                                    <img class="houses-item__thumbs-img" src="{{ asset(image_path("{$image->path}", "thumbnail")) }}" alt="{{$model->name}}" loading='lazy'>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="houses-item__navigation">
                                            <button class="swiper-button swiper-button--next swiper-button--round" type="button" aria-label="Следующий слайд">
                                                <svg role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg#arrow-right") }}"></use>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="houses-item__scrollbar">
                                            <div class="swiper-scrollbar"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="houses-item__content">
                                <h2 class="reset-mt houses-item__title houses-item__title--md">{{$model->name}}</h2>
                                @if($model->id == 6 or $model->id == 7 or $model->id == 8 or $model->id == 9 or $model->id == 10)
                                <p>Открытые беседки у воды, предоставляются в качестве дополнительной услуги и оплачиваются отдельно. Размещение в беседке возможно на время приобретенного входного билета, но не более суток. <br>Расчетный час 8:00.</p>
                                @else
                                <ul class="houses-item__services">
                                    <li>
                                        <span class="houses-item__services-img">
                                            <svg class="houses-item__services-icon houses-item__services-icon--users" role="img">
                                              <use xlink:href="{{ asset("assets/img/icons.svg#users") }}"></use>
                                            </svg>
                                        </span>
                                        <span class="houses-item__services-text">До {{$model->count}} человек</span>
                                    </li>
                                    <li>
                                        <span class="houses-item__services-img">
                                            <svg class="houses-item__services-icon houses-item__services-icon--clock" role="img">
                                              <use xlink:href="{{ asset("assets/img/icons.svg#clock") }}"></use>
                                            </svg>
                                        </span>
                                        <span class="houses-item__services-text">Заезд в 10:00 выезд 24:00</span>
                                    </li>
                                    <li>
                                        <span class="houses-item__services-img">
                                            <svg class="houses-item__services-icon houses-item__services-icon--user-plus" role="img">
                                              <use xlink:href="{{ asset("assets/img/icons.svg#user-plus") }}"></use>
                                            </svg>
                                        </span>
                                        <span class="houses-item__services-text">Дополнительное место {{$model->seat_cost}}₽/человек</span>
                                    </li>
                                </ul>
                                @endif
                                <div class="houses-item__in-house">
                                    <div class="houses-item__in-house-block">
                                        <span class="houses-item__in-house-title">В беседке</span>
                                        <ul class="houses-item__in-house-list">
                                            @forelse($model->objectAdditionals->groupBy('additional.additionalType.name') as $typeName => $objectAdditionals)
                                                @forelse($objectAdditionals as $objectAdditional)
                                                    <li class="houses-item__in-house-item">
                                                        <svg role="img">
                                                            <use xlink:href="{{ asset("assets/img/icons.svg#") }}{{$objectAdditional->additional->icon}}"></use>
                                                        </svg>
                                                        <span>{{$objectAdditional->additional->name}}</span>
                                                    </li>
                                                @empty

                                                @endforelse
                                            @empty

                                            @endforelse
                                        </ul>
                                    </div>
                                </div>
                                <div class="houses-item__price-button">
                                    <div class="houses-item__price"><span class="houses-item__price-value">Стоимость: {{$model->price}}₽/день</span><a class="houses-item__rules" href="rules" title="Правила проживания">Правила проживания</a></div>
                                    <div class="houses-item__button"><a class="button button--large button--green button--full" title="Забронировать" href="{{ route('booking') }}"><span class="button__text">Забронировать</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty

                    @endforelse
                </div>
            </div>
            <div class="section__title section__title--grid section--pb">
                <h3 class="section__title-text">Аренда беседки с мангалом на берегу озера</h3>
                <div class="section__wave">
                    <img class="section__wave-img" src="{{ asset("assets/img/waves.svg") }}" alt="decor" width="50" height="30" loading="lazy">
                </div>
                <div class="section__text">
                    <p>Аренда беседок — это современный формат комфортного отдыха на природе. Вас ждут чистая природа, живописный пейзаж и все необходимые удобства. Наши просторные беседки и мангальные зоны созданы для приятных встреч с друзьями и отдыха с семьей. Арендуя у нас беседку вам не придется переживать даже об углях и мангале — всё необходимое для шашлыка Вы найдете на нашей базе отдыха. Беседка со стенами и крышей защитит от дождя и ветра, а расположение на территории — гарантия того, что отдых не испортят неприятные кампании пососедству или другими отдыхающими оставленный мусор.</p>
                </div>
            </div>
        </div>
    </section>

@endsection

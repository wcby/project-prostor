@extends('layouts.app')
@section('content')
    @include('partials.top',
    [
        'image' => 'houses-bg',
        'title' => 'Домики',
        'top_text' => null
    ])
    <section class="section houses">
        <div class="container">
            <div class="section__title section__title--flex section--pt">
                <h2 class="reset section__title-text">Домики</h2>
                <div class="section__wave-text">
                    <div class="section__wave">
                        <img class="section__wave-img" src="{{ asset("assets/img/waves.svg") }}" alt="decor" width="50"
                             height="30" loading="lazy">
                    </div>
                    <p class="section__text">
                        Проведите незабываемый отдых с семьей и друзьями в одном из 14 уютных домиков нашей базы
                        отдыха.
                        В домиках есть всё необходимое для комфортного отдыха: душ, холодильник, кондиционер, телевизор.
                        Вам остается лишь выбрать, тот, что сделает ваш отдых идеальным
                    </p>
                </div>
            </div>
            <div class="section--p">
                <div class="houses__items" id="houses">
                    @forelse($models as $model)
                        <div class="houses-item" id="houses-{{ $model->id }}">
                            <h2 class="reset-mt houses-item__title houses-item__title--xs">{{ $model->name }}</h2>
                            <div class="houses-item__slider">
                                <div class="houses-item__images">
                                    <div class="swiper" data-swiper="images">
                                        <div class="swiper-wrapper">
                                            @foreach($model->images as $image)
                                                <div class="swiper-slide houses-item__slide">
                                                    <a class="houses-item__picture" href="{{ asset(image_path("{$image->path}", "large")) }}"
                                                       data-fancybox="gallery-houses-{{ $model->id }}" title="{{ $model->name }}">
                                                        <img class="houses-item__img"
                                                             src="{{ asset(image_path("{$image->path}", "medium")) }}" alt="{{ $model->name }}" loading='lazy'
                                                        >
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="houses-item__thumbs">
                                    <div class="swiper" data-swiper="thumbs">
                                        <div class="swiper-wrapper">
                                            @foreach($model->images as $image)
                                                <div class="swiper-slide houses-item__thumbs-slide">
                                                    <img class="houses-item__thumbs-img"
                                                         src="{{ asset(image_path("{$image->path}", "thumbnail")) }}" alt="{{ $model->name }}" loading='lazy'
                                                         >
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="houses-item__navigation">
                                            <button class="swiper-button swiper-button--next swiper-button--round"
                                                    type="button" aria-label="Следующий слайд">
                                                <svg role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg#arrow-right") }}"></use>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="houses-item__scrollbar">
                                            <div class="swiper-scrollbar"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="houses-item__content">
                                <h2 class="reset-mt houses-item__title houses-item__title--md">
                                    {{ $model->name }}
                                </h2>
                                <ul class="houses-item__services">
                                    <li>
                                        <span class="houses-item__services-img">
                                            <svg class="houses-item__services-icon houses-item__services-icon--star" role="img">
                                              <use xlink:href="{{ asset("assets/img/icons.svg#star") }}"></use>
                                            </svg>
                                        </span>
                                        <span class="houses-item__services-text">
                                            {{ $model->category->name }}
                                        </span>
                                    </li>
                                    <li>
                                        <span class="houses-item__services-img">
                                            <svg class="houses-item__services-icon houses-item__services-icon--users" role="img">
                                              <use xlink:href="{{ asset("assets/img/icons.svg#users") }}"></use>
                                            </svg>
                                        </span>
                                        <span class="houses-item__services-text">
                                            @if($model->count === '1')
                                                Одноместный
                                            @elseif($model->count === '2')
                                                Двухместный
                                            @elseif($model->count === '3')
                                                Трехместный
                                            @elseif($model->count === '4')
                                                Четырехместный
                                            @else
                                                {{ $model->count }}
                                            @endif
                                        </span>
                                    </li>
                                    <li>
                                        <span class="houses-item__services-img">
                                            <svg class="houses-item__services-icon houses-item__services-icon--clock" role="img">
                                              <use xlink:href="{{ asset("assets/img/icons.svg#clock") }}"></use>
                                            </svg>
                                        </span>
                                        <span class="houses-item__services-text">Заезд в 13:00 выезд 11:00</span>
                                    </li>
                                </ul>
                                <div class="houses-item__in-house">
                                    @forelse($model->objectAdditionals->groupBy('additional.additionalType.name') as $typeName => $objectAdditionals)
                                        <div class="houses-item__in-house-block">
                                            <span class="houses-item__in-house-title">{{ $typeName }}</span>
                                            <ul class="houses-item__in-house-list">
                                                @forelse($objectAdditionals as $objectAdditional)
                                                <li class="houses-item__in-house-item">
                                                    <svg role="img">
                                                        <use xlink:href="{{ asset("assets/img/icons.svg#") }}{{$objectAdditional->additional->icon}}"></use>
                                                    </svg>
                                                    <span>{{$objectAdditional->additional->name}}</span>
                                                </li>
                                                @empty

                                                @endforelse
                                            </ul>
                                        </div>
                                    @empty

                                    @endforelse
                                </div>
                                <div class="houses-item__price-button">
                                    <div class="houses-item__price">
                                        <span class="houses-item__price-value">Стоимость: {{ $model->price }}₽/сутки</span>
                                        <a class="houses-item__rules" href="{{ route('rules') }}" title="Правила проживания">
                                            Правила проживания
                                        </a>
                                    </div>
                                    <div class="houses-item__button">
                                        <a class="button button--large button--green button--full" title="Забронировать" href="#">
                                            <span class="button__text">Забронировать</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <span>Домики не добавлены, зайдите позже</span>
                    @endforelse
                </div>
            </div>
            <div class="section__title section__title--grid section--pb">
                <h3 class="section__title-text">Отдых в «Родных просторах»: комфортно и по приятной цене</h3>
                <div class="section__wave"><img class="section__wave-img" src="{{ asset("assets/img/waves.svg") }}"
                                                alt="decor" width="50" height="30" loading="lazy"></div>
                <div class="section__text">
                    <p>Отдыхайте на природе на уютной базе отдыха на берегу водоема! Насладитесь прекрасными видами,
                        комфортабельными беседками и удобными мангальными зонами. Отличное место для семейного
                        времяпровождения и дружеских посиделок.</p>
                    <p>Мы предлагаем различные варианты размещения по доступным ценам. Здесь вы сможете расслабиться,
                        наслаждаться уютной атмосферой. Забудьте о повседневных заботах и проведите время на природе.
                        База отдыха на берегу водоема — идеальное место для отдыха от городской суеты!</p>
                </div>
            </div>
        </div>
    </section>
@endsection

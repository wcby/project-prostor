@extends('layouts.app')
@section('content')
    @include('partials.top',
    [
        'image' => 'services-bg',
        'title' => 'Услуги',
        'top_text' => null
    ])
    <section class="section services">
        <div class="container">
            <div class="section__title section__title--flex section--pt">
                <h2 class="reset section__title-text">Совместите приятное с полезным</h2>
                <div class="section__wave-text">
                    <div class="section__wave"><img class="section__wave-img" src="{{ asset("assets/img/waves.svg") }}" alt="decor" width="50" height="30" loading="lazy"></div>
                    <p class="section__text">
                        Наша база отдыха идеально подходит для корпоративного и семейного отдыха.<br>
                        Мы гарантируем вам комфортное размещение и отличное место для проведения мероприятий
                    </p>
                </div>
            </div>
            <div class="section--pb">
                <div class="grid">
                    <a class="services__item" href="{{ route('services.fishing') }}" title="Рыбная ловля">
                        <img class="services__item-img" src="{{ asset("assets/img/fishing-thumb.jpg") }}" alt="Рыбная ловля" width="1500" height="880">
                        <span class="services__item-title">
                            Рыбная ловля
                            <span class="services__item-icon">
                                <svg role="img">
                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow") }}"></use>
                                </svg>
                            </span>
                        </span>
                    </a>
                    <a class="services__item" href="{{ route('services.pool') }}" title="Открытый бассейн">
                        <img class="services__item-img" src="{{ asset("assets/img/swimming-pool-thumb.jpg") }}" alt="Открытый бассейн" width="1500" height="880">
                        <span class="services__item-title">
                            Открытый бассейн
                            <span class="services__item-icon">
                                <svg role="img">
                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow") }}"></use>
                                </svg>
                            </span>
                        </span>
                    </a>
                    <a class="services__item" href="{{ route('services.family') }}" title="Семейный отдых">
                        <img class="services__item-img" src="{{ asset("assets/img/family-holiday-thumb.jpg") }}" alt="Семейный отдых" width="1500" height="880">
                        <span class="services__item-title">
                            Семейный отдых
                            <span class="services__item-icon">
                                <svg role="img">
                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow") }}"></use>
                                </svg>
                            </span>
                        </span>
                    </a>
                    <a class="services__item" href="{{ route('services.birthday') }}" title="День рождения">
                        <img class="services__item-img" src="{{ asset("assets/img/birthday-thumb.jpg") }}" alt="День рождения" width="1500" height="880">
                        <span class="services__item-title">
                            День рождения
                            <span class="services__item-icon">
                                <svg role="img">
                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow") }}"></use>
                                </svg>
                            </span>
                        </span>
                    </a>
                    <a class="services__item" href="{{ route('services.corporate') }}" title="Корпоративы на природе">
                        <img class="services__item-img" src="{{ asset("assets/img/corporate-events-thumb.jpg") }}" alt="Корпоративы на природе" width="1500" height="880">
                        <span class="services__item-title">
                            Корпоративы на природе
                            <span class="services__item-icon">
                                <svg role="img">
                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow") }}"></use>
                                </svg>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </section>

@endsection

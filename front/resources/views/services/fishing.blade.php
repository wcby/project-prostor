@extends('layouts.app')
@section('content')
    @include('partials.top',
    [
        'image' => 'service-fishing-bg',
        'title' => 'Рыбная ловля',
        'top_text' => null
    ])
    <div class="services">
        <div class="container">
            @include('partials.breadcrumbs', ['title' => 'Рыбная ловля'])
            <div class="section__title section__title--flex section--title">
                <h2 class="reset section__title-text">Место, где всегда клюет</h2>
                <div class="section__wave-text">
                    <div class="section__wave">
                        <img class="section__wave-img" src="{{ asset("assets/img/waves.svg") }}" alt="decor" width="50" height="30" loading="lazy"></div>
                    <p class="section__text">Настоящие рыбаки знают, насколько увлекательна и интересна рыбная ловля. <br>Мы находимся в непосредственной близости от г-к Анапы и круглый год работаем над тем, чтобы вы получали удовольствие от рыбалки и получили массу незабываемых впечатлений, отрешившись от суеты города. </p>
                </div>
            </div>
        </div>
        <section class="section section--default section--blue">
            <div class="tickets container">
                <div class="services__title">
                    <div class="services__title-icon">
                        <svg role="img">
                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#ticket") }}"></use>
                        </svg>
                    </div>
                    <h3 class="reset-all services__title-text">Входной билет</h3>
                </div>
                <div class="tickets__block">
                    <h5 class="reset-mt">Дневное время</h5>
                    <div class="tickets__items grid">
                        <div class="tickets__item">
                            <div class="tickets__item-title"><span>Мужчины</span><span>600₽</span></div>
                            <div class="tickets__item-text">Включено 4 кг выловленной рыбы (норма вылова)</div>
                        </div>
                        <div class="tickets__item">
                            <div class="tickets__item-title"><span>Женщины</span><span>250₽</span></div>
                            <div class="tickets__item-text">Пребывание на территории (норма вылова отсутствует, плата взимается за фактически пойманную рыбу)</div>
                        </div>
                        <div class="tickets__item">
                            <div class="tickets__item-title"><span>Подростки парни 13-18 лет</span><span>300₽</span></div>
                            <div class="tickets__item-text">50% стоимости мужского билета</div>
                        </div>
                        <div class="tickets__item">
                            <div class="tickets__item-title"><span>Дети до 12 лет включительно</span><span>0₽</span></div>
                            <div class="tickets__item-text">Бесплатно</div>
                        </div>
                    </div>
                </div>
                <div class="tickets__block">
                    <h5 class="reset-mt">На сутки</h5>
                    <div class="tickets__items grid">
                        <div class="tickets__item">
                            <div class="tickets__item-title"><span>Мужчины</span><span>1200₽</span></div>
                            <div class="tickets__item-text">Включено 8 кг выловленной рыбы (норма вылова)</div>
                        </div>
                        <div class="tickets__item">
                            <div class="tickets__item-title"><span>Женщины</span><span>500₽</span></div>
                            <div class="tickets__item-text">Пребывание на территории (норма вылова отсутствует, плата взимается за фактически пойманную рыбу)</div>
                        </div>
                        <div class="tickets__item">
                            <div class="tickets__item-title"><span>Подростки парни 13-18 лет</span><span>600₽</span></div>
                            <div class="tickets__item-text">50% стоимости мужского билета</div>
                        </div>
                        <div class="tickets__item">
                            <div class="tickets__item-title"><span>Дети до 12 лет включительно</span><span>0₽</span></div>
                            <div class="tickets__item-text">Бесплатно</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section--default">
            <div class="catch container">
                <div class="grid catch__block">
                    <div class="catch__content">
                        <div class="catch__title">
                            <div class="services__title">
                                <div class="services__title-icon">
                                    <svg role="img">
                                        <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#fish") }}"></use>
                                    </svg>
                                </div>
                                <h3 class="reset-all services__title-text">Нормы вылова</h3>
                            </div>
                            <div class="section__wave">
                                <img class="section__wave-img" src="{{ asset("assets/img/waves.svg") }}" alt="decor" width="50" height="30" loading="lazy">
                            </div>
                        </div>
                        <h5 class="catch__subtitle">В норму вылова входят</h5>
                        <ul class="catch__list">
                            <li class="catch__item catch__item--valid">Карп</li>
                            <li class="catch__item catch__item--valid">Сазан</li>
                            <li class="catch__item catch__item--valid">Сом</li>
                            <li class="catch__item catch__item--valid">Щука</li>
                            <li class="catch__item catch__item--valid">Белый амур</li>
                            <li class="catch__item catch__item--valid">Толстолобик</li>
                        </ul>
                        <h5 class="catch__subtitle">В норму вылова не входят</h5>
                        <div class="catch__list-wrapper">
                            <div class="catch__list-left">
                                <ul class="catch__list catch__list--invalid">
                                    <li class="catch__item catch__item--invalid">Карась</li>
                                    <li class="catch__item catch__item--invalid">Лещ</li>
                                    <li class="catch__item catch__item--invalid">Подлещик</li>
                                    <li class="catch__item catch__item--invalid">Красноперка</li>
                                </ul>
                                <p class="catch__text">Дополнительная плата не взимается</p>
                            </div>
                            <div class="catch__list-right">
                                <ul class="catch__list catch__list--invalid">
                                    <li class="catch__item catch__item--invalid">Судак</li>
                                </ul>
                                <p class="catch__text">Взимается плата 350₽ за 1 кг <br> Может быть включен в норму вылова пропорционально стоимости входного билета</p>
                            </div>
                        </div>
                    </div><img class="catch__img" src="{{ asset("assets/img/catch-1.jpg") }}" alt="Рыбная ловля" width="1500" height="974" loading="lazy">
                </div>
                <div class="grid catch__block">
                    <div class="catch__content catch__content--invalid">
                        <h5 class="reset-mt catch__subtitle">Превышение нормы вылова</h5>
                        <p class="catch__text">При превышении нормы вылова взимается плата за фактически пойманную массу рыбы из расчёта:</p>
                        <ul class="catch__list">
                            <li class="catch__item catch__item--full">350₽ / 1кг живого веса — судак, щука, сом</li>
                            <li class="catch__item catch__item--full">200₽ / 1кг живого веса — карп, белый амур</li>
                            <li class="catch__item catch__item--full">150₽ / 1кг живого веса — толстолобик</li>
                        </ul>
                        <div class="catch__attention">
                            <svg class="catch__attention-icon" role="img">
                                <use xlink:href="/assets/img/icons.svg?v=1693225697529#alert"></use>
                            </svg>
                            <p class="catch__text catch__text--attention">Карп/Сазан весом 5 кг. и более отпускается обратно в водоём. <br> В случае нарушения данного правила Карп, Сазан 5 кг и более оплачивается из расчёта 1000 рублей за 1 кг. живого веса <br> (см. п. 21 <a href="{{ route('rules') }}#rule-21" title="п.21 Правил и условий нахождения на территории базы отдыха">Правил и условий нахождения на территории базы отдыха</a> «Родные просторы»)</p>
                        </div>
                    </div><img class="catch__img" src="{{ asset("assets/img/catch-2.jpg") }}" alt="Рыбная ловля" width="1500" height="974" loading="lazy">
                </div>
                <div class="grid catch__block">
                    <div class="catch__content catch__content--other">
                        <div class="services__title">
                            <div class="services__title-icon">
                                <svg role="img">
                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#waves") }}"></use>
                                </svg>
                            </div>
                            <h3 class="reset-all services__title-text">Дополнительные услуги</h3>
                        </div>
                        <h5 class="catch__subtitle">Прокат рыболовных снастей</h5>
                        <ul class="catch__list">
                            <li class="catch__item catch__item--other">Удочки 200₽ световой день</li>
                            <li class="catch__item catch__item--other">Спиннинги 300₽ световой день</li>
                            <li class="catch__item catch__item--other">Садок 150₽ световой день</li>
                            <li class="catch__item catch__item--other">Подсак 200₽ световой день</li>
                            <li class="catch__item catch__item--other">Подставка 50₽ световой день</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="section section--default section--ovh section--blue">
            <div class="container">
                <div class="section__title section__title--flex section__title--mb">
                    <h2 class="reset section__title-text">Комфортное размещение в объятиях природы доступно для каждого</h2>
                    <div class="section__wave-text">
                        <div class="section__text">
                            <p>Мы предлагаем различные варианты проживания, чтобы наши гости чувствовали себя комфортно и наслаждались отдыхом</p><a class="slider__link-all" href="{{ route('premises.houses') }}" title="Все варианты размещения"><span>Все варианты размещения</span>
                                <svg role="img">
                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow") }}"></use>
                                </svg></a>
                        </div>
                    </div>
                </div>
                <div class="slider tabs" data-tabs>
                    <ul class="tabs__links tabs__links--full" data-tabs-links>
                        <li class="tabs__links-item"><a class="tabs__links-link current" href="#slider-0" data-link="{{ route('premises.houses') }}#houses" title="Домики">Домики</a></li>
                        <li class="tabs__links-item"><a class="tabs__links-link" href="#slider-1" data-link="{{ route('premises.houses') }}#guest-houses" title="Гостевые дома">Гостевые дома</a></li>
                        <li class="tabs__links-item"><a class="tabs__links-link" href="#slider-2" data-link="{{ route('premises.gazebos') }}#gazebos" title="Беседки">Беседки</a></li>
                        <li class="tabs__links-item"><a class="tabs__links-link" href="#slider-3" data-link="{{ route('premises.gazebos') }}#banquet-gazebos" title="Банкетные беседки">Банкетные беседки</a></li>
                    </ul>
                    <div class="tabs__items" data-tabs-items>
                        <div class="tabs__item current" id="slider-0">
                            <div class="slider__swiper swiper">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.houses') }}#houses-0" title="Домик №1, №2"><img class="slider__item-img" src="/assets/img/house-1.jpg" alt="Домик №1, №2" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Домик</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-2 человека</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.houses') }}#houses-1" title="Домик №3, №4"><img class="slider__item-img" src="/assets/img/house-1.jpg" alt="Домик №3, №4" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Домик</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-3 человека</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.houses') }}#houses-2" title="Домик №5, №6"><img class="slider__item-img" src="/assets/img/house-1.jpg" alt="Домик №5, №6" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Домик</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-2 человека</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.houses') }}#houses-3" title="Домик №8, №9"><img class="slider__item-img" src="/assets/img/house-1.jpg" alt="Домик №8, №9" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Домик</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-3 человека</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.houses') }}#houses-4" title="Домик №10, №11"><img class="slider__item-img" src="/assets/img/house-1.jpg" alt="Домик №10, №11" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Домик</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-2 человека</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.houses') }}#houses-5" title="Домик №12, №13"><img class="slider__item-img" src="/assets/img/house-1.jpg" alt="Домик №12, №13" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Домик</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-2 человека</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider__scrollbar">
                                    <div class="swiper-scrollbar"></div>
                                </div>
                                <div class="slider__control">
                                    <button class="swiper-button swiper-button--prev swiper-button--round" type="button" aria-label="Предыдущий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                    <button class="swiper-button swiper-button--next swiper-button--round" type="button" aria-label="Следующий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="tabs__item" id="slider-1">
                            <div class="slider__swiper swiper">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.houses') }}#guest-houses-0" title="Гостевой дом №1"><img class="slider__item-img" src="/assets/img/guest-house-1.jpg" alt="Гостевой дом №1" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Гостевой дом</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-8 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.houses') }}#guest-houses-1" title="Гостевой дом №2"><img class="slider__item-img" src="/assets/img/guest-house-1.jpg" alt="Гостевой дом №2" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Гостевой дом</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-6 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider__scrollbar">
                                    <div class="swiper-scrollbar"></div>
                                </div>
                                <div class="slider__control">
                                    <button class="swiper-button swiper-button--prev swiper-button--round" type="button" aria-label="Предыдущий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                    <button class="swiper-button swiper-button--next swiper-button--round" type="button" aria-label="Следующий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="tabs__item" id="slider-2">
                            <div class="slider__swiper swiper">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.gazebos') }}#gazebos-0" title="БЕСЕДКА №6"><img class="slider__item-img" src="/assets/img/gazebo-1.jpg" alt="БЕСЕДКА №6" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-8 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.gazebos') }}#gazebos-1" title="БЕСЕДКА №7"><img class="slider__item-img" src="/assets/img/gazebo-1.jpg" alt="БЕСЕДКА №7" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-8 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.gazebos') }}#gazebos-2" title="БЕСЕДКА №8"><img class="slider__item-img" src="/assets/img/gazebo-1.jpg" alt="БЕСЕДКА №8" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-8 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.gazebos') }}#gazebos-3" title="БЕСЕДКА №9"><img class="slider__item-img" src="/assets/img/gazebo-1.jpg" alt="БЕСЕДКА №9" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-8 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.gazebos') }}#gazebos-4" title="БЕСЕДКА №10"><img class="slider__item-img" src="/assets/img/gazebo-1.jpg" alt="БЕСЕДКА №10" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-8 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider__scrollbar">
                                    <div class="swiper-scrollbar"></div>
                                </div>
                                <div class="slider__control">
                                    <button class="swiper-button swiper-button--prev swiper-button--round" type="button" aria-label="Предыдущий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                    <button class="swiper-button swiper-button--next swiper-button--round" type="button" aria-label="Следующий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="tabs__item" id="slider-3">
                            <div class="slider__swiper swiper">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.gazebos') }}#banquet-gazebos-0" title="БЕСЕДКА БАНКЕТНАЯ №1"><img class="slider__item-img" src="/assets/img/banquet-gazebos-1.jpg" alt="БЕСЕДКА БАНКЕТНАЯ №1" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Банкетная беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-15 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.gazebos') }}#banquet-gazebos-1" title="БЕСЕДКА БАНКЕТНАЯ №2"><img class="slider__item-img" src="/assets/img/banquet-gazebos-1.jpg" alt="БЕСЕДКА БАНКЕТНАЯ №2" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Банкетная беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-10 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.gazebos') }}#banquet-gazebos-2" title="БЕСЕДКА БАНКЕТНАЯ №3"><img class="slider__item-img" src="/assets/img/banquet-gazebos-1.jpg" alt="БЕСЕДКА БАНКЕТНАЯ №3" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Банкетная беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-12 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.gazebos') }}#banquet-gazebos-3" title="БЕСЕДКА БАНКЕТНАЯ №4"><img class="slider__item-img" src="/assets/img/banquet-gazebos-1.jpg" alt="БЕСЕДКА БАНКЕТНАЯ №4" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Банкетная беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-12 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.gazebos') }}#banquet-gazebos-4" title="БЕСЕДКА БАНКЕТНАЯ №5"><img class="slider__item-img" src="/assets/img/banquet-gazebos-1.jpg" alt="БЕСЕДКА БАНКЕТНАЯ №5" width="4096" height="2731"></a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Банкетная беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-30 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider__scrollbar">
                                    <div class="swiper-scrollbar"></div>
                                </div>
                                <div class="slider__control">
                                    <button class="swiper-button swiper-button--prev swiper-button--round" type="button" aria-label="Предыдущий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                    <button class="swiper-button swiper-button--next swiper-button--round" type="button" aria-label="Следующий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

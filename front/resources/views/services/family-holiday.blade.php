@extends('layouts.app')
@section('content')
    @include('partials.top',
    [
        'image' => 'service-family-holiday-bg',
        'title' => 'Семейный отдых',
        'top_text' => null
    ])
    <div class="services">
        <div class="container">
            @include('partials.breadcrumbs', ['title' => 'Семейный отдых'])
            <div class="section__title section__title--grid section--title">
                <h2 class="section__title-text">Почему вам точно стоит выбрать семейный отдых в Родных просторах?</h2>
                <div class="section__wave">
                    <img class="section__wave-img" src="{{ asset("assets/img/waves.svg") }}" alt="decor" width="50" height="30" loading="lazy">
                </div>
                <div class="section__text">
                    <p>
                        У вас есть возможность насладиться незабываемым отпуском с вашей семьей, замечательно провести время и заняться активным отдыхом.
                        Детская площадка ждет наших маленьких гостей, а по праздникам у нас проходят анимационные программы.
                    </p>
                    <p>
                        Мы также предлагаем различные варианты размещения, чтобы вы смогли выбрать оптимальный вариант для своей семьи: беседки, гостевые
                        дома и уютные домики обеспечат комфорт, а доступные цены сделают отдых еще приятнее.
                    </p>
                </div>
            </div>
            <section class="section section--pb">
                <div class="grid"><a class="gallery__item" href="{{ asset("assets/img/family-holiday-thumb.jpg") }}" data-fancybox="gallery-0" title="Семейный отдых">
                        <img class="gallery__item-img" src="{{ asset("assets/img/family-holiday-thumb.jpg") }}" alt="Семейный отдых" width="1500" height="880">
                        <div class="gallery__item-thumb">
                            <div class="gallery__item-icon">
                                <svg role="img">
                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#search") }}"></use>
                                </svg>
                            </div>
                        </div></a><a class="gallery__item" href="{{ asset("assets/img/family-holiday-thumb.jpg") }}" data-fancybox="gallery-0" title="Семейный отдых">
                        <img class="gallery__item-img" src="{{ asset("assets/img/family-holiday-thumb.jpg") }}" alt="Семейный отдых" width="1500" height="880">
                        <div class="gallery__item-thumb">
                            <div class="gallery__item-icon">
                                <svg role="img">
                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#search") }}"></use>
                                </svg>
                            </div>
                        </div></a><a class="gallery__item" href="{{ asset("assets/img/family-holiday-thumb.jpg") }}" data-fancybox="gallery-0" title="Семейный отдых">
                        <img class="gallery__item-img" src="{{ asset("assets/img/family-holiday-thumb.jpg") }}" alt="Семейный отдых" width="1500" height="880">
                        <div class="gallery__item-thumb">
                            <div class="gallery__item-icon">
                                <svg role="img">
                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#search") }}"></use>
                                </svg>
                            </div>
                        </div></a><a class="gallery__item" href="{{ asset("assets/img/family-holiday-thumb.jpg") }}" data-fancybox="gallery-0" title="Семейный отдых">
                        <img class="gallery__item-img" src="{{ asset("assets/img/family-holiday-thumb.jpg") }}" alt="Семейный отдых" width="1500" height="880">
                        <div class="gallery__item-thumb">
                            <div class="gallery__item-icon">
                                <svg role="img">
                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#search") }}"></use>
                                </svg>
                            </div>
                        </div></a><a class="gallery__item" href="{{ asset("assets/img/family-holiday-thumb.jpg") }}" data-fancybox="gallery-0" title="Семейный отдых">
                        <img class="gallery__item-img" src="{{ asset("assets/img/family-holiday-thumb.jpg") }}" alt="Семейный отдых" width="1500" height="880">
                        <div class="gallery__item-thumb">
                            <div class="gallery__item-icon">
                                <svg role="img">
                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#search") }}"></use>
                                </svg>
                            </div>
                        </div></a><a class="gallery__item" href="{{ asset("assets/img/family-holiday-thumb.jpg") }}" data-fancybox="gallery-0" title="Семейный отдых">
                        <img class="gallery__item-img" src="{{ asset("assets/img/family-holiday-thumb.jpg") }}" alt="Семейный отдых" width="1500" height="880">
                        <div class="gallery__item-thumb">
                            <div class="gallery__item-icon">
                                <svg role="img">
                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#search") }}"></use>
                                </svg>
                            </div>
                        </div></a>
                </div>
            </section>
        </div>
        <section class="section section--default section--ovh section--blue">
            <div class="container">
                <div class="section__title section__title--flex section__title--mb">
                    <h2 class="reset section__title-text">Комфортное размещение в объятиях природы доступно для каждого</h2>
                    <div class="section__wave-text">
                        <div class="section__text">
                            <p>Мы предлагаем различные варианты проживания, чтобы наши гости чувствовали себя комфортно и наслаждались отдыхом</p>
                            <a class="slider__link-all" href="{{ route('premises.houses') }}#houses" title="Все варианты размещения"><span>Все варианты размещения</span>
                                <svg role="img">
                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow") }}"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="slider tabs" data-tabs>
                    <ul class="tabs__links tabs__links--full" data-tabs-links>
                        <li class="tabs__links-item"><a class="tabs__links-link current" href="#slider-0" data-link="{{ route('premises.houses') }}#houses" title="Домики">Домики</a></li>
                        <li class="tabs__links-item"><a class="tabs__links-link" href="#slider-1" data-link="{{ route('premises.houses') }}#guest-houses" title="Гостевые дома">Гостевые дома</a></li>
                        <li class="tabs__links-item"><a class="tabs__links-link" href="#slider-2" data-link="{{ route('premises.gazebos') }}#gazebos" title="Беседки">Беседки</a></li>
                        <li class="tabs__links-item"><a class="tabs__links-link" href="#slider-3" data-link="{{ route('premises.gazebos') }}#banquet-gazebos" title="Банкетные беседки">Банкетные беседки</a></li>
                    </ul>
                    <div class="tabs__items" data-tabs-items>
                        <div class="tabs__item current" id="slider-0">
                            <div class="slider__swiper swiper">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.houses') }}#houses-0" title="Домик №1, №2">
                                            <img class="slider__item-img" src="{{ asset("assets/img/house-1.jpg") }}" alt="Домик №1, №2" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Домик</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-2 человека</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.houses') }}#houses-1" title="Домик №3, №4">
                                            <img class="slider__item-img" src="{{ asset("assets/img/house-1.jpg") }}" alt="Домик №3, №4" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Домик</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-3 человека</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item"><a class="slider__item-picture" href="{{ route('premises.houses') }}#houses-2" title="Домик №5, №6">
                                            <img class="slider__item-img" src="{{ asset("assets/img/house-1.jpg") }}" alt="Домик №5, №6" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Домик</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg><span class="slider__item-residents">1-2 человека</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.houses') }}#houses-3" title="Домик №8, №9">
                                            <img class="slider__item-img" src="{{ asset("assets/img/house-1.jpg") }}" alt="Домик №8, №9" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Домик</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-3 человека</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.houses') }}#houses-4" title="Домик №10, №11">
                                            <img class="slider__item-img" src="{{ asset("assets/img/house-1.jpg") }}" alt="Домик №10, №11" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Домик</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-2 человека</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.houses') }}#houses-5" title="Домик №12, №13">
                                            <img class="slider__item-img" src="{{ asset("assets/img/house-1.jpg") }}" alt="Домик №12, №13" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Домик</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-2 человека</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider__scrollbar">
                                    <div class="swiper-scrollbar"></div>
                                </div>
                                <div class="slider__control">
                                    <button class="swiper-button swiper-button--prev swiper-button--round" type="button" aria-label="Предыдущий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                    <button class="swiper-button swiper-button--next swiper-button--round" type="button" aria-label="Следующий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="tabs__item" id="slider-1">
                            <div class="slider__swiper swiper">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.houses') }}#guest-houses-0" title="Гостевой дом №1">
                                            <img class="slider__item-img" src="{{ asset("assets/img/guest-house-1.jpg") }}" alt="Гостевой дом №1" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc"><span class="slider__item-type">Гостевой дом</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-8 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.houses') }}#guest-houses-1" title="Гостевой дом №2">
                                            <img class="slider__item-img" src="{{ asset("assets/img/guest-house-1.jpg") }}" alt="Гостевой дом №2" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Гостевой дом</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-6 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider__scrollbar">
                                    <div class="swiper-scrollbar"></div>
                                </div>
                                <div class="slider__control">
                                    <button class="swiper-button swiper-button--prev swiper-button--round" type="button" aria-label="Предыдущий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                    <button class="swiper-button swiper-button--next swiper-button--round" type="button" aria-label="Следующий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="tabs__item" id="slider-2">
                            <div class="slider__swiper swiper">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.gazebos') }}#gazebos-0" title="БЕСЕДКА №6">
                                            <img class="slider__item-img" src="{{ asset("assets/img/gazebo-1.jpg") }}" alt="БЕСЕДКА №6" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-8 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.gazebos') }}#gazebos-1" title="БЕСЕДКА №7">
                                            <img class="slider__item-img" src="{{ asset("assets/img/gazebo-1.jpg") }}" alt="БЕСЕДКА №7" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-8 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.gazebos') }}#gazebos-2" title="БЕСЕДКА №8">
                                            <img class="slider__item-img" src="{{ asset("assets/img/gazebo-1.jpg") }}" alt="БЕСЕДКА №8" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-8 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.gazebos') }}#gazebos-3" title="БЕСЕДКА №9">
                                            <img class="slider__item-img" src="{{ asset("assets/img/gazebo-1.jpg") }}" alt="БЕСЕДКА №9" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-8 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.gazebos') }}#gazebos-4" title="БЕСЕДКА №10">
                                            <img class="slider__item-img" src="{{ asset("assets/img/gazebo-1.jpg") }}" alt="БЕСЕДКА №10" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-8 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider__scrollbar">
                                    <div class="swiper-scrollbar"></div>
                                </div>
                                <div class="slider__control">
                                    <button class="swiper-button swiper-button--prev swiper-button--round" type="button" aria-label="Предыдущий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                    <button class="swiper-button swiper-button--next swiper-button--round" type="button" aria-label="Следующий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="tabs__item" id="slider-3">
                            <div class="slider__swiper swiper">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.gazebos') }}#banquet-gazebos-0" title="БЕСЕДКА БАНКЕТНАЯ №1">
                                            <img class="slider__item-img" src="{{ asset("assets/img/banquet-gazebos-1.jpg") }}" alt="БЕСЕДКА БАНКЕТНАЯ №1" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Банкетная беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-15 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.gazebos') }}#banquet-gazebos-1" title="БЕСЕДКА БАНКЕТНАЯ №2">
                                            <img class="slider__item-img" src="{{ asset("assets/img/banquet-gazebos-1.jpg") }}" alt="БЕСЕДКА БАНКЕТНАЯ №2" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Банкетная беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-10 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.gazebos') }}#banquet-gazebos-2" title="БЕСЕДКА БАНКЕТНАЯ №3">
                                            <img class="slider__item-img" src="{{ asset("assets/img/banquet-gazebos-1.jpg") }}" alt="БЕСЕДКА БАНКЕТНАЯ №3" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Банкетная беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-12 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.gazebos') }}#banquet-gazebos-3" title="БЕСЕДКА БАНКЕТНАЯ №4">
                                            <img class="slider__item-img" src="{{ asset("assets/img/banquet-gazebos-1.jpg") }}" alt="БЕСЕДКА БАНКЕТНАЯ №4" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Банкетная беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-12 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide slider__item">
                                        <a class="slider__item-picture" href="{{ route('premises.gazebos') }}#banquet-gazebos-4" title="БЕСЕДКА БАНКЕТНАЯ №5">
                                            <img class="slider__item-img" src="{{ asset("assets/img/banquet-gazebos-1.jpg") }}" alt="БЕСЕДКА БАНКЕТНАЯ №5" width="4096" height="2731">
                                        </a>
                                        <div class="slider__item-desc">
                                            <span class="slider__item-type">Банкетная беседка</span>
                                            <div class="slider__item-value">
                                                <svg class="slider__item-icon" role="img">
                                                    <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#user") }}"></use>
                                                </svg>
                                                <span class="slider__item-residents">1-30 человек</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="slider__scrollbar">
                                    <div class="swiper-scrollbar"></div>
                                </div>
                                <div class="slider__control">
                                    <button class="swiper-button swiper-button--prev swiper-button--round" type="button" aria-label="Предыдущий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                    <button class="swiper-button swiper-button--next swiper-button--round" type="button" aria-label="Следующий слайд">
                                        <svg role="img">
                                            <use xlink:href="{{ asset("assets/img/icons.svg?v=1693225697529#arrow-right") }}"></use>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection

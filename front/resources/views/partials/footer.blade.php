<div class="footer__wrapper">
    @include('partials.feedback')
    <footer class="footer">
        <div class="container">
            <div class="footer__main"><a class="footer__logo" href="{{ route('home') }}" title="Родные просторы">
                    <img class="footer__logo-img" src="{{ asset("/assets/img/logo.svg") }}" alt="Родные просторы" width="162" height="64" loading="lazy"/>
                </a>
                <div class="footer__nav">
                    <h5 class="footer__title">Отдыхающим</h5>
                    <ul class="footer__menu">
                        <li class="footer__menu-item"><a class="footer__menu-link" href="{{ route('gallery') }}" title="Галерея">Галерея</a></li>
                        <li class="footer__menu-item"><a class="footer__menu-link" href="{{ route('services.index') }}" title="Услуги">Услуги</a></li>
                        <li class="footer__menu-item"><a class="footer__menu-link" href="{{ route('rules') }}" title="Проживание">Проживание</a></li>
                        <li class="footer__menu-item"><a class="footer__menu-link" href="{{ route('contacts') }}" title="Контакты">Контакты</a></li>
                    </ul>
                </div>
                <div class="footer__address">
                    <h5 class="footer__title">Адрес</h5>
                    <div class="footer__address-text">Краснодарский край <br/>станица Гостагаевская г-к Анапа</div>
                    <a class="footer__address-link" target="_blank" href="https://yandex.ru/profile/92803645800?no-distribution=1&source=wizbiz_new_map_single" title="Проложить маршрут">Проложить маршрут</a>
                </div>
                <div class="footer__contacts">
                    <h5 class="footer__title">Связаться с нами</h5>
                    <div class="footer__contacts-main">
                        <div class="footer__contacts-blocks">
                            <div class="footer__contacts-block">
                                <svg class="footer__contacts-icon" role="img">
                                    <use xlink:href="{{ asset("/assets/img/icons.svg?v=1693225697529#envelope") }}"></use>
                                </svg>
                                <div class="footer__contacts-links"><a class="footer__contacts-item" href="mailto:r.prostori@mail.ru" title="Написать нам"> r.prostori@mail.ru</a></div>
                            </div>
                            <div class="footer__contacts-block">
                                <svg class="footer__contacts-icon" role="img">
                                    <use xlink:href="{{ asset("/assets/img/icons.svg?v=1693225697529#phone") }}"></use>
                                </svg>
                                <div class="footer__contacts-links">
                                    <a class="footer__contacts-item" href="tel:89883429798" title="Позвонить нам">+7 988 342-97-98</a>
                                </div>
                            </div>
                        </div>
                        <ul class="footer__social">
                            <li class="footer__social-item"><a class="footer__social-link" href="https://t.me/rodnieprostori" title="Мы в Телеграмме">
                                    <svg role="img">
                                        <use xlink:href="{{ asset("/assets/img/icons.svg?v=1693225697529#telegramm") }}"></use>
                                    </svg></a></li>
                            <li class="footer__social-item"><a class="footer__social-link" href="https://vk.com/r.prostori" title="Мы в Vk">
                                    <svg role="img">
                                        <use xlink:href="{{ asset("/assets/img/icons.svg?v=1693225697529#vk") }}"></use>
                                    </svg></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer__bottom">
                <div class="footer__bottom-item">© 2008 — 2023 Все права защищены</div>
                <div class="footer__bottom-item">
                    <a class="footer__bottom-link" href="#" title="Пользовательское соглашение">Пользовательское соглашение</a>
                </div>
                <div class="footer__bottom-item">
                    <a class="footer__bottom-link" href="#" title="Соглашение на обработку персональных данных">Политика конфиденциальности</a>
                </div>
            </div>
        </div>
    </footer>
</div>

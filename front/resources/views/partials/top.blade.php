<div class="top">
    <picture class="top__picture">
        <source srcset="{{ asset("assets/img/$image.jpg") }}" media="(min-width: 768px)">
        <img class="top__img" src="{{ asset("assets/img/$image--xs.jpg") }}" alt="Домики" width="720" height="902">
    </picture>
    <div class="container top__container">
        <h1 class="reset top__title">{{ $title }}</h1>
        @if($top_text)
            <span class="top__text">
                {{ $top_text }}
            </span>
        @endif
    </div>
</div>

<!-- Обратный звонок-->
<div class="modal modal--center" id="callback-modal">
    <div class="modal__overlay" data-modal-close=""></div>
    <div class="modal__inner modal__inner--center modal__inner--medium">
        <div class="modal__head">
            <div class="modal__title">Заказать обратный звонок</div>
            <button class="modal__close modal__close--center" aria-label="Закрыть модальное окно" data-modal-close=""></button>
        </div>
        <div class="modal__content">
            <div class="modal__text">Оставьте свой номер телефона и наш менеджер свяжется с вами в ближайшее время</div>
            <form class="form form--feedback ajax-form" action="{{ route('callbackSend') }}" id="callback-form" method="post">
                @csrf
                <div class="field">
                    <input class="field__input field__input--default  field__input--gray" id="callback-name" type="text" name="name" required="required"/>
                    <label class="field__label field__label--gray" for="callback-name">Ваше имя</label>
                </div>
                <div class="field">
                    <input class="field__input field__input--default  field__input--gray" id="callback-phone" type="tel" name="phone" required="required"/>
                    <label class="field__label field__label--gray" for="callback-phone">Телефон</label>
                </div>
                <button class="button button--large button--yellow button--full button--text-gray" type="submit"><span class="button__text">Жду звонка</span>
                </button>
            </form>
            <div class="form-message form-message--gray">
                Нажимая на кнопку вы даете согласие на обработку персональных данных и соглашаетесь с 
                <a href="#" title="Соглашение на обработку персональных данных">Политикой конфиденциальности</a>
            </div>
        </div>
    </div>
</div>
<!-- Спасибо за заявку-->
<div class="modal modal--center" id="thanks-modal">
    <div class="modal__overlay" data-modal-close=""></div>
    <div class="modal__inner modal__inner--center modal__inner--small">
        <div class="modal__head">
            <div class="modal__title">Спасибо за вашу заявку!</div>
            <button class="modal__close modal__close--center" aria-label="Закрыть модальное окно" data-modal-close=""></button>
        </div>
        <div class="modal__content">
            <div class="modal__text">Наши менеджеры свяжутся с вами в ближайшее время. <br/> Нет времени ждать? Перезвоните нам.</div>
            <div class="modal__contacts">
                <div class="modal__contacts-wrapper">
                    <div class="modal__contacts-links">
                        <a class="modal__contacts-item" href="tel:89883429798" title="Позвонить нам">+7 988 342-97-98</a>
                    </div>
                    <a class="button button--large button--yellow-two button--full" title="На главную" href="{{ route('home') }}"><span class="button__text">На главную</span></a>
                </div>
            </div>
        </div>
    </div>
</div>

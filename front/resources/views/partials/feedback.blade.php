<section class="section section--default feedback">
    <div class="feedback__wrap">
        <h4 class="feedback__title reset-mt">Остались вопросы?</h4>
        <p class="feedback__text">Оставьте заявку и мы свяжемся с вами в ближайшее время для консультации</p>
        <form class="form form--feedback ajax-form" id="feedback-form" action="{{ route('callbackSend') }}" method="post">
            <div class="field">
                <input class="field__input field__input--default  field__input--white" id="feedback-name" type="text" name="name" required="required"/>
                <label class="field__label field__label--white" for="feedback-name">Ваше имя</label>
            </div>
            <div class="field">
                <input class="field__input field__input--default  field__input--white" id="feedback-phone" type="tel" name="phone" required="required"/>
                <label class="field__label field__label--white" for="feedback-phone">Телефон</label>
            </div>
            <button class="button button--large button--yellow button--full button--text-white" id="feedback-button" type="submit">
                <span class="button__text">Жду звонка</span>
            </button>
            <span class="feedback__thanks--text" id="thanks-text">Спасибо, с вами в скором времени свяжуться</span>
        </form>
        <div class="form-message form-message--white">Нажимая на кнопку вы даете согласие на обработку персональных данных и соглашаетесь с 
            <a href="#" title="Соглашение на обработку персональных данных">Политикой конфиденциальности</a>
        </div>
    </div>
</section>
<style>
    .feedback__thanks--text{
        width: 100%;
        display: none;
        align-items: center;
        justify-content: center;
        font-size: 22px;
        font-weight: bold;
    }
</style>


<ol class="breadcrumbs" itemscope="" itemtype="https://schema.org/BreadcrumbList">
    <li class="breadcrumbs__item" itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a class="breadcrumbs__link" itemprop="item" href="{{ route('home') }}" title="Главная">
            <span itemprop="name">Главная</span>
            <meta itemprop="position" content="0"/>
        </a>
    </li>
    <li class="breadcrumbs__item" itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <a class="breadcrumbs__link" itemprop="item" href="{{ route('services.index') }}" title="Услуги">
            <span itemprop="name">Услуги</span>
            <meta itemprop="position" content="1"/>
        </a>
    </li>
    <li class="breadcrumbs__item" itemprop="itemListElement" itemscope="" itemtype="https://schema.org/ListItem">
        <span>{{ $title }}</span>
    </li>
</ol>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>{{ $meta['title'] }}</title>
    <meta name="keywords" content="{{ $meta['keywords'] }}">
    <meta name="description" content="{{ $meta['description'] }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.min.css?ver='.hash_file('md5','assets/css/style.css')) }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" type="image/png" href="{{ asset("assets/img/favicon.png") }}">
    <link rel="icon" href="{{ asset("assets/img/favicon.svg") }}" type="image/svg+xml">
    <!-- Fonts-->
    <link rel="preload" href="{{ asset("assets/fonts/Lora-Regular.woff2") }}" type="font/woff2" as="font" crossorigin="anonymous">
    <link rel="preload" href="{{ asset("assets/fonts/Lora-Medium.woff2") }}" type="font/woff2" as="font" crossorigin="anonymous">
    <link rel="preload" href="{{ asset("assets/fonts/Lora-SemiBold.woff2") }}" type="font/woff2" as="font" crossorigin="anonymous">
    <link rel="preload" href="{{ asset("assets/fonts/Lora-Bold.woff2") }}" type="font/woff2" as="font" crossorigin="anonymous">
    <link rel="preload" href="{{ asset("assets/fonts/Philosopher-Bold.woff2") }}" type="font/woff2" as="font" crossorigin="anonymous">
    <!-- Facebook meta tags-->
    <meta property="og:url" content="">
    <meta property="og:type" content="website">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <!-- Twitter meta tags-->
    <meta property="twitter:domain" content="">
    <meta property="twitter:url" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">

    @if(config('app.env') == 'production')
        <script type="text/javascript" >
            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                m[i].l=1*new Date();
                for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
                k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

            ym(94774804, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
            });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/94774804" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    @endif
</head>
<body>
<div class="wrapper">
    <!-- Header-->
    @include('partials.header')

    {{--  Callback   --}}
    @include('partials.callback')

    <main>
        @yield('content')
    </main>

    <!-- Footer-->
    @include('partials.footer')
</div>
<script src="{{ asset("assets/js/app.js?ver=" . hash_file('md5', 'assets/js/app.js')) }}"></script>
</body>
</html>

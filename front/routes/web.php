<?php

use App\Http\Controllers\PremisesController;
use App\Http\Controllers\ServicesController;
use App\Http\Controllers\StartController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', static function () {
    return view('home');
});

Route::get('/', [StartController::class, 'start'])->name('home');
Route::get('/gallery', [StartController::class, 'gallery'])->name('gallery');
Route::get('/rules', [StartController::class, 'rules'])->name('rules');
Route::get('/booking', [StartController::class, 'booking'])->name('booking');
Route::get('/contacts', [StartController::class, 'contacts'])->name('contacts');

Route::group(['prefix' => '', 'as' => 'premises.'], function ($router) {
    Route::get('/houses', [PremisesController::class, 'houses'])->name('houses');
    Route::get('/gazebos', [PremisesController::class, 'gazebos'])->name('gazebos');
});

Route::group(['prefix' => 'services', 'as' => 'services.'], function ($router) {
    $router->get('/', [ServicesController::class, 'services'])->name('index');
    $router->get('/fishing', [ServicesController::class, 'fishing'])->name('fishing');
    $router->get('/pool', [ServicesController::class, 'pool'])->name('pool');
    $router->get('/family-holiday', [ServicesController::class, 'family'])->name('family');
    $router->get('/birthday', [ServicesController::class, 'birthday'])->name('birthday');
    $router->get('/corporate', [ServicesController::class, 'corporate'])->name('corporate');
});

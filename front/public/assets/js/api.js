const feedback = document.querySelector('#feedback-form'); // Выберите форму
const booking = document.querySelector('#booking-form'); // Выберите форму
const button = document.querySelector('#feedback-button'); // Выберите Кнопку отправки
const text = document.querySelector('#thanks-text'); // Выберите Текст вместо кнопки
const form = document.querySelector('#callback-form'); // Выберите форму
const modal = document.querySelector('#thanks-modal'); // Выберите модальное окно спасибо

form.addEventListener('submit', async (e) => {
    e.preventDefault();

    const name = form.querySelector('#callback-name').value;
    const phone = form.querySelector('#callback-phone').value;

    // Отправка данных формы на сервер с помощью Fetch API
    const response = await fetch('/api/callbackSend', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ name, phone })
    });

    // Проверка успешного ответа от сервера
    if (response.ok) {
        const data = await response.json();

        // Скрытие формы
        form.style.display = 'none';

        // Открытие модального окна спасибо
        modal.style.display = 'block';
    }
});

feedback.addEventListener('submit', async (e) => {
    e.preventDefault();

    const name = feedback.querySelector('#feedback-name').value;
    const phone = feedback.querySelector('#feedback-phone').value;

    // Отправка данных формы на сервер с помощью Fetch API
    const response = await fetch('/api/callbackSend', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ name, phone })
    });

    // Проверка успешного ответа от сервера
    if (response.ok) {
        const data = await response.json();

        // Скрытие формы
        button.style.display = 'none';

        // Открытие модального окна спасибо
        text.style.display = 'flex';
    }
});

booking.addEventListener('submit', async (e) => {
    e.preventDefault();

    const name = booking.querySelector('#booking-name').value;
    const phone = booking.querySelector('#booking-phone').value;
    const checkIn = booking.querySelector('#booking-check-in').value;
    const checkOut = booking.querySelector('#booking-check-out').value;
    const adults = booking.querySelector('#booking-adults').value;
    const children = booking.querySelector('#booking-children').value;

    // Отправка данных формы на сервер с помощью Fetch API
    const response = await fetch('/api/bookingSend', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ name, phone, checkIn, checkOut, adults, children})
    });

    // Проверка успешного ответа от сервера
    if (response.ok) {
        const data = await response.json();

        // Открытие модального окна спасибо
        modal.style.display = 'block';
    }
});


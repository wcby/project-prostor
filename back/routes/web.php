<?php

use App\Http\Controllers\AdditionalController;
use App\Http\Controllers\AdditionalTypeController;
use App\Http\Controllers\AjaxController;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\CallbackController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CategoryPremiseController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\CommonController;
use App\Http\Controllers\DownPhotoController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\GazeboController;
use App\Http\Controllers\HouseController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\MenuItemController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\PremiseController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\StartController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\StyleGuideController;
use App\Http\Controllers\TypeGalleryController;
use App\Http\Controllers\TypePremiseController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'auth'], static function ($router) {
    // Authentication Routes...
    $router->get('login', [LoginController::class, 'showLoginForm'])->name('login');
    $router->post('login', [LoginController::class, 'login']);
    $router->any('logout', [LoginController::class, 'logout'])->name('logout');

    // Password Reset Routes...
    // Register the typical reset password routes for an application.
    $router->get('password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.request');
    $router->post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');
    $router->get('password/reset/{token}', [ForgotPasswordController::class, 'showResetForm'])->name('password.reset');
    $router->post('password/reset', [ForgotPasswordController::class, 'reset'])->name('password.update');
});

Route::group(['prefix' => '', 'middleware' => ['auth', 'active']], static function ($router) {
    $router->get('/', StartController::class)->name('start');
    $router->get('/dashboard', StartController::class)->name('dashboard');

    // Баннеры
    $router->any('/banners/filter', [BannerController::class, 'filter'])->name('banners.filter');
    $router->get('/banners/restore', [BannerController::class, 'restore'])->name('banners.restore');
    $router->post('/banners/restore', [BannerController::class, 'restore'])->name('banners.restoring');
    $router->delete('/banners', [BannerController::class, 'destroy'])->name('banners.destroy');
    $router->resource('banners', BannerController::class, ['except' => ['destroy']]);

    // Страницы
    $router->any('/pages/filter', [PageController::class, 'filter'])->name('pages.filter');
    $router->get('/pages/restore', [PageController::class, 'restore'])->name('pages.restore');
    $router->post('/pages/restore', [PageController::class, 'restore'])->name('pages.restoring');
    $router->delete('/pages', [PageController::class, 'destroy'])->name('pages.destroy');
    $router->resource('pages', PageController::class, ['except' => ['destroy']]);

    // Категории
    $router->any('/categories/filter', [CategoryController::class, 'filter'])->name('categories.filter');
    $router->get('/categories/restore', [CategoryController::class, 'restore'])->name('categories.restore');
    $router->post('/categories/restore', [CategoryController::class, 'restore'])->name('categories.restoring');
    $router->delete('/categories', [CategoryController::class, 'destroy'])->name('categories.destroy');
    $router->resource('categories', CategoryController::class, ['except' => ['destroy']]);

    // Статьи
    $router->any('/articles/filter', [ArticleController::class, 'filter'])->name('articles.filter');
    $router->get('/articles/restore', [ArticleController::class, 'restore'])->name('articles.restore');
    $router->post('/articles/restore', [ArticleController::class, 'restore'])->name('articles.restoring');
    $router->delete('/articles', [ArticleController::class, 'destroy'])->name('articles.destroy');
    $router->resource('articles', ArticleController::class, ['except' => ['destroy']]);

    // Пользователи
    $router->any('/users/filter', [UserController::class, 'filter'])->name('users.filter');
    $router->get('/profile', [UserController::class, 'profile'])->name('users.profile');
    $router->put('/profile/update', [UserController::class, 'profileUpdate'])->name('users.profile.update');
    $router->get('/users/restore', [UserController::class, 'restore'])->name('users.restore');
    $router->post('/users/restore', [UserController::class, 'restore'])->name('users.restoring');
    $router->delete('/users', [UserController::class, 'destroy'])->name('users.destroy');
    $router->resource('users', UserController::class, ['except' => ['destroy']]);

    // Роли
    $router->any('/roles/filter', [RoleController::class, 'filter'])->name('roles.filter');
    $router->get('/roles/restore', [RoleController::class, 'restore'])->name('roles.restore');
    $router->post('/roles/restore', [RoleController::class, 'restore'])->name('roles.restoring');
    $router->delete('/roles', [RoleController::class, 'destroy'])->name('roles.destroy');
    $router->resource('roles', RoleController::class, ['except' => ['destroy']]);

    // Разрешения
    $router->any('/permissions/filter', [PermissionController::class, 'filter'])->name('permissions.filter');
    $router->resource('permissions', PermissionController::class, ['only' => ['index']]);

    // Меню
    $router->any('/menus/filter', [MenuController::class, 'filter'])->name('menus.filter');
    $router->get('/menus/restore', [MenuController::class, 'restore'])->name('menus.restore');
    $router->post('/menus/restore', [MenuController::class, 'restore'])->name('menus.restoring');
    $router->delete('/menus', [MenuController::class, 'destroy'])->name('menus.destroy');
    $router->resource('menus', MenuController::class, ['except' => ['destroy']]);

    $router->group(['prefix' => 'menus', 'as' => 'menus.'], function ($router) {
        // Подменю
        $router->any('{menuId}/menu-items/filter', [MenuItemController::class, 'filter'])->name('menu-items.filter');
        $router->get('{menuId}/menu-items/restore', [MenuItemController::class, 'restore'])->name('menu-items.restore');
        $router->post('{menuId}/menu-items/restore', [MenuItemController::class, 'restore'])->name('menu-items.restoring');
        $router->delete('{menuId}/menu-items', [MenuItemController::class, 'destroy'])->name('menu-items.destroy');
        $router->resource('{menuId}/menu-items', MenuItemController::class, ['except' => ['destroy']]);
    });

    //Front

    $router->any('/additionals/filter', [AdditionalController::class, 'filter'])->name('additionals.filter');
    $router->get('/additionals/restore', [AdditionalController::class, 'restore'])->name('additionals.restore');
    $router->post('/additionals/restore', [AdditionalController::class, 'restore'])->name('additionals.restoring');
    $router->delete('/additionals', [AdditionalController::class, 'destroy'])->name('additionals.destroy');
    $router->resource('additionals', AdditionalController::class, ['except' => ['destroy']]);

    $router->any('/additional-types/filter', [AdditionalTypeController::class, 'filter'])->name('additional-types.filter');
    $router->get('/additional-types/restore', [AdditionalTypeController::class, 'restore'])->name('additional-types.restore');
    $router->post('/additional-types/restore', [AdditionalTypeController::class, 'restore'])->name('additional-types.restoring');
    $router->delete('/additional-types', [AdditionalTypeController::class, 'destroy'])->name('additional-types.destroy');
    $router->resource('additional-types', AdditionalTypeController::class, ['except' => ['destroy']]);

    $router->any('/houses/filter', [HouseController::class, 'filter'])->name('houses.filter');
    $router->get('/houses/restore', [HouseController::class, 'restore'])->name('houses.restore');
    $router->post('/houses/restore', [HouseController::class, 'restore'])->name('houses.restoring');
    $router->delete('/houses', [HouseController::class, 'destroy'])->name('houses.destroy');
    $router->resource('houses', HouseController::class, ['except' => ['destroy']]);

    $router->any('/gazebos/filter', [GazeboController::class, 'filter'])->name('gazebos.filter');
    $router->get('/gazebos/restore', [GazeboController::class, 'restore'])->name('gazebos.restore');
    $router->post('/gazebos/restore', [GazeboController::class, 'restore'])->name('gazebos.restoring');
    $router->delete('/gazebos', [GazeboController::class, 'destroy'])->name('gazebos.destroy');
    $router->resource('gazebos', GazeboController::class, ['except' => ['destroy']]);

    $router->any('/galleries/filter', [GalleryController::class, 'filter'])->name('galleries.filter');
    $router->get('/galleries/restore', [GalleryController::class, 'restore'])->name('galleries.restore');
    $router->post('/galleries/restore', [GalleryController::class, 'restore'])->name('galleries.restoring');
    $router->delete('/galleries', [GalleryController::class, 'destroy'])->name('galleries.destroy');
    $router->resource('galleries', GalleryController::class, ['except' => ['destroy']]);

    // Управление Элементами сайта

    $router->any('/sliders/filter', [SliderController::class, 'filter'])->name('sliders.filter');
    $router->get('/sliders/restore', [SliderController::class, 'restore'])->name('sliders.restore');
    $router->post('/sliders/restore', [SliderController::class, 'restore'])->name('sliders.restoring');
    $router->delete('/sliders', [SliderController::class, 'destroy'])->name('sliders.destroy');
    $router->resource('sliders', SliderController::class, ['except' => ['destroy']]);

    $router->any('/down-photos/filter', [DownPhotoController::class, 'filter'])->name('down-photos.filter');
    $router->get('/down-photos/restore', [DownPhotoController::class, 'restore'])->name('down-photos.restore');
    $router->post('/down-photos/restore', [DownPhotoController::class, 'restore'])->name('down-photos.restoring');
    $router->delete('/down-photos', [DownPhotoController::class, 'destroy'])->name('down-photos.destroy');
    $router->resource('down-photos', DownPhotoController::class, ['except' => ['destroy']]);


    //CRM

    //Помещений
    $router->any('/premises/filter', [PremiseController::class, 'filter'])->name('premises.filter');
    $router->get('/premises/restore', [PremiseController::class, 'restore'])->name('premises.restore');
    $router->post('/premises/restore', [PremiseController::class, 'restore'])->name('premises.restoring');
    $router->delete('/premises', [PremiseController::class, 'destroy'])->name('premises.destroy');
    $router->resource('premises', PremiseController::class, ['except' => ['destroy']]);

    //Статус
    $router->any('/statuses/filter', [StatusController::class, 'filter'])->name('statuses.filter');
    $router->get('/statuses/restore', [StatusController::class, 'restore'])->name('statuses.restore');
    $router->post('/statuses/restore', [StatusController::class, 'restore'])->name('statuses.restoring');
    $router->delete('/statuses', [StatusController::class, 'destroy'])->name('statuses.destroy');
    $router->resource('statuses', StatusController::class, ['except' => ['destroy']]);

    //Оплата
    $router->any('/payments/filter', [PaymentController::class, 'filter'])->name('payments.filter');
    $router->get('/payments/restore', [PaymentController::class, 'restore'])->name('payments.restore');
    $router->post('/payments/restore', [PaymentController::class, 'restore'])->name('payments.restoring');
    $router->delete('/payments', [PaymentController::class, 'destroy'])->name('payments.destroy');
    $router->resource('payments', PaymentController::class, ['except' => ['destroy']]);

    //Клиенты
    $router->any('/clients/filter', [ClientController::class, 'filter'])->name('clients.filter');
    $router->get('/clients/restore', [ClientController::class, 'restore'])->name('clients.restore');
    $router->post('/clients/restore', [ClientController::class, 'restore'])->name('clients.restoring');
    $router->delete('/clients', [ClientController::class, 'destroy'])->name('clients.destroy');
    $router->resource('clients', ClientController::class, ['except' => ['destroy']]);

    //Заявки
    $router->any('/callbacks/filter', [CallbackController::class, 'filter'])->name('callbacks.filter');
    $router->get('/callbacks/restore', [CallbackController::class, 'restore'])->name('callbacks.restore');
    $router->post('/callbacks/restore', [CallbackController::class, 'restore'])->name('callbacks.restoring');
    $router->delete('/callbacks', [CallbackController::class, 'destroy'])->name('callbacks.destroy');
    $router->resource('callbacks', CallbackController::class, ['except' => ['destroy']]);

    //Типы помещений
    $router->any('/type-premises/filter', [TypePremiseController::class, 'filter'])->name('type-premises.filter');
    $router->get('/type-premises/restore', [TypePremiseController::class, 'restore'])->name('type-premises.restore');
    $router->post('/type-premises/restore', [TypePremiseController::class, 'restore'])->name('type-premises.restoring');
    $router->delete('/type-premises', [TypePremiseController::class, 'destroy'])->name('type-premises.destroy');
    $router->resource('type-premises', TypePremiseController::class, ['except' => ['destroy']]);


    //Категории помещений
    $router->any('/category-premises/filter', [CategoryPremiseController::class, 'filter'])->name('category-premises.filter');
    $router->get('/category-premises/restore', [CategoryPremiseController::class, 'restore'])->name('category-premises.restore');
    $router->post('/category-premises/restore', [CategoryPremiseController::class, 'restore'])->name('category-premises.restoring');
    $router->delete('/category-premises', [CategoryPremiseController::class, 'destroy'])->name('category-premises.destroy');
    $router->resource('category-premises', CategoryPremiseController::class, ['except' => ['destroy']]);

    //Заявки
    $router->any('/applications/filter', [ApplicationController::class, 'filter'])->name('applications.filter');
    $router->get('/applications/restore', [ApplicationController::class, 'restore'])->name('applications.restore');
    $router->post('/applications/restore', [ApplicationController::class, 'restore'])->name('applications.restoring');
    $router->delete('/applications', [ApplicationController::class, 'destroy'])->name('applications.destroy');
    $router->resource('applications', ApplicationController::class, ['except' => ['destroy']]);

    // Система
    $router->group(['prefix' => 'system'], function ($router) {
        // Настройки
        $router->any('/settings/filter', [SettingController::class, 'filter'])->name('settings.filter');
        $router->get('/settings/restore', [SettingController::class, 'restore'])->name('settings.restore.list');
        $router->post('/settings/restore', [SettingController::class, 'restore'])->name('settings.restore');
        $router->delete('/settings', [SettingController::class, 'destroy'])->name('settings.destroy');
        $router->resource('settings', SettingController::class, ['except' => ['destroy']]);
    });

    $router->group(['prefix' => 'style-guide'], function ($router) {
        $router->get('/', StyleGuideController::class)->name('style-guide');
        $router->get('/forms', [StyleGuideController::class, 'forms'])->name('forms');
        $router->get('/tables', [StyleGuideController::class, 'tables'])->name('tables');
    });

    // Ajax
    $router
        ->controller(AjaxController::class)
        ->prefix('ajax')
        ->group(function ($router) {
            $router->any('/{action}', 'index');
        })
    ;

    // Сброс кук
    Route::get('/remove-cookies', [CommonController::class, 'removeCookies']);
});

<?php
return [
    '#aa7941' => 'Эконом',
    '#fefb00' => 'Стандарт',
    '#ff9200' => 'Стандарт+',
    '#ffff00' => 'Гостевой Дом',
    '#ff00ff' => 'Гостевой Дом-2',
    '#00ffff' => 'Банкетная',
    '#0432ff' => 'Беседка',
];

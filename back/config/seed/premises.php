<?php
return [
    'HO-121' => [
        'type_premise_id' => 2,
        'category_premise_id' => 1,
        'cost' => 2000,
        'is_active' => 1,
    ],
    'HO-122' => [
        'type_premise_id' => 2,
        'category_premise_id' => 1,
        'cost' => 2000,
        'is_active' => 1,
    ],
    'HO-123' => [
        'type_premise_id' => 2,
        'category_premise_id' => 2,
        'cost' => 2000,
        'is_active' => 1,
    ],
    'HO-124' => [
        'type_premise_id' => 2,
        'category_premise_id' => 3,
        'cost' => 2000,
        'is_active' => 1,
    ],
    'Номер 101' => [
        'type_premise_id' => 2,
        'category_premise_id' => 4,
        'cost' => 2000,
        'is_active' => 1,
    ],
    'Номер 102' => [
        'type_premise_id' => 2,
        'category_premise_id' => 4,
        'cost' => 2000,
        'is_active' => 1,
    ],
    'Номер 103' => [
        'type_premise_id' => 2,
        'category_premise_id' => 4,
        'cost' => 2000,
        'is_active' => 1,
    ],
    'Номер 11' => [
        'type_premise_id' => 2,
        'category_premise_id' => 5,
        'cost' => 2000,
        'is_active' => 1,
    ],
    'Номер 12' => [
        'type_premise_id' => 2,
        'category_premise_id' => 5,
        'cost' => 2000,
        'is_active' => 1,
    ],
    'Беседка 1' => [
        'type_premise_id' => 1,
        'category_premise_id' => 7,
        'cost' => 2000,
        'is_active' => 1,
    ],
    'Беседка 2' => [
        'type_premise_id' => 1,
        'category_premise_id' => 7,
        'cost' => 2000,
        'is_active' => 1,
    ],
    'Беседка 3' => [
        'type_premise_id' => 1,
        'category_premise_id' => 7,
        'cost' => 2000,
        'is_active' => 1,
    ],
    'Банкетная 1' => [
        'type_premise_id' => 1,
        'category_premise_id' => 6,
        'cost' => 2000,
        'is_active' => 1,
    ],
    'Банкетная 2' => [
        'type_premise_id' => 1,
        'category_premise_id' => 6,
        'cost' => 2000,
        'is_active' => 1,
    ],
];

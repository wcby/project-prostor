<?php

return [
    // Поля что выводятся изначально в списке
    'fields_selected_default' => [
        'id',
        'avatar',
        'name',
        'email',
        'is_active',
    ],
    // Поля которые можно вывести
    'fields_for_showing' => [
        'avatar',
        'name',
        'email',
        'role_id',
        'is_active',
    ],
    // Миниатюры для фото если есть
    'miniatures' => [
        "thumbnail" => [
            "width" => 200,
            "height" => 200,
        ],
    ],
    // Вывод типов полей в списке
    'fields_type' => [
        // Вывод по ключу из массива
        'config' => [],
        // Вывод отношения
        'relationships' => [
            'role_id' => 'role',
        ],
        // Картинки
        'images' => [
            'avatar',
        ],
        // Ссылки
        'links' => [
            'id',
            'avatar',
            'email',
        ],
        // checkbox
        'checkbox' => [
            'is_active'
        ],
        // Класс full
        'full' => [
            'email',
        ],
        // Сортировка по полям
        'sorting' => [
            'id',
            'email',
        ],
    ],
];

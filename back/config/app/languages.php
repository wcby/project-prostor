<?php

return [
    // Поиск по полям
    'searching' => ['id', 'name'],
    // Поля что выводятся изначально в списке
    'fields_selected_default' => [
        'id',
        'locale',       // Языковой стандарт
        'name',         // Наименование языка
    ],
    // Поля которые можно вывести
    'fields_for_showing' => [
        'id',           // ID
        'locale',       // Языковой стандарт
        'name',         // Наименование языка
        'code',         // Код языка
        'is_default',   // Язык по умолчанию
        'is_active',    // Статус языка
    ],
    // Миниатюры для фото если есть
    'miniatures' => [],
    // Вывод типов полей в списке
    'fields_type' => [
        // Вывод по ключу из массива
        'config' => [],
        // Вывод отношения
        'relationships' => [],
        // Картинки
        'images' => [],
        // Ссылки
        'links' => [
            'id',
            'locale',
            'name',
        ],
        // checkbox
        'checkbox' => [
            'is_active'
        ],
        // Класс full
        'full' => [
            'locale',
            'name',
        ],
        // Сортировка по полям
        'sorting' => [
            'id',
            'locale',
            'name',
        ],
    ],
];

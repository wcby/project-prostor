<?php

return [
    // Поля что выводятся изначально в списке
    'fields_selected_default' => [
        'id',
        'name',
        'is_active',
    ],
    // Поля которые можно вывести
    'fields_for_showing' => [
        'name',
        'is_active',
    ],
    // Миниатюры для фото если есть
    'miniatures' => [
        'large' => [
            'width' => 1500,
            'height' => 800,
            "make" => false,
            "crop" => true
        ],
        'medium' => [
            'width' => 870,
            'height' => 489,
            "make" => false,
            "crop" => true
        ],
        'small' => [
            'width' => 660,
            'height' => 440,
            "make" => true,
            "crop" => true
        ],
        'thumbnail' => [
            'width' => 200,
            'height' => 200,
            "make" => false,
            "crop" => true
        ],
    ],
    // Вывод типов полей в списке
    'fields_type' => [
        // Вывод по ключу из массива
        'config' => [],
        // Вывод отношения
        'relationships' => [],
        // Картинки
        'images' => [],
        // Ссылки
        'links' => [
            'id',
            'name',
        ],
        // checkbox
        'checkbox' => [
            'is_active'
        ],
        // Класс full
        'full' => [
            'name',
        ],
        // Сортировка по полям
        'sorting' => [
            'id',
            'name',
        ],
    ],
];

<?php

return [
    // Поиск по полям
    'searching' => ['id', 'name'],
    // Поля что выводятся изначально в списке
    'fields_selected_default' => [
        'id',
        'name',
        'is_active',
    ],
    // Поля которые можно вывести
    'fields_for_showing' => [
        'name',             // Название
        'is_active',        // Статус
    ],
    // Миниатюры для фото если есть
    'miniatures' => [],
    // Вывод типов полей в списке
    'fields_type' => [
        // Вывод по ключу из массива
        'config' => [
            'parent_id' => 'menus'
        ],
        // Вывод отношения
        'relationships' => [],
        // Картинки
        'images' => [],
        // Ссылки
        'links' => [
            'id',
            'name',
        ],
        // checkbox
        'checkbox' => [
            'is_active'
        ],
        // Класс full
        'full' => [
            'name',
        ],
        // Сортировка по полям
        'sorting' => [
            'id',
            'name',
        ],
    ],
];

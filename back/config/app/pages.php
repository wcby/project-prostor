<?php

return [
    // Поиск по полям
    'searching' => ['id', 'name'],
    // Поля что выводятся изначально в списке
    'fields_selected_default' => [
        'id',
        'name',
        'url',
        'is_active',
    ],
    // Поля которые можно вывести
    'fields_for_showing' => [
        'name',             // Название
        'url',              // Урл
        'content',          // Текст
        'meta_title',       // Метатег title
        'meta_keywords',    // Метатег keywords
        'meta_description', // Метатег description
        'is_active',        // Статус
    ],
    // Миниатюры для фото если есть
    'miniatures' => [],
    // Вывод типов полей в списке
    'fields_type' => [
        // Вывод по ключу из массива
        'config' => [],
        // Вывод отношения
        'relationships' => [],
        // Картинки
        'images' => [],
        // Ссылки
        'links' => [
            'id',
            'name',
        ],
        // checkbox
        'checkbox' => [
            'is_active'
        ],
        // Класс full
        'full' => [
            'name',
        ],
        // Сортировка по полям
        'sorting' => [
            'id',
            'name',
        ],
    ],
];

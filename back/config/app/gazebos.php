<?php

return [
    // Поля что выводятся изначально в списке
    'fields_selected_default' => [
        'id',
        'name',
        'is_active',
    ],
    // Поля которые можно вывести
    'fields_for_showing' => [
        'name',
        'price',
        'is_active',
    ],
    // Миниатюры для фото если есть
    'miniatures' => [
        'large' => [
            'width' => 870,
            'height' => 489,
            "crop" => false,
        ],
        'medium' => [
            'width' => 660,
            'height' => 440,
            "crop" => true,
        ],
        'small' => [
            'width' => 200,
            'height' => 200,
            "crop" => true,
        ],
        'thumbnail' => [
            'width' => 200,
            'height' => 200,
            "crop" => false,
        ],
    ],
    // Вывод типов полей в списке
    'fields_type' => [
        // Вывод по ключу из массива
        'config' => [],
        // Вывод отношения
        'relationships' => [],
        // Картинки
        'images' => [],
        // Ссылки
        'links' => [
            'id',
            'name',
        ],
        // checkbox
        'checkbox' => [
            'is_active'
        ],
        // Класс full
        'full' => [
            'name',
        ],
        // Сортировка по полям
        'sorting' => [
            'id',
            'name',
        ],
    ],
];

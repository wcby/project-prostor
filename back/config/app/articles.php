<?php

return [
    // Поиск по полям
    'searching' => ['id', 'name'],
    // Поля что выводятся изначально в списке
    'fields_selected_default' => [
        'id',
        'photo',
        'name',
        'is_active',
    ],
    // Поля которые можно вывести
    'fields_for_showing' => [
        'photo',            // Фото
        'name',             // Название
        'url',              // Урл
        'date',             // Дата
        'brief_description',// Краткое описание
        'description',      // Полное описание
        'meta_title',       // Метатег title
        'meta_keywords',    // Метатег keywords
        'meta_description', // Метатег description
        'is_favourite',     // Избранное
        'is_active',        // Статус
    ],
    // Миниатюры для фото если есть
    'miniatures' => [
        "thumbnail" => [
            "width" => 200,
            "height" => 200,
        ],
    ],
    // Вывод типов полей в списке
    'fields_type' => [
        // Вывод по ключу из массива
        'config' => [],
        // Вывод отношения
        'relationships' => [],
        // Картинки
        'images' => [
            'photo',
        ],
        // Ссылки
        'links' => [
            'id',
            'name',
        ],
        // checkbox
        'checkbox' => [
            'is_favourite',
            'is_active',
        ],
        // Класс full
        'full' => [
            'name',
        ],
        // Сортировка по полям
        'sorting' => [
            'id',
            'name',
            'date',
        ],
    ],
];

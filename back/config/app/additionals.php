<?php

return [
    // Поля что выводятся изначально в списке
    'fields_selected_default' => [
        'id',
        'name',
        'is_active',
        'icon'
    ],
    // Поля которые можно вывести
    'fields_for_showing' => [
        'name',             // Название
        'is_active',
        'icon',
        'additional_type_id',
    ],
    // Миниатюры для фото если есть
    'miniatures' => [],
    // Вывод типов полей в списке
    'fields_type' => [
        // Вывод по ключу из массива
        'config' => [],
        // Вывод отношения
        'relationships' => [
            'additional_type_id' => 'additionalType'
        ],
        // Картинки
        'images' => [],
        // Ссылки
        'links' => [
            'id',
            'name',
        ],
        // checkbox
        'checkbox' => [
            'is_active'
        ],
        // Класс full
        'full' => [
            'name',
        ],
        // Сортировка по полям
        'sorting' => [
            'id',
            'name',
        ],
    ],
    'icons' => [
        'single-bed' => 'Раздельные кровати',
        'double-bed' => 'Двухспальная кровать',
        'plan' => 'Гостиная с диваном',
        'arrows-out' => 'Увеличеннпя площадь',
        'kettle' => 'Чайник',
        'tree' => 'Отделка деревом',
        'antenna' => 'Спутниковое ТВ',
        'tv' => 'Телевизор',
        'thermometer' => 'Сплит-система',
        'snowflake' => 'Холодильник',
        'shower-head' => 'Душ',
        'water-drop'=> 'Горячая и холодная вода',
        'toilet' => 'Санузел',
        'mirror' => 'Раковина с зеркалом',
        'hot-pot' => 'Мангал',
        'cloud-rain' => 'Защищена от дождя',
        'alcove' => 'Открытая веранда',
        'microwave-oven' => 'Микроволновка',
        'chef-hat' => 'Кухня',
        'sofa' => 'Диван',
        'table' => 'Столик',
     ]
];

<?php

return [
    // Поиск по полям
    'searching' => ['id', 'name'],
    // Поля что выводятся изначально в списке
    'fields_selected_default' => [
        'id',
        'key',
        'value',
    ],
    // Поля которые можно вывести
    'fields_for_showing' => [
        'id',           // ID
        'key',          // Ключ
        'value',        // Значение
        'description',  // Описание
        'type',         // Тип поля
    ],
    // Миниатюры для фото если есть
    'miniatures' => [
        "thumbnail" => [
            "width" => 200,
            "height" => 200,
        ],
    ],
    // Вывод типов полей в списке
    'fields_type' => [
        // Вывод по ключу из массива
        'config' => [],
        // Вывод отношения
        'relationships' => [],
        // Картинки
        'images' => [],
        // Ссылки
        'links' => [
            'id',
            'key',
            'value',
        ],
        // checkbox
        'checkbox' => [],
        // Класс full
        'full' => [
            'key',
            'value',
        ],
        // Сортировка по полям
        'sorting' => [
            'id',
            'key',
            'value',
        ],
    ],
    // Типы полей
    'types' => [
        'input',
        'textarea',
        'image',
    ],
];

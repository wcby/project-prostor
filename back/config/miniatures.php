<?php

return [
    'large' => [
        'width' => 1500,
        'height' => 800,
        "make" => false,
    ],
    'medium' => [
        'width' => 870,
        'height' => 489,
        "make" => false,
    ],
    'small' => [
        'width' => 660,
        'height' => 440,
        "make" => true,
    ],
    'thumbnail' => [
        'width' => 200,
        'height' => 200,
        "make" => false,
    ],
];

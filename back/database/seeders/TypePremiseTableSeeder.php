<?php

namespace Database\Seeders;

use App\Models\TypePremise;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\ConsoleOutput;

class TypePremiseTableSeeder extends Seeder
{
    protected $actions = [
        'view',
        'create',
        'update',
        'delete',
        'restore',
    ];

    public $output;

    public $model;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            $this->model = (new TypePremise());

            $this->output = new ConsoleOutput();

            DB::table('type_premises')->delete();

            DB::statement("ALTER TABLE `type_premises` AUTO_INCREMENT = 1;");
            $type_premises = config('seed.type_premise');
            foreach($type_premises as $type_premise)
            {
                $this->model->create([
                    "name" => $type_premise,
                    "is_active" => 1,
                ]);
            }


            set_permissions($this->model, $this->output, $this->actions);
        } catch (\Exception $e) {

            $errorMessage = $e->getMessage();
            $this->output->writeln("<error>{$errorMessage}</error> ");
        }
    }
}

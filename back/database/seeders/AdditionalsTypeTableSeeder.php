<?php

namespace Database\Seeders;

use App\Models\AdditionalType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\ConsoleOutput;

class AdditionalsTypeTableSeeder extends Seeder
{
    protected $actions = [
        'create',
        'view',
        'update',
        'delete',
        'restore',
    ];

    public $output;

    public $model;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            $this->model = (new AdditionalType());

            $this->output = new ConsoleOutput();

            DB::table('additional_types')->delete();

            DB::statement("ALTER TABLE `additional_types` AUTO_INCREMENT = 1;");
            $types = config('seed.additional_types');

            foreach($types as $key => $type)
            {
                $this->model->create([
                    "name" => $key,
                    "type_premise_id" => $type,
                    "is_active" => 1,
                ]);
            }


            set_permissions($this->model, $this->output, $this->actions);
        } catch (\Exception $e) {

            $errorMessage = $e->getMessage();
            $this->output->writeln("<error>{$errorMessage}</error> ");
        }
    }
}

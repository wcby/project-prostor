<?php

namespace Database\Seeders;

use App\Models\Application;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Database\Seeder;

class ApplicationTableSeeder extends Seeder
{
    protected $actions = [
        'view',
        'update',
        'delete',
        'restore',
    ];

    public $output;

    public $model;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            $this->model = Application::make();

            $this->output = new ConsoleOutput();

            set_permissions($this->model, $this->output, $this->actions);
        } catch (\Exception $e) {

            $errorMessage = $e->getMessage();
            $this->output->writeln("<error>{$errorMessage}</error> ");
        }
    }
}

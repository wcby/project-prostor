<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
//        $this->call(RolesTableSeeder::class);
//        $this->call(UsersTableSeeder::class);
//        $this->call(PagesTableSeeder::class);
//        $this->call(CategoriesTableSeeder::class);
//        $this->call(ArticlesTableSeeder::class);
//        $this->call(MenusTableSeeder::class);
//        $this->call(SettingsTableSeeder::class);
//        $this->call(StatusTableSeeder::class);
//        $this->call(PaymentTableSeeder::class);
//        $this->call(TypePremiseTableSeeder::class);
//        $this->call(CategoryPermiseTableSeeder::class);
//        $this->call(PermiseTableSeeder::class);
//        $this->call(ApplicationTableSeeder::class);
//        $this->call(ClientTableSeeder::class);
//        $this->call(AdditionalsTypeTableSeeder::class);
//        $this->call(HousesTableSeeder::class);
//        $this->call(GazebosTableSeeder::class);
//        $this->call(AdditionalsTableSeeder::class);
//        $this->call(GalleriesTableSeeder::class);
        $this->call(SlidersTableSeeder::class);
        $this->call(DownPhotosTableSeeder::class);
//        $this->call(CallbacksTableSeeder::class);

        $suRole = Role::firstOrNew(['id' => 1]);

        $permissions = (new Permission())->all()->pluck('id');

        $suRole->permissions()->sync($permissions);
    }
}

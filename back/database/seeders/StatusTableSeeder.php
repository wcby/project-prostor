<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\ConsoleOutput;

class StatusTableSeeder extends Seeder
{
    protected $actions = [
        'view',
        'create',
        'update',
        'delete',
        'restore',
    ];

    public $output;

    public $model;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            $this->model = (new Status());

            $this->output = new ConsoleOutput();

            DB::table('statuses')->delete();

            DB::statement("ALTER TABLE `statuses` AUTO_INCREMENT = 1;");
            $statuses = config('seed.status');

            foreach($statuses as $key => $status)
            {
                $this->model->create([
                    "name" => $status,
                    "is_active" => 1,
                    'slug' => $key,
                ]);
            }


            set_permissions($this->model, $this->output, $this->actions);
        } catch (\Exception $e) {

            $errorMessage = $e->getMessage();
            $this->output->writeln("<error>{$errorMessage}</error> ");
        }
    }
}

<?php

namespace Database\Seeders;

use App\Models\Payment;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\ConsoleOutput;

class PaymentTableSeeder extends Seeder
{
    protected $actions = [
        'view',
        'create',
        'update',
        'delete',
        'restore',
    ];

    public $output;

    public $model;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            $this->model = (new Payment());

            $this->output = new ConsoleOutput();

            DB::table('payments')->delete();

            DB::statement("ALTER TABLE `payments` AUTO_INCREMENT = 1;");
            $payments = config('seed.payment');
            foreach($payments as $payment)
            {
                $this->model->create([
                    "name" => $payment,
                    "is_active" => 1,
                ]);
            }


            set_permissions($this->model, $this->output, $this->actions);
        } catch (\Exception $e) {

            $errorMessage = $e->getMessage();
            $this->output->writeln("<error>{$errorMessage}</error> ");
        }
    }
}

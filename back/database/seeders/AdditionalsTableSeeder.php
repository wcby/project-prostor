<?php

namespace Database\Seeders;


use App\Models\Additional;
use App\Models\AdditionalType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\ConsoleOutput;

class AdditionalsTableSeeder extends Seeder
{
    protected $actions = [
        'create',
        'view',
        'update',
        'delete',
        'restore',
    ];

    public $output;

    public $model;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            $this->model = (new Additional());

            $this->output = new ConsoleOutput();

            DB::table('additionals')->delete();

            DB::statement("ALTER TABLE `additionals` AUTO_INCREMENT = 1;");
            $types = config('seed.additionals');
            foreach($types as $type => $type_array)
            {
                foreach($type_array as $icon=>$name){
                    $this->model->create([
                        "name" => $name,
                        "icon" => $icon,
                        "additional_type_id" => $type,
                        "is_active" => 1,
                    ]);
                }
            }

            set_permissions($this->model, $this->output, $this->actions);
        } catch (\Exception $e) {

            $errorMessage = $e->getMessage();
            $this->output->writeln("<error>{$errorMessage}</error> ");
        }
    }
}

<?php

namespace Database\Seeders;


use App\Models\Slider;
use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

class SlidersTableSeeder extends Seeder
{
    protected $actions = [
        'create',
        'view',
        'update',
        'delete',
        'restore',
    ];

    public $output;

    public $model;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            $this->model = Slider::make();

            $this->output = new ConsoleOutput();

            set_permissions($this->model, $this->output, $this->actions);
        } catch (\Exception $e) {

            $errorMessage = $e->getMessage();
            $this->output->writeln("<error>{$errorMessage}</error> ");
        }
    }
}

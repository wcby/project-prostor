<?php

namespace Database\Seeders;

use App\Models\Premise;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\ConsoleOutput;

class PermiseTableSeeder extends Seeder
{
    protected $actions = [
        'view',
        'create',
        'update',
        'delete',
        'restore',
    ];

    public $output;

    public $model;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            $this->model = Premise::make();

            $this->output = new ConsoleOutput();
            DB::table('premises')->delete();

            DB::statement("ALTER TABLE `premises` AUTO_INCREMENT = 1;");
            $premises = config('seed.premises');
            foreach($premises as $key => $premise)
            {
                $this->model->create([
                    "name" => $key,
                    'type_premise_id' => $premise['type_premise_id'],
                    'category_premise_id' => $premise['category_premise_id'],
                    'cost' => $premise['cost'],
                    'is_active' => $premise['is_active'],
                ]);
            }
            set_permissions($this->model, $this->output, $this->actions);
        } catch (\Exception $e) {

            $errorMessage = $e->getMessage();
            $this->output->writeln("<error>{$errorMessage}</error> ");
        }
    }
}

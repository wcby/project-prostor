<?php
namespace Database\Seeders;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    protected $actions = [
        'view',
        'create',
        'update',
        'delete',
        'restore',
    ];

    public $output;

    public $model;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            $this->model = (new User());

            $this->output = new ConsoleOutput();

            DB::table('users')->delete();

            DB::statement("ALTER TABLE `users` AUTO_INCREMENT = 1;");

            $su = Role::find(1);

            $mg = Role::find(2);

            $this->model->create([
                "email" => "admin@example.com",
                "password" => Hash::make('secret'),
                "role_id" => $su ? $su->id : 1,
                "is_active" => 1,
            ]);

            $this->model->create([
                "email" => "manager@example.com",
                "password" => Hash::make('manager123'),
                "role_id" => $mg ? $mg->id : 2,
                "is_active" => 1,
            ]);

            set_permissions($this->model, $this->output, $this->actions);
        } catch (\Exception $e) {

            $errorMessage = $e->getMessage();
            $this->output->writeln("<error>{$errorMessage}</error> ");
        }
    }
}

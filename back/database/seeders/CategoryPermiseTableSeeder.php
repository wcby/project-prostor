<?php

namespace Database\Seeders;

use App\Models\CategoryPremise;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\ConsoleOutput;

class CategoryPermiseTableSeeder extends Seeder
{
    protected $actions = [
        'view',
        'create',
        'update',
        'delete',
        'restore',
    ];

    public $output;

    public $model;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            $this->model = (new CategoryPremise());

            $this->output = new ConsoleOutput();

            DB::table('category_premises')->delete();

            DB::statement("ALTER TABLE `category_premises` AUTO_INCREMENT = 1;");
            $category_premises = config('seed.category_premise');
            foreach($category_premises as $key => $category_premise)
            {
                $this->model->create([
                    "name" => $category_premise,
                    "color" => $key,
                    "is_active" => 1,
                ]);
            }


            set_permissions($this->model, $this->output, $this->actions);
        } catch (\Exception $e) {

            $errorMessage = $e->getMessage();
            $this->output->writeln("<error>{$errorMessage}</error> ");
        }
    }
}

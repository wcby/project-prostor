<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('additionals', function (Blueprint $table) {
            $table->id();
            $table
                ->string('name')
                ->comment('Название');
            $table
                ->string('icon')
                ->comment('Иконка');
            $table
                ->unsignedBigInteger('additional_type_id')
                ->nullable()
                ->comment('Тип');
            $table
                ->unsignedTinyInteger('is_active')
                ->nullable()
                ->default(0)
                ->comment('Статус');
            $table->timestamps();
            $table->softDeletes();

            $table
                ->foreign('additional_type_id')
                ->references('id')
                ->on('additional_types')
                ->onDelete('set null')
            ;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('additionals');
    }
};

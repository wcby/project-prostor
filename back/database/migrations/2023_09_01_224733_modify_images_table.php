<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('images', function (Blueprint $table) {
            $table->unsignedInteger('image_id')->nullable()->change();
            $table->renameColumn('image_id', 'parent_id');
            $table->renameColumn('image_type', 'type');
            $table->string('name')->nullable();
            $table->dropColumn(['position']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('images', function (Blueprint $table) {
            $table->renameColumn('parent_id', 'image_id');
            $table->renameColumn('type', 'image_type');
            $table->dropColumn([
                'name',
                'path'
            ]);
        });
    }
};

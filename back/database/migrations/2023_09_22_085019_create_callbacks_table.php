<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('callbacks', function (Blueprint $table) {
            $table->id();
            $table
                ->string('title')
                ->comment('Тип заявки');
            $table
                ->string('name')
                ->comment('ФИО');
            $table
                ->string('phone')
                ->comment('Телефон');;
            $table
                ->date('check_in')
                ->default(date('Y-m-d'))
                ->nullable()
                ->comment('Заезд');
            $table
                ->date('check_out')
                ->default(date('Y-m-d'))
                ->nullable()
                ->comment('Выезд');
            $table
                ->integer('adult')
                ->comment('Взрослые')
                ->nullable();;
            $table
                ->integer('children')
                ->comment('Дети')
                ->nullable();
            $table
                ->unsignedTinyInteger('is_active')
                ->nullable()
                ->default(0)
                ->comment('Статус');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('callbacks');
    }
};

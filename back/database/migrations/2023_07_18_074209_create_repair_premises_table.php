<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('repair_premises', function (Blueprint $table) {
            $table->id();
            $table
                ->unsignedBigInteger('premise_id')
                ->nullable()
                ->comment('Помещение');
            $table
                ->date('check_in')
                ->default(date('Y-m-d'))
                ->comment('Начало ремонта');
            $table
                ->date('check_out')
                ->default(date('Y-m-d'))
                ->comment('Окончание ремонта');
            $table
                ->unsignedTinyInteger('is_active')
                ->nullable()
                ->default(0)
                ->comment('Статус');

            $table
                ->foreign('premise_id')
                ->references('id')
                ->on('premises')
                ->onDelete('set null')
            ;

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('repairs');
    }
};

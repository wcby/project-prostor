<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('articles', function (Blueprint $table) {

            $table->id();
            $table
                ->string('name')
                ->comment('Название');

            $table
                ->string('slug')
                ->comment('Ссылка');

            $table
                ->longText('brief_description')
                ->nullable()
                ->comment('Краткое описание');

            $table
                ->longText('description')
                ->nullable()
                ->comment('Описание');

            $table
                ->timestamp('date')
                ->useCurrent()
                ->comment('Дата');

            $table
                ->string('photo')
                ->nullable()
                ->comment('Фото');

            $table
                ->tinyInteger('is_favourite')
                ->nullable()
                ->default(0)
                ->comment('Избранное');

            $table
                ->unsignedTinyInteger('is_active')
                ->nullable()
                ->default(0)
                ->comment('Статус');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('articles');
    }
};

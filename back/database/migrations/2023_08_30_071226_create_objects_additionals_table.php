<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('objects_additionals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('house_id')->nullable();
            $table->unsignedBigInteger('gazebo_id')->nullable();
            $table
                ->unsignedBigInteger('additional_id')
                ->nullable()
                ->default(0)
                ->comment('Тип доп услуги');
            $table
                ->unsignedTinyInteger('is_active')
                ->nullable()
                ->default(0)
                ->comment('Статус');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('house_id')->references('id')->on('houses');
            $table->foreign('gazebo_id')->references('id')->on('gazebos');

            $table
                ->foreign('additional_id')
                ->references('id')
                ->on('additionals')
                ->onDelete('set null')
            ;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('objects_additionals');
    }
};

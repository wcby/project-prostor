<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->id();
            $table
                ->string('name')
                ->comment('Название');
            $table
                ->unsignedBigInteger('category_premise_id')
                ->nullable()
                ->default(0)
                ->comment('Категория');
            $table
                ->string('count')
                ->comment('Количество людей');
            $table
                ->string('price')
                ->comment('Стоимость');
            $table
                ->string('type_premise_id')
                ->default(2)
                ->comment('Тип помещения');
            $table
                ->unsignedTinyInteger('is_active')
                ->nullable()
                ->default(0)
                ->comment('Статус');
            $table->timestamps();
            $table->softDeletes();

            $table
                ->foreign('category_premise_id')
                ->references('id')
                ->on('category_premises')
                ->onDelete('set null')
            ;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('houses');
    }
};

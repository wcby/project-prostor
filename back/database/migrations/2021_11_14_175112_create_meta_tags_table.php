<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetaTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_tags', function (Blueprint $table) {

            $table->id();

            $table
                ->unsignedBigInteger('entity_id')
                ->comment('ID основной таблицы')
            ;

            $table
                ->string('locale', 10)
                ->comment('Локаль')
            ;

            $table
                ->string('meta_title')
                ->nullable()
                ->comment('Мета тэг title')
            ;

            $table
                ->string('meta_keywords')
                ->nullable()
                ->comment('Мета тэг keywords')
            ;

            $table
                ->string('meta_description')
                ->nullable()
                ->comment('Мета тэг description')
            ;

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta_tags');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('menus', function (Blueprint $table) {

            $table->id();

            $table
                ->string('name')
                ->nullable()
                ->comment('Наименование меню');

            $table
                ->string('alias')
                ->comment('Техническое наименование меню');

            $table
                ->unsignedTinyInteger('is_active')
                ->nullable()
                ->default(0)
                ->comment('Активность меню');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('menus');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('premises', function (Blueprint $table) {
            $table->id();
            $table
                ->string('name')
                ->comment('Название');
            $table
                ->unsignedBigInteger('type_premise_id')
                ->nullable()
                ->default(0)
                ->comment('Тип помещения');
            $table
                ->unsignedBigInteger('category_premise_id')
                ->nullable()
                ->default(0)
                ->comment('Категория помещения');
            $table
                ->integer('cost')
                ->default(0);
            $table
                ->unsignedTinyInteger('is_active')
                ->nullable()
                ->default(0)
                ->comment('Статус');
            $table->timestamps();
            $table->softDeletes();

            $table
                ->foreign('type_premise_id')
                ->references('id')
                ->on('type_premises')
                ->onDelete('set null')
            ;
            $table
                ->foreign('category_premise_id')
                ->references('id')
                ->on('category_premises')
                ->onDelete('set null')
            ;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('premises');
    }
};

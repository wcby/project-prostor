<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('category_premises', function (Blueprint $table) {
            $table->id();
            $table
                ->string('name')
                ->comment('Название');
            $table
                ->unsignedTinyInteger('is_active')
                ->nullable()
                ->default(0)
                ->comment('Статус');
            $table
                ->string('color')
                ->comment('Цвет');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('category_premises');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('menu_items', function (Blueprint $table) {

            $table->id();

            $table
                ->string('name')
                ->comment('Название');

            $table
                ->unsignedBigInteger('menu_id')
                ->nullable()
                ->default(0)
                ->comment('ID меню');

            $table
                ->string('url')
                ->comment('Url подменю');

            $table
                ->unsignedBigInteger('parent_id')
                ->nullable()
                ->comment('Родитель подменю');

            $table
                ->unsignedTinyInteger('position')
                ->nullable()
                ->default(0)
                ->comment('Позиция подменю');

            $table
                ->unsignedTinyInteger('is_active')
                ->nullable()
                ->default(0)
                ->comment('Активность подменю');

            $table->timestamps();
            $table->softDeletes();

            $table
                ->foreign('parent_id')
                ->references('id')
                ->on('menu_items')
                ->onDelete('set null')
            ;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('menu_items');
    }
};

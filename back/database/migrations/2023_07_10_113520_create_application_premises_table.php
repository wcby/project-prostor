<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('application_premises', function (Blueprint $table) {
            $table
                ->unsignedBigInteger('application_id')
                ->nullable(false)
                ->comment('Заявка');
            $table
                ->unsignedBigInteger('premise_id')
                ->nullable(false)
                ->comment('Помещение');

            $table->timestamps();
            $table->softDeletes();

            $table
                ->foreign('application_id')
                ->references('id')
                ->on('applications')
            ;
            $table
                ->foreign('premise_id')
                ->references('id')
                ->on('premises')
            ;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('application_premises');
    }
};

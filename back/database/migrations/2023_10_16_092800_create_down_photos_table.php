<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('down_photos', function (Blueprint $table) {
            $table->id();
            $table
                ->string('name')
                ->nullable()
                ->comment('Alt-подсказка');
            $table
                ->integer('position')
                ->default(0);
            $table
                ->unsignedTinyInteger('is_active')
                ->nullable()
                ->default(0)
                ->comment('Статус');
            $table
                ->string('count')
                ->default(0);
            $table
                ->string('description')
                ->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('down_photos');
    }
};

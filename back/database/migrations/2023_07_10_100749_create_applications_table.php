<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->id();
            $table
                ->unsignedBigInteger('client_id')
                ->nullable(false)
                ->comment('Клиент');
            $table
                ->integer('adult')
                ->nullable(false)
                ->comment('Взрослые');
            $table
                ->integer('child')
                ->nullable()
                ->comment('Дети');
            $table
                ->longtext('comment')
                ->nullable()
                ->comment('Комментарий');
            $table
                ->float('total')
                ->nullable(0)
                ->comment('Всего');
            $table
                ->float('pay')
                ->nullable()
                ->comment('Оплата');
            $table
                ->date('check_in')
                ->default(date('Y-m-d'))
                ->comment('Заезд');
            $table
                ->date('check_out')
                ->default(date('Y-m-d'))
                ->comment('Выезд');
            $table
                ->unsignedBigInteger('payment_id')
                ->default(3)
                ->comment('Статус оплаты');
            $table
                ->unsignedBigInteger('is_view')
                ->default(0)
                ->comment('Статус просмотра');
            $table
                ->unsignedBigInteger('is_active')
                ->default(1)
                ->comment('Статус архива');
            $table
                ->unsignedBigInteger('is_canceled')
                ->default(0)
                ->comment('Статус отмены');
            $table
                ->unsignedBigInteger('status_id')
                ->default(1)
                ->comment('Статус');
            $table->timestamps();
            $table->softDeletes();

            $table
                ->foreign('client_id')
                ->references('id')
                ->on('clients')
            ;

            $table
                ->foreign('payment_id')
                ->references('id')
                ->on('payments')
            ;

            $table
                ->foreign('status_id')
                ->references('id')
                ->on('statuses')
            ;

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('applications');
    }
};

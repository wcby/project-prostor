<?php

namespace App\Providers;

use App\Jobs\Midnight;
use Illuminate\Support\ServiceProvider;

class JobServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Отправка в очередь задания из сервис провайдера
//        Midnight::dispatch()->delay(now()->minutes(1));
    }
}

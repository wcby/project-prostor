<?php

namespace App\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Intervention\Image\Facades\Image;

class RegisterServiceProvider extends ServiceProvider
{
    /**
     * List of Local Environment Providers.
     *
     * @var array
     */
    protected $providers = [
        \App\Providers\CollectionServiceProvider::class,
    ];
    /**
     * List of only Local Environment Facade Aliases.
     *
     * @var array
     */
    protected $facadesAliases = [
//        'Image' => Image::class,
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerServiceProviders();
        $this->registerFacadeAliases();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Load local service providers.
     */
    protected function registerServiceProviders()
    {
        foreach ($this->providers as $provider) {

            $this->app->register($provider);
        }
    }

    /**
     * Load additional Aliases.
     */
    public function registerFacadeAliases()
    {
        $loader = AliasLoader::getInstance();

        foreach ($this->facadesAliases as $alias => $facade) {

            $loader->alias($alias, $facade);
        }
    }
}

<?php

namespace App\Providers;

use App\Models\Additional;
use App\Models\AdditionalType;
use App\Models\Application;
use App\Models\Article;
use App\Models\Banner;
use App\Models\Callback;
use App\Models\Category;
use App\Models\CategoryPremise;
use App\Models\Client;
use App\Models\DownPhoto;
use App\Models\Gallery;
use App\Models\Gazebo;
use App\Models\House;
use App\Models\Language;
use App\Models\Menu;
use App\Models\MenuItem;
use App\Models\Page;
use App\Models\Payment;
use App\Models\Premise;
use App\Models\Role;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\Status;
use App\Models\TypeGallery;
use App\Models\TypePremise;
use App\Models\User;
use App\Policies\Policy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        User::class => Policy::class,
        Article::class => Policy::class,
        Role::class => Policy::class,
        Page::class => Policy::class,
        Category::class => Policy::class,
        Menu::class => Policy::class,
        MenuItem::class => Policy::class,
        Application::class => Policy::class,
        CategoryPremise::class => Policy::class,
        Payment::class => Policy::class,
        Client::class => Policy::class,
        Premise::class => Policy::class,
        Status::class => Policy::class,
        TypePremise::class => Policy::class,
        House::class => Policy::class,
        Gazebo::class => Policy::class,
        Gallery::class => Policy::class,
        Additional::class => Policy::class,
        AdditionalType::class => Policy::class,
        Slider::class => Policy::class,
        DownPhoto::class => Policy::class,
        Callback::class => Policy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->registerPolicies();

        //
    }
}

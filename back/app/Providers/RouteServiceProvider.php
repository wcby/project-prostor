<?php

namespace App\Providers;

use App\Models\Additional;
use App\Models\AdditionalType;
use App\Models\Application;
use App\Models\Article;
use App\Models\Callback;
use App\Models\Category;
use App\Models\CategoryPremise;
use App\Models\DownPhoto;
use App\Models\Gallery;
use App\Models\Gazebo;
use App\Models\House;
use App\Models\Menu;
use App\Models\MenuItem;
use App\Models\Page;
use App\Models\Payment;
use App\Models\Premise;
use App\Models\Role;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\Status;
use App\Models\TypePremise;
use App\Models\User;
use App\Models\Client;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/';

    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::middleware('api')
                ->prefix('api')
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->group(base_path('routes/web.php'));
        });

        // Для инъекции нужной модели
        Route::model('user', User::class);
        Route::model('article', Article::class);
        Route::model('role', Role::class);
        Route::model('page', Page::class);
        Route::model('category', Category::class);
        Route::model('setting', Setting::class);
        Route::model('menu', Menu::class);
        Route::model('menuId', Menu::class);
        Route::model('menu_item', MenuItem::class);
        Route::model('category_premise', CategoryPremise::class);
        Route::model('application', Application::class);
        Route::model('payment', Payment::class);
        Route::model('premise', Premise::class);
        Route::model('status', Status::class);
        Route::model('type_premise', TypePremise::class);
        Route::model('client', Client::class);
        Route::model('house', House::class);
        Route::model('gazebo', Gazebo::class);
        Route::model('additional', Additional::class);
        Route::model('additional_type', AdditionalType::class);
        Route::model('gallery', Gallery::class);
        Route::model('slider', Slider::class);
        Route::model('down_photo', DownPhoto::class);
        Route::model('callback', Callback::class);
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting(): void
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map(): void
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes(): void
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes(): void
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}

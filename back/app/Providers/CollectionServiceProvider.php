<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class CollectionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Расширяем класс Illuminate\Support\Collection
        $this
            ->paginate()
            ->deactivationOfSchedule()
        ;
    }

    public function paginate()
    {
        if (!Collection::hasMacro('paginate')) {

            Collection::macro('paginate', function ($perPage = 15, $page = null, $options = []) {

                $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

                return (new LengthAwarePaginator($this->forPage($page, $perPage), $this->count(), $perPage, $page, $options))->withPath(Paginator::resolveCurrentPath());
            });
        }

        return $this;
    }

    public function deactivationOfSchedule(): CollectionServiceProvider
    {
        if (!Collection::hasMacro('deactivationOfSchedule')) {

            Collection::macro('deactivationOfSchedule', function () {

                return $this->filter(function ($query) {

                    if ($query
                        ->scheduleDate
                        ->where('date_start', '>', Carbon::now()->subDays(env('DISPLAY_DAYS_PERIOD')))
                        ->isEmpty()) {

                        $query->is_active = 0;
                        $query->save();
                    } else {

                        return $query;
                    }
                });
            });
        }

        return $this;
    }
}

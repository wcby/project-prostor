<?php

namespace App\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class LocalEnvironmentServiceProvider extends ServiceProvider
{
    /**
     * List of Local Environment Providers.
     *
     * @var array
     */
    protected array $localProviders = [
        IdeHelperServiceProvider::class
    ];

    /**
     * List of only Local Environment Facade Aliases.
     *
     * @var array
     */
    protected array $facadeAliases = [
    ];

    /**
     * Bootstrap the application services.
     */
    public function boot(): void
    {
        if ($this->app->isLocal()) {

            $this->registerServiceProviders();
            $this->registerFacadeAliases();
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        //
    }

    /**
     * Load local service providers.
     */
    protected function registerServiceProviders(): void
    {
        foreach ($this->localProviders as $provider) {

            $this->app->register($provider);
        }
    }

    /**
     * Load additional Aliases.
     */
    public function registerFacadeAliases(): void
    {
        $loader = AliasLoader::getInstance();

        foreach ($this->facadeAliases as $alias => $facade) {

            $loader->alias($alias, $facade);
        }
    }
}

<?php


namespace App\Traits;

use Illuminate\Support\Facades\Log;

trait Logger
{
    // Информация
    public function logInfo($message, array $context = array())
    {
        Log::channel('custom_info')->info($message, $context);
    }

    // Уведомление
    public function logNotice($message, array $context = array())
    {
        Log::channel('custom_notice')->notice($message, $context);
    }

    // Предупреждение
    public function logWarning($message, array $context = array())
    {
        Log::channel('custom_warning')->warning($message, $context);
    }

    // Ошибка
    public function logError($message, array $context = array())
    {
        Log::channel('custom_error')->error($message, $context);
    }

    // Тревога
    public function logAlert($message, array $context = array())
    {
        Log::channel('custom_alert')->alert($message, $context);
    }

    // Критический
    public function logCritical($message, array $context = array())
    {
        Log::channel('custom_critical')->critical($message, $context);
    }

    // Чрезвычайная ситуация
    public function logEmergency($message, array $context = array())
    {
        Log::channel('custom_emergency')->emergency($message, $context);
    }

    // Отладка
    public function logDebug($message, array $context = array())
    {
        Log::channel('custom_debug')->debug($message, $context);
    }
}

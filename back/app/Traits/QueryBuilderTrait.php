<?php

namespace App\Traits;

trait QueryBuilderTrait
{
    // Если нужна выборка по Like
    public function queryBuilderLike($models, $field, $value)
    {
        return $models->where($field, 'like', '%' . $value . '%');
    }

    // Если нужна обычная выборка по =
    public function queryBuilderExactly($models, $field, $value)
    {
        return $models->where($field, $value);
    }

    // Если нужна выборка от и до
    public function queryBuilderBetween($models, $field, $values)
    {
        return $models->whereBetween($field, $values);
    }
}

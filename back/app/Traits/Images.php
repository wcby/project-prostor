<?php

namespace App\Traits;

use File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

trait Images
{
    public $large;

    public $medium;

    public $small;

    public $thumbnail;

    public function removeLink($file)
    {
        if (file_exists($file) && is_file($file)){

            unlink($file);
        }
    }

    public function removeLargeLink($file)
    {
        $file = str_replace(basename($file), '', $file) . 'large_' . basename($file);

        if (file_exists($file) && is_file($file)){
            unlink($file);
        }
    }

    public function removeMediumLink($file)
    {
        $file = str_replace(basename($file), '', $file) . 'medium_' . basename($file);

        if (file_exists($file) && is_file($file)){
            unlink($file);
        }
    }

    public function removeSmallLink($file)
    {
        $file = str_replace(basename($file), '', $file) . 'small_' . basename($file);

        if (file_exists($file) && is_file($file)){
            unlink($file);
        }
    }

    public function removeThumbnailLink($file)
    {
        $file = str_replace(basename($file), '', $file) . 'thumbnail_' . basename($file);

        if (file_exists(public_path($file)) && is_file($file)){
            unlink($file);
        }
    }

    public function createDirectory($path, $isFile = true)
    {
        $results = '';
        try{

            $directories = explode('/', trim($path, '/'));
            if ($isFile) {
                array_pop($directories);
            }
            foreach ($directories as $directory) {
                $results .= '/' . $directory;

                if (!file_exists(public_path($results))) {
                    $r = File::makeDirectory(public_path($results), 0777, true);
                } else {
                    chmod(public_path($results), 755);
                }
            }

            $results = true;

        } catch (\Exception $e) {

            $results = false;

        }

        return $results;
    }

    public function createImage($file, $path)
    {
        $fileName = Str::random(10).'.'.$file->getClientOriginalExtension();
        $file->move($path, $fileName);

        return $fileName;
    }

    public function createLargeImage($fileName, $filePath, $width = 960, $height = 640)
    {
        // create an image
        $image = Image::make($filePath . $fileName);

        // backup status
        $image->backup();

        if ($image->width() > $image->height()) {

            $height = null;
        }

        if ($image->width() < $image->height()) {

            $width = null;
        }

        $image->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })
            ->save($filePath.'large_'.$fileName, 100);

        // reset image (return to backup state)
        $image->reset();
    }

    public function createMediumImage($fileName, $filePath, $width = 600, $height = 600)
    {
        // create an image
        $image = Image::make($filePath . $fileName);

        // backup status
        $image->backup();

        if ($image->width() > $image->height()) {

            $height = null;
        }

        if ($image->width() < $image->height()) {

            $width = null;
        }

        $image->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })
            ->save($filePath.'medium_'.$fileName, 100);

        // reset image (return to backup state)
        $image->reset();
    }

    public function createSmallImage($fileName, $filePath, $width = '400', $height = '400')
    {
        // create an image
        $image = Image::make($filePath . $fileName);

        // backup status
        $image->backup();

        if ($image->width() > $image->height()) {

            $height = null;
        }

        if ($image->width() < $image->height()) {

            $width = null;
        }

        $image->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })
            ->save($filePath.'small_'.$fileName, 100);

        // reset image (return to backup state)
        $image->reset();
    }

    public function createThumbnailImage($fileName, $filePath, $width = '200', $height = '200')
    {
        // create an image
        $image = Image::make($filePath . $fileName);

        // backup status
        $image->backup();

        if ($image->width() > $image->height()) {

            $height = null;
        }

        if ($image->width() < $image->height()) {

            $width = null;
        }

        $image->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })
            ->save($filePath.'thumbnail_'.$fileName, 100);

        // reset image (return to backup state)
        $image->reset();
    }

}

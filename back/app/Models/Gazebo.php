<?php

namespace App\Models;

use App\Models\Observers\GalleryObserver;
use App\Models\Observers\GazeboObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gazebo extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'gazebos';

    protected $fillable = [
        'id',
        'name',                 // Название
        'count',                // Количество мест
        'seat_cost',            // Доплата за место
        'price',                // Цена
        'type_premise_id',      // тип помещения
        'is_active'
    ];


    protected static function boot()
    {
        parent::boot();

        self::observe(GazeboObserver::class);
    }

    public function typePremise(): BelongsTo
    {
        return $this->belongsTo(TypePremise::class, 'type_premise_id');
    }

    public function objectAdditionals(): HasMany
    {
        return $this->hasMany(ObjectsAdditional::class, 'gazebo_id', 'id');
    }


    public function images()
    {
        return $this
            ->hasMany(Image::class, 'parent_id', 'id')
            ->where('type', 'gazebo')
            ->orderBy('position');
    }


    public function getDateAttribute($date): string
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function setDateAttribute($date): void
    {
        $this->attributes['date'] = Carbon::parse($date);
    }

    // Фильтрация
    public function filtering()
    {
        $queryBuilder = $this;

        $sessionData = session("{$this->entity()}", []);

        $filter = collect($sessionData)->except('page', 'method', 'sort_column', 'sort_direction')->toArray();

        $filter['used'] = false;

        $fields = collect($sessionData)
            ->keys()
            ->intersect($this->getFillable())
            ->toArray()
        ;

        if (in_array('id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('id', session("{$this->getTable()}.id"));
        }

        if (in_array('name', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('name', session("{$this->getTable()}.name"));
        }

        if (in_array('count', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('count',  session("{$this->getTable()}.count"));
        }

        if (in_array('seat_cost', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('seat_cost',  session("{$this->getTable()}.seat_cost"));
        }

        if (in_array('price', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('price',  session("{$this->getTable()}.price"));
        }

        if (in_array('category_premise_id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('category_premise_id',  session("{$this->getTable()}.category_premise_id"));
        }

        $sessionData['filter'] = $filter;

        // Пишем его в сессию
        session([$this->entity() => $sessionData]);

        return $queryBuilder;
    }
}

<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Additional extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'additionals';

    protected $fillable = [
        'id',
        'name',                 // Название
        'icon',                 // Иконка
        'additional_type_id',   // тип доп услуги
        'is_active'
    ];


    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\AdditionalObserver::class);
    }

    public function additionalType(): BelongsTo
    {
        return $this->belongsTo(AdditionalType::class, 'additional_type_id');
    }


    public function getDateAttribute($date): string
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function setDateAttribute($date): void
    {
        $this->attributes['date'] = Carbon::parse($date);
    }

    // Фильтрация
    public function filtering()
    {
        $queryBuilder = $this;

        $sessionData = session("{$this->entity()}", []);

        $filter = collect($sessionData)->except('page', 'method', 'sort_column', 'sort_direction')->toArray();

        $filter['used'] = false;

        $fields = collect($sessionData)
            ->keys()
            ->intersect($this->getFillable())
            ->toArray()
        ;

        if (in_array('id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('id', session("{$this->getTable()}.id"));
        }

        if (in_array('name', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('name', 'like', '%' . session("{$this->getTable()}.name") . '%');
        }

        if (in_array('icon', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('icon',  session("{$this->getTable()}.icon"));
        }

        if (in_array('additional_type_id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('additional_type_id',  session("{$this->getTable()}.additional_type_id"));
        }

        $sessionData['filter'] = $filter;

        // Пишем его в сессию
        session([$this->entity() => $sessionData]);

        return $queryBuilder;
    }
}

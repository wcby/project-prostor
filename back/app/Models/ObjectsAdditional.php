<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ObjectsAdditional extends DefaultModel
{
    protected $table = 'objects_additionals';

    protected $fillable = [
        'id',
        'house_id',                 // Название
        'gazebo_id',                // Количество мест
        'additional_id',            // Доплата за место
        'is_active'
    ];

    public function gazebos(): BelongsTo
    {
        return $this->belongsTo(Gazebo::class, 'id');
    }

    public function houses(): BelongsTo
    {
        return $this->belongsTo(House::class, 'id');
    }
}

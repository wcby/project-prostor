<?php

namespace App\Models;

use App\Models\Observers\AdditionalTypeObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdditionalType extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'additional_types';

    protected $fillable = [
        'id',
        'name',              // Название
        'type_premise_id',   // тип помещения
        'is_active'
    ];


    protected static function boot()
    {
        parent::boot();

        self::observe(AdditionalTypeObserver::class);
    }

    public function additionals(): HasMany
    {
        return $this->hasMany(Additional::class, 'additional_type_id');
    }

    public function typePremise(): BelongsTo
    {
        return $this->belongsTo(TypePremise::class, 'type_premise_id');
    }


    public function getDateAttribute($date): string
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function setDateAttribute($date): void
    {
        $this->attributes['date'] = Carbon::parse($date);
    }

    // Фильтрация
    public function filtering()
    {
        $queryBuilder = $this;

        $sessionData = session("{$this->entity()}", []);

        $filter = collect($sessionData)->except('page', 'method', 'sort_column', 'sort_direction')->toArray();

        $filter['used'] = false;

        $fields = collect($sessionData)
            ->keys()
            ->intersect($this->getFillable())
            ->toArray()
        ;

        if (in_array('id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('id', session("{$this->entity()}.id"));
        }

        if (in_array('name', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('name', 'like', '%' . session("{$this->entity()}.name") . '%');
        }

        if (in_array('type_premise_id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('type_premise_id',  session("{$this->entity()}.type_premise_id"));
        }

        $sessionData['filter'] = $filter;

        // Пишем его в сессию
        session([$this->entity() => $sessionData]);

        return $queryBuilder;
    }
}

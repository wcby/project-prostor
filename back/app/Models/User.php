<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;

class User extends DefaultUserModel
{
    use Notifiable, SoftDeletes;

    /**
     * ----------------------------------------------------------
     * The attributes that are mass assignable.
     * ----------------------------------------------------------
     * Атрибуты, которые могут быть присвоены в массовом порядке
     * ----------------------------------------------------------
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'email',
        'email_verified_at',
        'phone',
        'avatar',
        'password',
        'role_id',
        'is_active',
    ];

    /**
     * --------------------------------------------------
     * The attributes that should be hidden for arrays.
     * --------------------------------------------------
     * Атрибуты, которые должны быть скрыты для массивов
     * --------------------------------------------------
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * ------------------------------------------------------------
     * The attributes that should be cast to native types.
     * ------------------------------------------------------------
     * Атрибуты, которые должны быть приведены к собственным типам
     * ------------------------------------------------------------
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $with = [
        'role',
    ];

    public $permission;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->permission = (new Permission());
    }

    /**
     * The "booting" method of the model.
     */
    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\UserObserver::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class)->with('permissions');
    }

    /**
     * Determine if the user may perform the given permission.
     *
     * @param $attributes
     * @return bool
     */
    public function hasPermission($attributes)
    {
        if (empty($attributes)) {

            return true;
        }

        if (is_array($attributes)) {

            $permission = $this
                ->role
                ->permissions
                ->where('group', $attributes['group'])
                ->where('action', $attributes['action'])
                ->first()
            ;
        } elseif ($attributes instanceof Collection) {

            $permission = $this
                ->role
                ->permissions
                ->where('group', $attributes->get('group'))
                ->where('action', $attributes->get('action'))
                ->first()
            ;
        } else {

            list($group, $action) = explode('.', $attributes);

            $permission = $this
                ->role
                ->permissions
                ->where('group', $group)
                ->where('action', $action)
                ->first()
            ;
        }

        return ((bool) $permission);
    }

    public function hasRole($roles)
    {
        if (is_string($roles)) {

            return collect()->push($this->role)->contains('alias', $roles);
        }

        return $roles->intersect(collect()->push($this->role))->isNotEmpty();
    }

    // Фильтрация
    public function filtering()
    {
        $queryBuilder = $this;

        $sessionData = session("{$this->entity()}", []);

        $filter = collect($sessionData)->except('page', 'method', 'sort_column', 'sort_direction')->toArray();

        $filter['used'] = false;

        $fields = collect($sessionData)
            ->keys()
            ->intersect($this->getFillable())
            ->toArray()
        ;

        if (in_array('id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('id', session("{$this->getTable()}.id"));
        }

        if (in_array('email', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('email', 'like', '%' . session("{$this->getTable()}.email") . '%');
        }

        if (in_array('role_id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('role_id', session("{$this->getTable()}.role_id"));
        }

        if (in_array('is_active', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('is_active', session("{$this->getTable()}.is_active"));
        }

        $sessionData['filter'] = $filter;

        // Пишем его в сессию
        session([$this->entity() => $sessionData]);

        return $queryBuilder;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicationPremise extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'application_premises';

    protected $fillable = [
        'application_id',   // Заявка
        'premise_id',       // Помещение
    ];

    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\ApplicationPremiseObserver::class);
    }
}

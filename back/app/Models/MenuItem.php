<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class MenuItem extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'menu_items';

    /**
     * ----------------------------------------------------------
     * The attributes that are mass assignable.
     * ----------------------------------------------------------
     * Атрибуты, которые могут быть присвоены в массовом порядке
     * ----------------------------------------------------------
     *
     * @var array
     */
    protected $fillable = [
        'id',               // ID
        'name',             // Имя
        'menu_id',          // Основное меню
        'url',              // Url подменю
        'parent_id',        // Родитель подменю
        'position',         // Позиция подменю
        'is_active',        // Активность подменю
    ];

    /**
     * The "booting" method of the model.
     */
    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\MenuItemObserver::class);
    }

    public function children()
    {
        return $this->hasMany($this, 'parent_id', 'id');
    }
    public function parent()
    {
        return $this->belongsTo($this, 'parent_id');
    }

    public function filtering(Menu $menu)
    {
        $queryBuilder = $this->where('menu_id', $menu->id);

        $sessionData = session("{$this->entity()}", []);

        $filter = collect($sessionData)->except('page', 'method', 'sort_column', 'sort_direction')->toArray();

        $filter['used'] = false;

        $fields = collect($sessionData)
            ->keys()
            ->intersect($this->getFillable())
            ->toArray()
        ;

        if (in_array('id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('id', session("{$this->getTable()}.id"));
        }

        if (in_array('name', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('name', 'like', '%' . session("{$this->getTable()}.name") . '%');
        }

        if (in_array('url', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('url', 'like', '%' . session("{$this->getTable()}.url") . '%');
        }

        if (in_array('is_active', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('is_active', session("{$this->getTable()}.is_active"));
        }

        $sessionData['filter'] = $filter;

        // Пишем его в сессию
        session([$this->entity() => $sessionData]);

        return $queryBuilder;
    }
}

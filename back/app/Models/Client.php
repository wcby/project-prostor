<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'clients';

    protected $fillable = [
        'id',               // ID
        'name',             // Название
        'phone',            // Телефон
        'email'             // Email
    ];

    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\ClientObserver::class);
    }

    public function getDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function setDateAttribute($date)
    {
        $this->attributes['date'] = Carbon::parse($date);
    }

    // Фильтрация
    public function filtering()
    {
        $queryBuilder = $this;

        $sessionData = session("{$this->entity()}", []);

        $filter = collect($sessionData)->except('page', 'method', 'sort_column', 'sort_direction')->toArray();

        $filter['used'] = false;

        $fields = collect($sessionData)
            ->keys()
            ->intersect($this->getFillable())
            ->toArray()
        ;

        if (in_array('id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('id', session("{$this->getTable()}.id"));
        }

        if (in_array('name', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('name', 'like', '%' . session("{$this->getTable()}.name") . '%');
        }

        if (in_array('url', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('url', 'like', '%' . session("{$this->getTable()}.url") . '%');
        }

        if (session()->has("{$this->getTable()}.from_date") || session()->has("{$this->getTable()}.to_date")) {

            $filter['used'] = true;

            $from = session()->has("{$this->getTable()}.from_date") ? Carbon::parse(session("{$this->getTable()}.from_date")) : Carbon::create(1900);//Carbon::now();
            $to = session()->has("{$this->getTable()}.to_date") ? Carbon::parse(session("{$this->getTable()}.to_date")) : Carbon::create(2120);//Carbon::now();

            $queryBuilder = $queryBuilder->whereBetween('date', [$from, $to]);
        }

        if (in_array('brief_description', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('brief_description', 'like', '%' . session("{$this->getTable()}.brief_description") . '%');
        }

        if (in_array('description', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('description', 'like', '%' . session("{$this->getTable()}.description") . '%');
        }

        if (in_array('meta_title', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('meta_title', 'like', '%' . session("{$this->getTable()}.meta_title") . '%');
        }

        if (in_array('meta_keywords', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('meta_keywords', 'like', '%' . session("{$this->getTable()}.meta_keywords") . '%');
        }

        if (in_array('meta_description', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('meta_description', 'like', '%' . session("{$this->getTable()}.meta_description") . '%');
        }

        if (in_array('is_active', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('is_active', session("{$this->getTable()}.is_active"));
        }

        $sessionData['filter'] = $filter;

        // Пишем его в сессию
        session([$this->entity() => $sessionData]);

        return $queryBuilder;
    }
}

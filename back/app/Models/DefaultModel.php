<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class DefaultModel extends Model
{
    /**
     * ---------------------------------
     * Поле для сортировки по умолчанию
     * ---------------------------------
     *
     * @var string
     */
    public string $columnDefault = 'id';

    /**
     * ----------------------------------------
     * Направление для сортировки по умолчанию
     * ----------------------------------------
     *
     * @var string
     */
    protected string $directionDefault = 'desc';

    /**
     * -------------------------------------
     * Количество выбранных записей из базы
     * -------------------------------------
     *
     * @var int
     */
    protected int $total = 20;

    /**
     * ---------------------
     * Статус is_active = 1
     * ---------------------
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query): mixed
    {
        return $query->where('is_active', 1);
    }

    /**
     * --------------------------------
     * Поля которые выводятся в списке
     * --------------------------------
     *
     * @return array
     */
    public function fieldsSelected(): array
    {
        if (has_cookie("fields_{$this->entity()}")) {

            $fields = collect(get_cookie("fields_{$this->entity()}", []))->unique()->toArray();
        } else {

            $fields = collect(config("app.{$this->entity()}.fields_selected_default", []))
                ->unique()
                ->toArray();
        }
        //dd($this->entity(), $fields);
        return $fields;
    }

    /**
     * ------------------
     * Название сущности
     * ------------------
     *
     * @return string
     */
    public function entity(): string
    {
        return (string)Str::of($this->getTable())->replace('_', '-');
    }

    /**
     * ------------------------------
     * Поля для отображения в списке
     * ------------------------------
     *
     * @return array
     */
    public function fieldsForShowing(): array
    {
        return collect(config("app.{$this->entity()}.fields_for_showing", []))
            ->unique()
            ->toArray();
    }

    public function getSorting($key = null)
    {
        return $key ? get_cookie("sorting_{$this->entity()}.{$key}")
            : get_cookie("sorting_{$this->entity()}");
    }

    public function getColumnSortingAttribute()
    {
        return $this->getSorting('sortColumn') ?? $this->columnDefault;
    }

    public function getDirectionSortingAttribute()
    {
        $sorting = get_cookie("sorting_{$this->entity()}");

        return $sorting['sortDirection'] ?? $this->directionDefault;
    }

    public function methodExists($method): bool
    {
        return method_exists($this, $method);
    }

    public function setMetaTitle($metaTitle): void
    {
        if (!empty($metaTitle) && $this->methodExists('metaTags')) {
            dd($metaTitle);
            $metaTag = $this
                ->metaTags()
                ->firstOrNew();
            $metaTag->save();
        }
    }

    public function setMetaKeywords($metaKeywords): void
    {
        if (!empty($metaKeywords) && $this->methodExists('metaTags')) {

            $metaTag = $this
                ->metaTags()
                ->firstOrNew();

        }
    }

    public function setMetaDescription($metaDescription): void
    {
        if (!empty($metaDescription) && $this->methodExists('metaTags')) {

            $metaTag = $this
                ->metaTags()
                ->firstOrNew();
        }
    }

    public function getTotalRecordsAttribute(): int
    {
        return $this->total;
    }
}

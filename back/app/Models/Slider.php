<?php

namespace App\Models;

use App\Models\Observers\SliderObserver;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'sliders';

    protected $fillable = [
        'id',
        'name',
        'position',
        'is_active'
    ];

    protected $with = [
        'images',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected static function boot()
    {
        parent::boot();

        self::observe(SliderObserver::class);
    }

    public function images()
    {
        return $this
            ->hasMany(Image::class, 'parent_id', 'id')
            ->where('type', 'slider')
            ->orderBy('position');
    }

    public function getImageAttribute()
    {
        return $this->images->first();
    }

    public function getImagePathAttribute()
    {
        return $this->image?->path;
    }


    // Фильтрация
    public function filtering()
    {
        $queryBuilder = $this;

        $sessionData = session("{$this->entity()}", []);

        $filter = collect($sessionData)->except('page', 'method', 'sort_column', 'sort_direction')->toArray();

        $filter['used'] = false;

        $fields = collect($sessionData)
            ->keys()
            ->intersect($this->getFillable())
            ->toArray()
        ;

        if (in_array('id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('id', session("{$this->getTable()}.id"));
        }

        $sessionData['filter'] = $filter;

        // Пишем его в сессию
        session([$this->entity() => $sessionData]);

        return $queryBuilder;
    }
}


<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class MetaTag extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'meta_tags';

    /**
     * ----------------------------------------------------------
     * The attributes that are mass assignable.
     * ----------------------------------------------------------
     * Атрибуты, которые могут быть присвоены в массовом порядке
     * ----------------------------------------------------------
     *
     * @var array
     */
    protected $fillable = [
        'id',           // ID
        'meta_type',    // Morph Class
        'meta_id',      // ID смежной таблицы
    ];

    protected $appends = [
        'meta_title',
        'meta_keywords',
        'meta_description',
    ];

    protected $with = [
        'entities',
    ];

    /**
     * The "booting" method of the model.
     */
    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\MetaTagObserver::class);
    }

    public function meta(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

    // Локали
    public function entities()
    {
        return $this
            ->hasMany(MetaTagI18n::class, 'entity_id', 'id')
        ;
    }

    public function current_locale()
    {
        return $this->entities ? $this->entities->where('locale', get_current_locale())->first() : null;
    }

    public function getMetaTitleAttribute()
    {
        return $this->current_locale() ? $this->current_locale()->meta_title : null;
    }

    public function getMetaKeywordsAttribute()
    {
        return $this->current_locale() ? $this->current_locale()->meta_keywords : null;
    }

    public function getMetaDescriptionAttribute()
    {
        return $this->current_locale() ? $this->current_locale()->meta_description : null;
    }

    // Фильтрация
    public function filtering()
    {
        $queryBuilder = $this;

        $sessionData = session("{$this->entity()}", []);

        $filter = collect($sessionData)->except('page', 'method', 'sort_column', 'sort_direction')->toArray();

        $filter['used'] = false;

        $fields = collect($sessionData)
            ->keys()
            ->intersect($this->getFillable())
            ->toArray()
        ;

        if (in_array('id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('id', session("{$this->getTable()}.id"));
        }

        if (in_array('meta_title', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('meta_title', 'like', '%' . session("{$this->getTable()}.meta_title") . '%');
        }

        if (in_array('meta_keywords', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('meta_keywords', 'like', '%' . session("{$this->getTable()}.meta_keywords") . '%');
        }

        if (in_array('meta_description', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('meta_description', 'like', '%' . session("{$this->getTable()}.meta_description") . '%');
        }

        if (in_array('is_active', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('is_active', session("{$this->getTable()}.is_active"));
        }

        $sessionData['filter'] = $filter;

        // Пишем его в сессию
        session([$this->entity() => $sessionData]);

        return $queryBuilder;
    }
}

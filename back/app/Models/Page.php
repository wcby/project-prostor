<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'pages';

    /**
     * ----------------------------------------------------------
     * The attributes that are mass assignable.
     * ----------------------------------------------------------
     * Атрибуты, которые могут быть присвоены в массовом порядке
     * ----------------------------------------------------------
     *
     * @var array
     */
    protected $fillable = [
        'name',             // Имя
        'description',      // Oписание
        'id',               // ID
        'slug',              // Урл
        'is_active',        // Статус
    ];

    protected $appends = [
        'name',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description',
    ];

    protected $with = [
        'entities',
        'metaTags'
    ];

    /**
     * The "booting" method of the model.
     */
    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\PageObserver::class);
    }

    public function metaTags(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne(MetaTag::class, 'meta');
    }

    public function current_locale()
    {
        return $this->entities ? $this->entities->where('locale', get_current_locale())->first() : null;
    }

    public function getNameAttribute()
    {
        return $this->current_locale() ? $this->current_locale()->name : null;
    }

    public function getDescriptionAttribute()
    {
        return $this->current_locale() ? $this->current_locale()->description : null;
    }

    public function getMetaTitleAttribute()
    {
        return $this->methodExists('metaTags') && $this->metaTags && $this->metaTags->current_locale() ? $this->metaTags->current_locale()->meta_title : null;
    }

    public function getMetaKeywordsAttribute()
    {
        return $this->methodExists('metaTags') && $this->metaTags && $this->metaTags->current_locale() ? $this->metaTags->current_locale()->meta_keywords : null;
    }

    public function getMetaDescriptionAttribute()
    {
        return $this->methodExists('metaTags') && $this->metaTags && $this->metaTags->current_locale() ? $this->metaTags->current_locale()->meta_description : null;
    }

    // Фильтрация
    public function filtering()
    {
        $queryBuilder = $this;

        $sessionData = session("{$this->entity()}", []);

        $filter = collect($sessionData)->except('page', 'method', 'sort_column', 'sort_direction')->toArray();

        $filter['used'] = false;

        $fields = collect($sessionData)
            ->keys()
            ->intersect($this->getFillable())
            ->toArray()
        ;

        if (in_array('id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('id', session("{$this->getTable()}.id"));
        }

        if (in_array('name', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('name', 'like', '%' . session("{$this->getTable()}.name") . '%');
        }

        if (in_array('url', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('url', 'like', '%' . session("{$this->getTable()}.url") . '%');
        }

        if (in_array('content', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('content', 'like', '%' . session("{$this->getTable()}.content") . '%');
        }

        if (in_array('meta_title', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('meta_title', 'like', '%' . session("{$this->getTable()}.meta_title") . '%');
        }

        if (in_array('meta_keywords', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('meta_keywords', 'like', '%' . session("{$this->getTable()}.meta_keywords") . '%');
        }

        if (in_array('meta_description', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('meta_description', 'like', '%' . session("{$this->getTable()}.meta_description") . '%');
        }

        if (in_array('is_active', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('is_active', session("{$this->getTable()}.is_active"));
        }

        $sessionData['filter'] = $filter;

        // Пишем его в сессию
        session([$this->entity() => $sessionData]);

        return $queryBuilder;
    }
}

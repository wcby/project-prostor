<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class House extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'houses';

    protected $fillable = [
        'id',
        'name',                 // Название
        'count',                // количество мест
        'price',                // стоимость
        'category_premise_id',  // категория
        'type_premise_id',      // тип помещения
        'is_active'
    ];


    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\HouseObserver::class);
    }

    public function categoryPremise(): BelongsTo
    {
        return $this->belongsTo(CategoryPremise::class, 'category_premise_id');
    }

    public function typePremise(): BelongsTo
    {
        return $this->belongsTo(TypePremise::class, 'type_premise_id');
    }

    public function images()
    {
        return $this
            ->hasMany(Image::class, 'parent_id', 'id')
            ->where('type', 'house')
            ->orderBy('position');
    }


    public function getDateAttribute($date): string
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function setDateAttribute($date): void
    {
        $this->attributes['date'] = Carbon::parse($date);
    }

    // Фильтрация
    public function filtering()
    {
        $queryBuilder = $this;

        $sessionData = session("{$this->entity()}", []);

        $filter = collect($sessionData)->except('page', 'method', 'sort_column', 'sort_direction')->toArray();

        $filter['used'] = false;

        $fields = collect($sessionData)
            ->keys()
            ->intersect($this->getFillable())
            ->toArray()
        ;

        if (in_array('id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('id', session("{$this->getTable()}.id"));
        }

        if (in_array('name', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('name', session("{$this->getTable()}.name"));
        }

        if (in_array('category_premise_id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('category_premise_id',  session("{$this->getTable()}.category_premise_id"));
        }

        if (in_array('count', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('count',  session("{$this->getTable()}.count"));
        }

        if (in_array('price', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('price',  session("{$this->getTable()}.price"));
        }

        $sessionData['filter'] = $filter;

        // Пишем его в сессию
        session([$this->entity() => $sessionData]);

        return $queryBuilder;
    }
}

<?php

namespace App\Models\Import;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $connection = 'import';

    protected $table = 'cat_articles';

	protected $fillable = [];
}

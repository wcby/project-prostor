<?php

namespace App\Models\Import;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $connection = 'import';

    protected $table = 'articles';

	protected $fillable = [];
}

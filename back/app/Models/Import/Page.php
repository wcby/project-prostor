<?php

namespace App\Models\Import;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $connection = 'import';

    protected $table = 'pages';

    protected $fillable = [];
}

<?php

namespace App\Models;

use Illuminate\Support\Str;

trait DefaultModelTrait
{
    public function scopeActive($query): mixed
    {
        return $query->where('is_active', 1);
    }

    public function scopeOrderDesc($query)
    {
        return $query->orderBy('id', 'desc');
    }

    public function scopeOrder($query)
    {
        return $query->orderBy('id', 'asc');
    }

    public function fieldsSelected(): array
    {
        if (has_cookie("fields_{$this->entity()}")) {

            $fields = collect(get_cookie("fields_{$this->entity()}", []))->unique()->toArray();
        } else {

            $fields = collect(config("app.{$this->entity()}.fields_selected_default", []))
                ->unique()
                ->toArray();
        }

        return $fields;
    }

    public function fieldsForShowing(): array
    {
        return collect(config("app.{$this->entity()}.fields_for_showing", []))
            ->unique()
            ->toArray();
    }

    public function getColumnSorting(): void
    {
        $sorting = get_cookie("sorting_{$this->entity()}");

        $column = $sorting['sortColumn'] ?? $this->columnDefault;
    }

    public function getDirectionSorting(): void
    {

    }

    public function entity(): string
    {
        return (string)Str::of($this->getTable())->replace('_', '-');
    }

    public function methodExists($method): bool
    {
        return method_exists($this, $method);
    }

    public function setMetaTitle($metaTitle): void
    {
        if (!empty($metaTitle) && $this->methodExists('metaTags')) {

            $metaTag = $this
                ->metaTags()
                ->firstOrNew();
            $metaTag->save();

            if ($metaTag->exists) {

                $metaTagI18n = $metaTag
                    ->entities()
                    ->firstOrNew([
                        'locale' => get_current_locale()
                    ]);

                $metaTagI18n
                    ->fill([
                        'meta_title' => $metaTitle,
                    ])
                    ->save();
            }
        }
    }

    public function setMetaKeywords($metaKeywords): void
    {
        if (!empty($metaKeywords) && $this->methodExists('metaTags')) {

            $metaTag = $this
                ->metaTags()
                ->firstOrNew();

            if ($metaTag->exists) {


                $metaTagI18n = $metaTag
                    ->entities()
                    ->firstOrNew([
                        'locale' => get_current_locale()
                    ]);

                $metaTagI18n
                    ->fill([
                        'meta_keywords' => $metaKeywords,
                    ])
                    ->save();
            }
        }
    }

    public function setMetaDescription($metaDescription): void
    {
        if (!empty($metaDescription && $this->methodExists('metaTags'))) {

            $metaTag = $this
                ->metaTags()
                ->firstOrNew();

            if ($metaTag->exists) {


                $metaTagI18n = $metaTag
                    ->entities()
                    ->firstOrNew([
                        'locale' => get_current_locale()
                    ]);

                $metaTagI18n
                    ->fill([
                        'meta_description' => $metaDescription,
                    ])
                    ->save();
            }
        }
    }
}

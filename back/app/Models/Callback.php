<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Callback extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'callbacks';

    protected $fillable = [
        'id',
        'title',                // Название заявки
        'name',                 // ФИО
        'phone',                // Телефон
        'type',                 // Тип
        'adult',                // Взрослые
        'child',                // Дети
        'check_in',             // Заезд
        'check_out',            // Выезд
    ];


    protected static function boot()
    {
        //
    }
}

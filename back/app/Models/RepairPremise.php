<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RepairPremise extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'repair_premises';

    protected $fillable = [
    'id',                   // ID
    'premise_id',           // Помещение
    'check_in',             // Дата начала ремонта
    'check_out'             // Дата окончания ремонта
];

    protected static function boot()
    {
        parent::boot();
        self::observe(Observers\RepairPremiseObserver::class);
    }

    public function premise()
    {
        return $this->belongsTo(Premise::class, 'premise_id');
    }
}

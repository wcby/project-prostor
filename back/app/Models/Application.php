<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends DefaultModel
{
    use SoftDeletes;

    protected $table = 'applications';

    protected $fillable = [
        'type_premises_id',     // тип заявки
        'adult',            // Взрослые
        'child',            // Дети
        'comment',          // Комментарий
        'total',            // Всего
        'pay',              // Оплата
        'payment_id',       // Способ Оплата
    ];

    protected $with = [
        'client',
        'payment',
    ];

    protected static function boot()
    {
        parent::boot();

        self::observe(Observers\ApplicationObserver::class);
    }

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function payment(): BelongsTo
    {
        return $this->belongsTo(Payment::class, 'payment_id');
    }

    public function premises(): HasMany
    {
        return $this->hasMany(ApplicationPremise::class, 'application_id', 'id');
    }


    public function getDateAttribute($date): string
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    public function setDateAttribute($date): void
    {
        $this->attributes['date'] = Carbon::parse($date);
    }

    // Фильтрация
    public function filtering()
    {
        $queryBuilder = $this;

        $sessionData = session("{$this->entity()}", []);

        $filter = collect($sessionData)->except('page', 'method', 'sort_column', 'sort_direction')->toArray();

        $filter['used'] = false;

        $fields = collect($sessionData)
            ->keys()
            ->intersect($this->getFillable())
            ->toArray()
        ;

        if (in_array('id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('id', session("{$this->getTable()}.id"));
        }

        if (in_array('client_id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('client_id', session("{$this->getTable()}.client_id"));
        }

        if (in_array('adult', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('adult',  session("{$this->getTable()}.adult"));
        }

        if (in_array('child', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('child',  session("{$this->getTable()}.child"));
        }

        if (in_array('comment', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('comment', 'like', '%' . session("{$this->getTable()}.comment") . '%');
        }

        if (in_array('total', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('total', 'like', '%' . session("{$this->getTable()}.total") . '%');
        }

        if (in_array('pay', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('pay', 'like', '%' . session("{$this->getTable()}.pay") . '%');
        }

        if (in_array('payment_id', $fields)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder->where('payment_id', session("{$this->getTable()}.payment_id"));
        }

        $sessionData['filter'] = $filter;

        // Пишем его в сессию
        session([$this->entity() => $sessionData]);

        return $queryBuilder;
    }
}

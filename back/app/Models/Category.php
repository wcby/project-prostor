<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends DefaultModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'categories';

    /**
     * ----------------------------------------------------------
     * The attributes that are mass assignable.
     * ----------------------------------------------------------
     * Атрибуты, которые могут быть присвоены в массовом порядке
     * ----------------------------------------------------------
     *
     * @var array
     */
    protected $fillable = [
        'name',             // Имя
        'is_active',        // Статус
    ];

    protected static function boot(): void
    {
        parent::boot();

        self::observe(Observers\CategoryObserver::class);
    }

    // Фильтрация
    public function filtering(): Category
    {
        $queryBuilder = $this;

        $sessionData = session($this->entity(), []);

        $filter = collect($sessionData)->except('page', 'method', 'sort_column', 'sort_direction')->toArray();

        $filter['used'] = false;

        $fields = collect($sessionData)
            ->keys()
            ->intersect($this->getFillable())
            ->toArray();

        if (in_array('id', $fields, true)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder::where('id', session("{$this->getTable()}.id"));
        }

        if (in_array('name', $fields, true)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder::where('name', 'like', '%' . session("{$this->getTable()}.name") . '%');
        }

        if (in_array('is_active', $fields, true)) {

            $filter['used'] = true;

            $queryBuilder = $queryBuilder::where('is_active', session("{$this->getTable()}.is_active"));
        }

        $sessionData['filter'] = $filter;

        // Пишем его в сессию
        session([$this->entity() => $sessionData]);

        return $queryBuilder;
    }

    public function getRestoreModels()
    {
        return false;
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends DefaultModel
{
    protected $table = "images";

    protected $fillable = [
        'id',           // ID
        'type',         // Тип
        'parent_id',    // ID смежной таблицы
        'path',         // Путь до файла
        'name',         // Название
    ];

    public function image(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }
}

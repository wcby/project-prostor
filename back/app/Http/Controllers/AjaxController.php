<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Image;
use App\Traits\ImageTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AjaxController extends CommonController
{
    use ImageTrait;

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {

            ini_set('memory_limit', '1536M');          // OK - 1536MB
            ini_set('post_max_size', '50M');          // OK - 1536MB
            ini_set('upload_max_filesize', '50M');    // OK - 100MB
            ini_set('max_execution_time', '60');
//            ini_set('memory_limit', 512000000);  // OK - 512MB
//            ini_set('upload_max_filesize', 100000000);  // OK - 10MB
//            ini_set('post_max_size', 100000000);  // OK - 10MB

            return $next($request);
        });
    }

    public function index(Request $request, $action)
    {
        if (method_exists($this, $action)) {

            return $this->$action($request);
        } else {

            abort(404);
        }
    }

    // Поиск по базе
    public function searching(Request $request) {
        $this->entity = $request->input('db');
        $search = $request->input('s');
        $columns = config("common.{$this->entity}.searching", []);

        $query = DB::table($this->entity)->select($columns);

        if($request->input('byID')) {

            $query->where('id', '=', $search)->whereNull('deleted_at');
        } else {

            $i = 1;
            foreach ($columns as $column) {
                if($i == 1) {
                    $query->where($column, 'like', '%' . $search . '%')->whereNull('deleted_at');
                } else {
                    $query->orWhere($column, 'like', '%' . $search . '%')->whereNull('deleted_at');
                }
                $i++;
            }
        }

        $model = $query->get();

        $response = ['data' => $model, 'entity' => $this->entity, 'type' => '/edit'];

        if($this->entity === 'questionnaires') {
            $response['types'] = config("common.{$this->entity}.types");
        }

        return response()->json($response);
    }

    // Добавление одного фото
    public function uploadImage(Request $request)
    {
        $result = [];

        try {

            if ($request->hasFile('files')) {
                $id = $request->input('id');
                $this->entity = $request->input('entity');
                $columnName = $request->input('columnName');
                $modelFullName = $request->input('modelFullName');

                $model = (new $modelFullName())->find($id);

                if ($model) {

                    $pathImage = "{$this->imagePath}/{$this->entity}/{$id}";

                    $this->miniatures = $this->miniatures($model->getTable());

                    foreach ($this->miniatures as $miniatureKey => $miniatureValue) {

                        $this->{$miniatureKey} = $miniatureValue;
                    }

                    $this->createDirectory($pathImage);

                    $file = $request->file('files')[0];
                    $path = public_path($pathImage);
                    $fileName = $this->createImage($file, $path);
                    $imageFilePath = "/" . trim($pathImage, "/") . "/{$fileName}";

                    $model->{$columnName} = $imageFilePath;
                    $model->save();

                    $result['imageHeader'] = false;

                    if ($model instanceof \App\Models\User && (int) auth()->id() === (int) $id) {

                        $result['imageHeader'] = true;
                    }

                    $result['imageFilePath'] = $imageFilePath;
                    $result['miniature'] = image_path($imageFilePath, 'thumbnail');
                    $result['success'] = true;
                    $result['message'] = __('common.file_successfully_created_and_written_to_database');
                } else {

                    $result['success'] = false;
                    $result['message'] = __('common.entry_missing', ['id' => $id]);
                }
            } else {
                $result['success'] = false;
                $result['message'] = __('common.no_file');
            }
        } catch (\Exception $e) {

            $result['success']  = false;
            $result['message'] = $e->getMessage();
        }

        return response()->json($result);
    }

    // Удаление одного фото
    public function deleteImage(Request $request)
    {
        $result = [];

        try {

            $id = $request->input('id');
            $this->entity = $request->input('entity');
            $columnName = $request->input('columnName');
            $modelFullName = $request->input('modelFullName');
            $imageFilePath = $request->input('imageFilePath');

            $model = (new $modelFullName())->find($id);

            if ($model) {

                $this->miniatures = $this->miniatures($model->getTable());

                if (!empty($imageFilePath) && file_exists(public_path($imageFilePath))) {

                    $this->removeLink($imageFilePath);
                }

                if ($model->methodExists('entities')) {

                    $modelI18n = $model
                        ->entities()
                        ->firstOrNew([
                            'locale' => get_current_locale(),
                        ])
                    ;

                    $modelI18n
                        ->fill(['value' => null])
                        ->save()
                    ;
                } else {

                    $model->{$columnName} = null;
                    $model->save();
                }

                $result['imageHeader'] = false;

                if ($model instanceof \App\Models\User && (int) auth()->id() === (int) $id) {

                    $result['imageHeader'] = true;
                }

                $result['success'] = true;
                $result['message'] = __('common.file_successfully_deleted');
            } else {

                $result['success'] = false;
                $result['message'] = __('common.entry_missing', ['id' => $id]);
            }
        } catch (\Exception $e) {

            $result['success']  = false;
            $result['message'] = $e->getMessage();
        }

        return response()->json($result);
    }

    // Переключение статуса
    public function activeToggle(Request $request)
    {
        $result = [];

        try {

            $id = (int) $request->input('id');
            $isActive = (int) $request->input('is_active');
            $name = $request->input('name');
            $entity = $request->input('entity');
            $modelFullName = $request->input('modelFullName');

            $model = (new $modelFullName())->find($id);

//            $model = DB::table($entity)->where('id', $id)->first();

            if ($model) {

                $model->{$name} = $isActive;
                $model->save();

//                DB::table($entity)->where('id', $id)->update(["{$name}" => $isActive]);

                $result['success'] = true;
                $result['message'] = "Статус у записи с id {$id} успешно изменен!";
            } else {

                $result['success'] = false;
                $result['message'] = "Запись с id {$id} отсутствует!";
            }
        } catch (\Exception $e) {

            $result['success']  = false;
            $result['message'] = $e->getMessage();
        }

        return response()->json($result);
    }

    public function multiExplode ($delimiters, $string) {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);

        return  collect($launch)
            ->filter()
            ->values()
            ->toArray()
        ;
    }

    public function stdClass(Request $request)
    {
        $requestData = $this->requestData($request);
        $stdClass = new \stdClass();

        foreach ($requestData as $key => $requestDatum) {

            $stdClass->{$key} = $requestDatum;
        }

        return $stdClass;
    }

    public function getArticles($mailing)
    {
        return Article::active()->where('date', Carbon::now(env('TIME_ZONE'))->subDays($mailing->last_days)->toDateString())->get();
    }

    // Добавление множества фото
    public function uploadImages(Request $request): \Illuminate\Http\JsonResponse
    {
        $result = [];
        $images = [];

        try {

            if ($request->hasFile('files')) {

                $id = $request->input('id');
                $morphClass = $request->input('morphClass');
                $imageEntity = $request->input('type');
                $request->merge(['entity' => Str::plural($imageEntity)]);

                $model = (new $morphClass())->find($id);

                if ($model) {

                    $pathImage = "/{$this->imagePath}/{$model->entity()}/{$id}";

                    $this->miniatures = $this->miniatures($model->getTable());

                    foreach ($this->miniatures as $miniatureKey => $miniatureValue) {

                        $this->{$miniatureKey} = $miniatureValue;
                    }

                    $this->createDirectory($pathImage);

                    foreach ($request->file('files') as $file) {

                        $path = public_path($pathImage);
                        $fileName = $this->createImage($file, $path);
                        $imageFilePath = "/" . trim($pathImage, "/") . "/{$fileName}";

                        $image = $model->images()->firstOrNew([
                            'type' => $imageEntity,
                            'path' => $imageFilePath,
                        ]);

                        $image->position = 999;
                        $image->save();

                        $images[] = [
                            'id' => $image->id,
                            'position' => $image->position,
                            'path' => asset(image_path("{$imageFilePath}", "thumbnail")),
                        ];
                    }

                    $result['id'] = $id;
                    $result['images'] = $images;
                    $result['morphClass'] = $morphClass;
                    $result['success'] = true;
                    $result['message'] = __("common.file_successfully_created_and_written_to_database");
                } else {

                    $result['success'] = false;
                    $result['message'] = __("common.entry_missing", ['id' => $id]);
                }
            } else {

                $result['success'] = false;
                $result['message'] = __("common.no_file");
            }
        } catch (\Exception $e) {
            $result['type'] = $request->all();
            $result['success']  = false;
            $result['message'] = $e->getMessage();
        }

        return response()->json($result);
    }

    // Удаление фото множества
    public function removeImages(Request $request): \Illuminate\Http\JsonResponse
    {
        $result = [];

        try {

            $id = $request->input('id');
            $imageId = $request->input('imageId');
            $morphClass = $request->input('morphClass');
            $imageEntity = $request->input('imageEntity');

            $model = (new $morphClass())->find($id);

            if ($model) {

                $request->merge(['entity' => Str::plural($model->getTable())]);

                $this->miniatures = $this->miniatures($model->getTable());

                $image = $model->images()->where('id', $imageId)->first();

                if ($image && file_exists(public_path("{$image->path}"))) {

                    $this->removeLink($image->path);
                    $this->removeLink($image->path . ".webp");
                }

                $model->images()->where('id', $image->id)->delete();

                $result['success'] = true;
                $result['message'] = 'Файл успешно удален!';
            } else {

                $result['success'] = false;
                $result['message'] = __("common.entry_missing", ['id' => $id]);
            }

        } catch (\Exception $e) {

            $result['success']  = false;
            $result['message'] = $e->getMessage();
        }

        return response()->json($result);
    }

    // Смена позиции у фото
    public function changePositionImages(Request $request)
    {
        $result = [];

        try {

            $images = $request->input('images');

            foreach ($images as $key => $image) {

                Image::where('id', $image['imageId'])->update(['position' => $key]);
            }

            $result['success'] = true;
            $result['message'] = 'Позиция успешно поменена!';
        } catch (\Exception $e) {

            $result['success']  = false;
            $result['message'] = $e->getMessage();
        }

        return response()->json($result);
    }

    // Получаем миниатюры по названию сущности
    public function miniatures($entity)
    {
        $this->miniatures = config("app.{$entity}.miniatures", []);

        if (empty($this->miniatures)) {

            $this->miniatures = config("app.common.miniatures", []);
        }

        return $this->miniatures;
    }

    public function autoCompleteContactsList(Request $request, $results = [])
    {
        $returnData = $request->all();

        $contacts = \App\Models\AmoContact::where('amo_name', 'like', '%' . $request->input('search') . '%')->get();

        if ($contacts->isNotEmpty()) {

            foreach ($contacts as $contact) {

                $results[] = array(
                    'value' => $contact->amo_id,
                    'label' => $contact->amo_name,
                    'success' => true,
                );
            }
        } else {

            $results[] = array(
                'value' => '',
                'label' => 'Клиенты с данными параметрами отсутствуют!',
                'success' => false,
            );
        }

        return response()->json($results);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\MenuItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class MenuItemController extends CommonController
{
    protected $entity = 'menu-items';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {

            View::share('entity', $this->entity);

            $this->setCollect([
                'breadcrumbs' => array_merge($this->getCollect('breadcrumbs'), [
                    [
                        'name' => __("menus.title_index"),
                        'url' => route("menus.index")
                    ],
                    [
                        'name' => $this->getCollect('titleIndex'),
                        'url' => route("menus.{$this->entity}.index", $request->menuId)
                    ],
                ]),
            ]);

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @param MenuItem $model
     * @param Menu $menu
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(MenuItem $model, Menu $menu)
    {
        $this->authorize('view', $menu);

        $models = $model->filtering($menu);

        $models = $models
            ->orderBy($model->columnSorting, $model->directionSorting)
            ->paginate($model->totalRecords)
        ;

        $models_count = $models->count();

        $redirectRouteName = __FUNCTION__;

        $this->setCommonData($model);

        $this
            ->setCollect('model', $model)
            ->setCollect('menu', $menu)
            ->setCollect('models', $models)
            ->setCollect('models_count', $models_count)
            ->setCollect('redirectRouteName', $redirectRouteName)
            ->setCollect('breadcrumbs', (String) View::make("partial.breadcrumb", $this->getCollect())->render());

        return view("{$this->entity}." . __FUNCTION__, $this->getCollect());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param MenuItem $model
     * @param Menu $menu
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(MenuItem $model, Menu $menu)
    {
        $this->authorize('create', $menu);

        $menuItems = $model->active()
            ->where('menu_id', $menu->id)
            ->get()
            ->pluck('name', 'id');

        $this
            ->setCollect([
                'breadcrumbs' => array_merge($this->getCollect('breadcrumbs'), [
                    [
                        'name' => $this->getCollect('titleCreate'),
                        'url' => route("menus.{$this->entity}." . __FUNCTION__, $menu)
                    ],
                ]),
            ])
            ->setCollect('model', $model)
            ->setCollect('menu', $menu)
            ->setCollect('menuItems', $menuItems)
            ->setCollect('breadcrumbs', (String) View::make("partial.breadcrumb", $this->getCollect())->render());

        return view("{$this->entity}." . __FUNCTION__, $this->getCollect());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param MenuItem $model
     * @param Menu $menu
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, MenuItem $model, Menu $menu)
    {
        $requestData = $request->all();
        $requestData['menu_id'] = $menu->id;
        $model = $model->create($requestData);

        return redirect(route("menus.{$this->entity}.edit", $model));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param MenuItem $model
     * @param Menu $menu
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Menu $menu, MenuItem $model)
    {
        $this->authorize('update', $menu);

        $menuItems = $model->active()
            ->where('menu_id', $menu->id)
            ->get()
            ->pluck('name', 'id');

        $this
            ->setCollect([
                'breadcrumbs' => array_merge($this->getCollect('breadcrumbs'), [
                    [
                        'name' => $this->getCollect('titleEdit'),
                        'url' => route("menus.{$this->entity}." . __FUNCTION__, [$menu, $model])
                    ],
                ]),
            ])
            ->setCollect('model', $model)
            ->setCollect('menu', $menu)
            ->setCollect('menuItems', $menuItems)
            ->setCollect('breadcrumbs', (String) View::make("partial.breadcrumb", $this->getCollect())->render());

        return view("{$this->entity}." . __FUNCTION__, $this->getCollect());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Menu $menu
     * @param MenuItem $model
     * @param MenuItem $model
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Menu $menu, MenuItem $model)
    {
        $requestData = $request->all();
        $requestData['menu_id'] = $menu->id;
        $model->update($requestData);

        return redirect(route("menus.{$this->entity}.index", $menu));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Menu $menu
     * @param MenuItem $model
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Request $request, MenuItem $model, Menu $menu)
    {
        $this->authorize('delete', $menu);
        $result = $model;
        return response()->json($result);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param Request $request
     * @param Menu $menu
     * @param MenuItem $model
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function restore(Request $request, MenuItem $model, Menu $menu)
    {
        $this->authorize('restore', $menu);

        if ($request->isMethod('GET')) {

            $models = $model->filtering();

            $models = $models
                ->onlyTrashed()
                ->orderBy($model->columnSorting, $model->directionSorting)
                ->paginate($model->totalRecords)
            ;

            $redirectRouteName = __FUNCTION__;

            $this->setCommonData($model);

            $this
                ->setCollect([
                    'breadcrumbs' => array_merge($this->getCollect('breadcrumbs'), [
                        [
                            'name' => $this->getCollect('titleRestore'),
                            'url' => route("menus.{$this->entity}." . __FUNCTION__, $menu)
                        ],
                    ]),
                ])
                ->setCollect('model', $model)
                ->setCollect('models', $models)
                ->setCollect('redirectRouteName', $redirectRouteName)
                ->setCollect('breadcrumbs', (String) View::make("partial.breadcrumb", $this->getCollect())->render());

            return view("{$this->entity}." . __FUNCTION__, $this->getCollect());
        } else {
            $result = $model;
            return response()->json($result);
        }
    }
}

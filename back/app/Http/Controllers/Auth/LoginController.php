<?php

namespace App\Http\Controllers\Auth;

//use App\Events\Authentication;
use App\Http\Controllers\CommonController;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class LoginController extends CommonController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected string $redirectTo = RouteServiceProvider::HOME;

    // Наше сообщение в лог
    protected $log_auth;
    protected $auth_status;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('guest')->except('logout');
    }

    public function __destruct()
    {
        if($this->log_auth){
            Log::channel('auth_log')->debug($this->log_auth. ' '. $this->auth_status);
        }
    }
    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param  mixed  $user
     * @return void
     */
    protected function authenticated(Request $request, $user): void
    {
        $this->auth_status = "authenticated";
    }

    protected function sendFailedLoginResponse(Request $request): void
    {
        $this->auth_status = "pass failure";
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    protected function validateLogin(Request $request): void
    {
        $this->log_auth = "from ".$request->getClientIp()." email:".$request->get('email');
        $this->auth_status = "mail failure";
        $request->validate([
            $this->username() => 'required|string|exists:users,' . $this->username() . ',is_active,1',
            'password' => 'required|string',
        ]);

    }
}

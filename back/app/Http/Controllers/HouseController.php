<?php

namespace App\Http\Controllers;

use App\Models\Additional;
use App\Models\AdditionalType;
use App\Models\CategoryPremise;
use App\Models\House;
use App\Models\ObjectsAdditional;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class HouseController extends CommonController
{
    use ImageTrait;

    protected $entity = 'houses';
    protected string $typeEntity = 'house';
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {

            View::share('entity', $this->entity);

            $this->setCollect([
                'breadcrumbs' => array_merge($this->getCollect('breadcrumbs'), [
                    [
                        'name' => $this->getCollect('titleIndex'),
                        'url' => route("{$this->entity}.index")
                    ],
                ]),
                'typeEntity' => $this->typeEntity,
            ]);

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @param House $model
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(House $model)
    {
        $this->authorize('view', $model);

        $models = $model->filtering();

        $models = $models
            ->orderBy($model->columnSorting, $model->directionSorting)
            ->paginate($model->totalRecords)
        ;

        $redirectRouteName = __FUNCTION__;

        $models_count = $models->count();

        $this->setCommonData($model);

        $this
            ->setCollect('model', $model)
            ->setCollect('models', $models)
            ->setCollect('models_count', $models_count)
            ->setCollect('redirectRouteName', $redirectRouteName)
            ->setCollect('breadcrumbs', (String) View::make("partial.breadcrumb", $this->getCollect())->render());

        return view(__FUNCTION__, $this->getCollect());
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param House $model
     * @param AdditionalType $additionalsType
     * @param CategoryPremise $categoryPremise
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(House $model, CategoryPremise $categoryPremise, AdditionalType $additionalsType)
    {
        $this->authorize('create', $model);
        $types = $categoryPremise->all();
        $additionalsType = $additionalsType->where('type_premise_id', $model->type_premise_id)->get();
        $additionalsChecked = null;
        $images = $model->images;
        $this
            ->setCollect([
                'breadcrumbs' => array_merge($this->getCollect('breadcrumbs'), [
                    [
                        'name' => $this->getCollect('titleCreate'),
                        'url' => route("{$this->entity}." . __FUNCTION__)
                    ],
                ]),
            ])
            ->setCollect('model', $model)
            ->setCollect('types', $types)
            ->setCollect('additionalsType', $additionalsType)
            ->setCollect('additionalsChecked', $additionalsChecked)
            ->setCollect('images', $images)
            ->setCollect('breadcrumbs', (String) View::make("partial.breadcrumb", $this->getCollect())->render());

        return view(__FUNCTION__, $this->getCollect());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param House $model
     * @param ObjectsAdditional $objectsAdditional
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, House $model, ObjectsAdditional $objectsAdditional)
    {
        $model = $model->create($request->all());

        $additionals = $request->get('additionals');
        if($additionals){
            foreach($additionals as $additional){
                $objectsAdditional = $objectsAdditional->create([
                    'house_id' => $model->id,
                    'gazebo_id' => null,
                    'additional_id' => $additional,
                    'is_active' => 1,
                ]);
            }
        }


        $model->setMetaTitle($request->input('meta_title'));
        $model->setMetaKeywords($request->input('meta_keywords'));
        $model->setMetaDescription($request->input('meta_description'));
        return redirect(route("{$this->entity}.edit", $model));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param House $model
     * @param AdditionalType $additionalsType
     * @param CategoryPremise $categoryPremise
     * @param ObjectsAdditional $objectsAdditional
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(House $model, CategoryPremise $categoryPremise, AdditionalType $additionalsType, ObjectsAdditional $objectsAdditional)
    {
        $this->authorize('update', $model);
        $id = $model->id;
        $types = $categoryPremise->all();
        $additionalsType = $additionalsType->where('is_active', 1)->get();
        $additionalsChecked = $objectsAdditional
            ->where('house_id', $id)
            ->pluck('additional_id')
            ->toArray();
        $images = $model->images;
        $this
            ->setCollect([
                'breadcrumbs' => array_merge($this->getCollect('breadcrumbs'), [
                    [
                        'name' => $this->getCollect('titleEdit'),
                        'url' => route("{$this->entity}." . __FUNCTION__, $model)
                    ],
                ]),
            ])
            ->setCollect('model', $model)
            ->setCollect('types', $types)
            ->setCollect('additionalsType', $additionalsType)
            ->setCollect('additionalsChecked', $additionalsChecked)
            ->setCollect('images', $images)
            ->setCollect('breadcrumbs', (String) View::make("partial.breadcrumb", $this->getCollect())->render());

        return view(__FUNCTION__, $this->getCollect());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param House $model
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, House $model, ObjectsAdditional $objectsAdditional)
    {
        $model->update($request->all());
        $objectsAdditional->where('house_id', $model->id)->delete();
        $additionals = $request->get('additionals');
        if($additionals){
            foreach($additionals as $additional){
                $objectsAdditional = $objectsAdditional->create([
                    'house_id' => $model->id,
                    'gazebo_id' => null,
                    'additional_id' => $additional,
                    'is_active' => 1,
                ]);
            }
        }
        $model->setMetaTitle($request->input('meta_title'));
        $model->setMetaKeywords($request->input('meta_keywords'));
        $model->setMetaDescription($request->input('meta_description'));
        return redirect(route("{$this->entity}.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param House $model
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Request $request, House $model)
    {
        $this->authorize('delete', $model);

        $result = $this->destroy_entity(
            $model,
            $request->input('ids')
        );

        return response()->json($result);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param Request $request
     * @param House $model
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function restore(Request $request, House $model)
    {
        $this->authorize('restore', $model);

        if ($request->isMethod('GET')) {

            $models = $model->filtering();
            $models = $models
                ->onlyTrashed()
                ->orderBy($model->columnSorting, $model->directionSorting)
                ->paginate($model->totalRecords)
            ;

            $redirectRouteName = __FUNCTION__;

            $this->setCommonData($model);

            $this
                ->setCollect([
                    'breadcrumbs' => array_merge($this->getCollect('breadcrumbs'), [
                        [
                            'name' => $this->getCollect('titleRestore'),
                            'url' => route("{$this->entity}." . __FUNCTION__)
                        ],
                    ]),
                ])
                ->setCollect('model', $model)
                ->setCollect('models', $models)
                ->setCollect('redirectRouteName', $redirectRouteName)
                ->setCollect('breadcrumbs', (String) View::make("partial.breadcrumb", $this->getCollect())->render());

            return view(__FUNCTION__, $this->getCollect());
        } else {

            $result = $this->restore_entity(
                $model,
                $request->input('ids')
            );

            return response()->json($result);
        }
    }
}

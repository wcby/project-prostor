<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StyleGuideController extends CommonController
{
    protected $entity = 'style-guide';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {

            return $next($request);
        });
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $this
            ->setCollect([
                'titleIndex' => 'Руководство по стилю',
            ]);

        return view("{$this->entity}.index", $this->getCollect());
    }

    public function forms()
    {
        $this
            ->setCollect([
                'titleIndex' => 'Формы',
            ]);

        return view("{$this->entity}.forms", $this->getCollect());
    }

    public function tables()
    {
        $this
            ->setCollect([
                'titleIndex' => 'Таблицы',
            ]);

        return view("{$this->entity}.tables", $this->getCollect());
    }
}

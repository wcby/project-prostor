<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StartController extends CommonController
{
    protected $entity = 'dashboard';

    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {

            $this
                ->setCollect([
                    'titleIndex' => config('app.entity.'.$this->entity.'.title_index'),
                ]);

            return $next($request);
        });
    }
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return Application|Factory|View
     */
    public function __invoke(Request $request)
    {

        $this
            ->setCollect([
            ]);

        return view("start", $this->getCollect());
    }
}

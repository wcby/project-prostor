<?php

namespace App\Http\Controllers;

use App\Models\Additional;
use App\Models\AdditionalType;
use App\Models\CategoryPremise;
use App\Models\Gallery;
use App\Models\Gazebo;
use App\Models\House;
use App\Models\Image;
use App\Models\ObjectsAdditional;
use App\Traits\ImageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class GazeboController extends CommonController
{

    use ImageTrait;

    protected $entity = 'gazebos';
    protected string $typeEntity = 'gazebo';
    public function __construct()
    {
        parent::__construct();

        $this->middleware(function ($request, $next) {

            View::share('entity', $this->entity);

            $this->setCollect([
                'breadcrumbs' => array_merge($this->getCollect('breadcrumbs'), [
                    [
                        'name' => $this->getCollect('titleIndex'),
                        'url' => route("{$this->entity}.index")
                    ],
                ]),
                'typeEntity' => $this->typeEntity,
            ]);

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @param Gazebo $model
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Gazebo $model)
    {
        $this->authorize('view', $model);

        $models = $model->filtering();

        $models = $models
            ->orderBy($model->columnSorting, $model->directionSorting)
            ->paginate($model->totalRecords)
        ;

        $redirectRouteName = __FUNCTION__;

        $models_count = $models->count();

        $this->setCommonData($model);

        $this
            ->setCollect('model', $model)
            ->setCollect('models', $models)
            ->setCollect('models_count', $models_count)
            ->setCollect('redirectRouteName', $redirectRouteName)
            ->setCollect('breadcrumbs', (String) View::make("partial.breadcrumb", $this->getCollect())->render());

        return view(__FUNCTION__, $this->getCollect());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Gazebo $model
     * @param AdditionalType $additionalsType
     * @param ObjectsAdditional $additionalsChecked
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Gazebo $model, AdditionalType $additionalsType, ObjectsAdditional $additionalsChecked)
    {
        $this->authorize('create', $model);
        $additionalsType = $additionalsType->where('type_premise_id', $model->type_premise_id)->get();
        $additionalsChecked = [];
        $images = $model->images;
        $this
            ->setCollect([
                'breadcrumbs' => array_merge($this->getCollect('breadcrumbs'), [
                    [
                        'name' => $this->getCollect('titleCreate'),
                        'url' => route("{$this->entity}." . __FUNCTION__)
                    ],
                ]),
            ])
            ->setCollect('model', $model)
            ->setCollect('additionalsType', $additionalsType)
            ->setCollect('additionalsChecked', $additionalsChecked)
            ->setCollect('images', $images)
            ->setCollect('breadcrumbs', (String) View::make("partial.breadcrumb", $this->getCollect())->render());

        return view(__FUNCTION__, $this->getCollect());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Gazebo $model
     * @param ObjectsAdditional $objectsAdditional
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request, Gazebo $model, ObjectsAdditional $objectsAdditional)
    {
        $requestData = $request->all();

        $requestData['imageEntity'] = $model->imageEntity;
        $request->merge($requestData);
        $model = $model->create($request->all());
        $additionals = $request->get('additionals');
        if($additionals){
            foreach($additionals as $additional){
                $objectsAdditional = $objectsAdditional->create([
                    'house_id' => null,
                    'gazebo_id' => $model->id,
                    'additional_id' => $additional,
                    'is_active' => 1,
                ]);
            }
        }
        return redirect(route("{$this->entity}.edit", $model));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param Gazebo $model
     * * @param AdditionalType $additionalsType
     * * @param ObjectsAdditional $objectsAdditional
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Gazebo $model, AdditionalType $additionalsType, ObjectsAdditional $objectsAdditional)
    {
        $this->authorize('update', $model);

        $id = $model->id;
        $additionalsType = $additionalsType->where('is_active', 1)->get();
        $additionalsChecked = $objectsAdditional->where('gazebo_id', $id)->pluck('additional_id')->toArray();
        $images = $model->images;
        $this
            ->setCollect([
                'breadcrumbs' => array_merge($this->getCollect('breadcrumbs'), [
                    [
                        'name' => $this->getCollect('titleEdit'),
                        'url' => route("{$this->entity}." . __FUNCTION__, $model)
                    ],
                ]),
            ])
            ->setCollect('model', $model)
            ->setCollect('additionalsType', $additionalsType)
            ->setCollect('additionalsChecked', $additionalsChecked)
            ->setCollect('images', $images)
            ->setCollect('breadcrumbs', (String) View::make("partial.breadcrumb", $this->getCollect())->render());

        return view(__FUNCTION__, $this->getCollect());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Gazebo $model
     * @param ObjectsAdditional $objectsAdditional
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Gazebo $model, ObjectsAdditional $objectsAdditional)
    {
        $model->update($request->all());
        $objectsAdditional->where('gazebo_id', $model->id)->delete();
        $additionals = $request->get('additionals');
        if($additionals){
            foreach($additionals as $additional){
                $objectsAdditional = $objectsAdditional->create([
                    'house_id' => null,
                    'gazebo_id' => $model->id,
                    'additional_id' => $additional,
                    'is_active' => 1,
                ]);
            }
        }
        return redirect(route("{$this->entity}.index"));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Gazebo $model
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Request $request, Gazebo $model)
    {
        $this->authorize('delete', $model);

        $result = $this->destroy_entity(
            $model,
            $request->input('ids')
        );

        return response()->json($result);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param Request $request
     * @param Gazebo $model
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function restore(Request $request, Gazebo $model)
    {
        $this->authorize('restore', $model);

        if ($request->isMethod('GET')) {

            $models = $model->filtering();
            $models = $models
                ->onlyTrashed()
                ->orderBy($model->columnSorting, $model->directionSorting)
                ->paginate($model->totalRecords)
            ;

            $redirectRouteName = __FUNCTION__;

            $this->setCommonData($model);

            $this
                ->setCollect([
                    'breadcrumbs' => array_merge($this->getCollect('breadcrumbs'), [
                        [
                            'name' => $this->getCollect('titleRestore'),
                            'url' => route("{$this->entity}." . __FUNCTION__)
                        ],
                    ]),
                ])
                ->setCollect('model', $model)
                ->setCollect('models', $models)
                ->setCollect('redirectRouteName', $redirectRouteName)
                ->setCollect('breadcrumbs', (String) View::make("partial.breadcrumb", $this->getCollect())->render());

            return view(__FUNCTION__, $this->getCollect());
        } else {

            $result = $this->restore_entity(
                $model,
                $request->input('ids')
            );

            return response()->json($result);
        }
    }
}

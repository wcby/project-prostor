@extends('layouts.app')

@section('title')Профиль@endsection

@push('header')
    <h1 class="h2 reset-m">@yield('title')</h1>
    <ol class="breadcrumb">
        {!! $breadcrumbs !!}
    </ol>
@endpush

@section('content')
    <div class="panel">
        <form action="{{ route("{$entity}.profile.update") }}" method="post" enctype="multipart/form-data">
            @method('put')
            @csrf
            <div class="panel__head">
                <div class="panel__actions">
                    <div class="panel__actions-item right">
                        <button type="submit" class="btn btn--medium btn--green">
                            <i class="fas fa-check btn__icon"></i>
                            <span class="btn__text btn__text--right">{{config('app.entity.common.save')}}</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="panel__body">
                <div class="panel__content">
                    <div class="row form__group-wrap">
                        <div class="col-xs-12 col-md-8 col-lg-7 col-xl-6">
                            <div class="form__group form__group--input">
                            @if($model->exists)
                                {!! $avatar !!}
                            @endif
                            </div>
                            <div class="form__group form__group--input">
                                <div class="row row--small row--ai-center">
                                    <div class="col-xs-12 col-sm-2 text-sm-right">
                                        <label class="form__label form__label--sm-left" for="name">{{config('app.entity.'.$entity.'.name')}}</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-10">
                                        <input type="text" name="name" class="form__input form__input--large @error('name'){{ 'is-invalid' }}@enderror" id="name" value="{{ old('name') ?? $model->name }}" required autofocus>
                                    </div>
                                    <div class="col-xs-12">
                                    @error('name')
                                        <div class="form__message form__message--error">{{ $message }}</div>
                                    @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form__group form__group--input">
                                <div class="row row--small row--ai-center">
                                    <div class="col-xs-12 col-sm-2 text-sm-right">
                                        <label class="form__label form__label--sm-left" for="email">{{config('app.entity.'.$entity.'.email')}}</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-10">
                                        <input type="email" name="email" class="form__input form__input--large @error('email'){{ 'is-invalid' }}@enderror" id="email" value="{{ old('email') ?? $model->email }}" required>
                                    </div>
                                    <div class="col-xs-12">
                                    @error('email')
                                        <div class="form__message form__message--error">{{ $message }}</div>
                                    @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form__group form__group--input">
                                <div class="row row--small row--ai-center">
                                    <div class="col-xs-12 col-sm-2 text-sm-right">
                                        <label class="form__label form__label--sm-left" for="phone">{{config('app.entity.'.$entity.'.phone')}}</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-10">
                                        <input type="text" name="phone" class="form__input form__input--large @error('phone'){{ 'is-invalid' }}@enderror" id="phone" value="{{ old('phone') ?? $model->phone }}">
                                    </div>
                                    <div class="col-xs-12">
                                    @error('phone')
                                        <div class="form__message form__message--error">{{ $message }}</div>
                                    @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form__group form__group--input">
                                <div class="row row--small row--ai-center">
                                    <div class="col-xs-12 col-sm-3 col-md-4 text-sm-right">
                                        <label class="form__label form__label--sm-left" for="password">{{config('app.entity.'.$entity.'.password')}}</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-9 col-md-8">
                                        <input type="password" name="password" class="form__input form__input--large" id="password">
                                    </div>
                                    <div class="col-xs-12">
                                    @if ($errors->has('password'))
                                        <div class="form__message form__message--error">{{ $errors->first('password') }}</div>
                                    @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel__footer">
                <div class="panel__actions">
                    <div class="panel__actions-item right">
                        <button type="submit" class="btn btn--medium btn--green">
                            <i class="fas fa-check btn__icon"></i>
                            <span class="btn__text btn__text--right">{{config('app.entity.common.save')}}</span>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

<div class="panel__body">
    <div class="panel__content">
        <div class="row form__group-wrap">
            <div class="col-xs-12">
                <div class="form__group form__group--input">
                @if($model->exists)
                    {!! $avatar !!}
                @endif
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="name">@lang("{$entity}.name")</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="text" name="name" class="form__input form__input--large @error('name'){{ 'is-invalid' }}@enderror" id="name" value="{{ old('name') ?? $model->name }}" required autofocus>
                        </div>
                        <div class="col-xs-12">
                        @error('name')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="email">@lang("{$entity}.email")</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="email" name="email" class="form__input form__input--large @error('email'){{ 'is-invalid' }}@enderror" id="email" value="{{ old('email') ?? $model->email }}" required>
                        </div>
                        <div class="col-xs-12">
                        @error('email')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="phone">@lang("{$entity}.phone")</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="text" name="phone" class="form__input form__input--large @error('phone'){{ 'is-invalid' }}@enderror" id="phone" value="{{ old('phone') ?? $model->phone }}">
                        </div>
                        <div class="col-xs-12">
                        @error('phone')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="password">@lang("{$entity}.password")</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="password" name="password" class="form__input form__input--large @error('password'){{ 'is-invalid' }}@enderror" id="password" @if (!$model->exists){{ 'required' }}@endif>
                        </div>
                        <div class="col-xs-12">
                        @error('password')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="role_id">@lang("{$entity}.role_id")</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <select name="role_id" id="role_id" class="form__select form__select--large @error('role_id'){{ 'is-invalid' }}@enderror">
                                <option value="">@lang("app.common.select")</option>
                                @foreach ($roles as $key => $roleName)
                                <option value="{{ $key }}" @if ((int) $model->role_id === (int) $key || (int) $key === (int) old('role_id')){{ 'selected' }}@endif>{{ $roleName }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-12">
                        @error('role_id')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="is_active">@lang("{$entity}.is_active")</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <label class="form__toggle @error('is_active'){{ 'is-invalid' }}@enderror">
                                <input type="hidden" name="is_active" value="0">
                                <input class="form__toggle-input" type="checkbox" id="is_active" name="is_active" value="1" @if ($model->is_active){{ 'checked' }}@endif>
                                <span class="form__toggle-icon"></span>
                            </label>
                        </div>
                        <div class="col-xs-12">
                        @error('is_active')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

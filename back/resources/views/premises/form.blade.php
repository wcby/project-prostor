<div class="panel__body">
    <div class="panel__content">
        <div class="row form__group-wrap">
            <div class="col-xs-12">
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="name">{{config('app.entity.'.$entity.'.name')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="text" name="name"
                                   class="form__input form__input--large @error('name'){{ 'is-invalid' }}@enderror"
                                   id="name" value="{{ old('name') ?? $model->name }}" required autofocus>
                        </div>
                        <div class="col-xs-12">
                            @error('name')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="type_premise_id">
                                {{config('app.entity.'.$entity.'.type_premise_id')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <select name="type_premise_id" id="type_premise_id" class="form__select form__select--medium">
                                <option value="">Выбрать</option>
                                @foreach($types as $type)
                                    <option value="{{ $type->id }}" @if ($model->type_premise_id === $type->id)
                                        {{ 'selected' }}
                                        @endif>
                                        {{ $type->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-12">
                            @error('type_premise_id')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="category_premise_id">
                                {{config('app.entity.'.$entity.'.category_premise_id')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <select name="category_premise_id"
                                    id="category_premise_id"
                                    class="form__select form__select--medium"
                            >
                                <option value="">Выбрать</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" @if ($model->category_premise_id === $category->id)
                                        {{ 'selected' }}
                                        @endif>
                                        {{ $category->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-12">
                            @error('category_premise_id')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="cost">{{config('app.entity.'.$entity.'.cost')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="text" name="cost"
                                   class="form__input form__input--large @error('cost'){{ 'is-invalid' }}@enderror"
                                   id="cost" value="{{ old('cost') ?? $model->cost }}" required autofocus>
                        </div>
                        <div class="col-xs-12">
                            @error('cost')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left"
                                   for="is_active">{{config('app.entity.'.$entity.'.is_active')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <label class="form__toggle @error('is_active'){{ 'is-invalid' }}@enderror">
                                <input type="hidden" name="is_active" value="0">
                                <input class="form__toggle-input" type="checkbox" id="is_active" name="is_active"
                                       value="1" @if ($model->is_active)
                                    {{ 'checked' }}
                                    @endif>
                                <span class="form__toggle-icon"></span>
                            </label>
                        </div>
                        <div class="col-xs-12">
                            @error('is_active')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@includeIf("partial.meta-tags")

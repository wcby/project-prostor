<div class="panel__body">
    <div class="panel__content">
        <div class="row form__group-wrap">
            <div class="col-xs-12">
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="name">{{config('app.entity.'.$entity.'.name')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="text" name="name"
                                   placeholder="ФИО"
                                   class="form__input form__input--large @error('name'){{ 'is-invalid' }}@enderror"
                                   id="name" value="{{ old('name') ?? $model->name }}" required autofocus>
                        </div>
                        <div class="col-xs-12">
                            @error('name')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="phone">{{config('app.entity.'.$entity.'.phone')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="tel" name="phone"
                                   placeholder="Начните с +7"
                                   class="form__input form__input--large @error('phone'){{ 'is-invalid' }}@enderror"
                                   id="phone" value="{{ old('phone') ?? $model->phone }}" maxlength="12" required autofocus>
                        </div>
                        <div class="col-xs-12">
                            @error('phone')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="email">{{config('app.entity.'.$entity.'.email')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="email" name="email"
                                   class="form__input form__input--large @error('email'){{ 'is-invalid' }}@enderror"
                                   id="email" value="{{ old('email') ?? $model->email }}">
                        </div>
                        <div class="col-xs-12">
                            @error('phone')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@includeIf("partial.meta-tags")

<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" type="text/css" />
    @stack('custom-styles')
    <link rel="shortcut icon" href="{{ asset("assets/images/favicon.png") }}" />
    <title>@yield('title')</title>
    @if(config('app.env') == 'production')
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();
            for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
            k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(94723009, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/94723009" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    @endif
</head>
<body>
    <div class="page">
        <header class="page__header header">
            @include("include.header")
        </header>
        <div class="page__content">
            <aside class="page__sidebar sidebar">
                @include("partial.sidebar.items")
            </aside>
            <main class="page__main main">
                @stack("header")
                @yield('content')
            </main>
        </div>
        <footer class="page__footer footer">
            @include("include.footer")
        </footer>
    </div>
    <script src="{{ asset("assets/plugins/tinymce/tinymce.min.js") }}"></script>
    <script src="{{ asset("assets/js/app.js?ver=" . hash_file('md5', 'assets/js/app.js')) }}"></script>
    <script>

      $(document).ready(function (){

      });

    </script>
</body>
</html>

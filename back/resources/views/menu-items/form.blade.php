<div class="panel__body">
    <div class="panel__content">
        <div class="row form__group-wrap">
            <div class="col-xs-12">
                {{-- name --}}
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="name">{{config('app.entity.'.$entity.'.name')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="text" name="name" class="form__input form__input--large @error('name'){{ 'is-invalid' }}@enderror" id="name" value="{{ old('name') ?? $model->name }}" required autofocus>
                        </div>
                        <div class="col-xs-12">
                        @error('name')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                {{-- url --}}
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="url">{{config('app.entity.'.$entity.'.url')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="text" name="url" class="form__input form__input--large @error('url'){{ 'is-invalid' }}@enderror" id="url" value="{{ old('url') ?? $model->url }}">
                        </div>
                        <div class="col-xs-12">
                        @error('url')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                {{-- parent_id --}}
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="parent_id">{{config('app.entity.'.$entity.'.parent_id')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <select name="parent_id" id="parent_id" class="form__select form__select--medium">
                                <option value="">Выбрать</option>
                                @foreach($menuItems as $key => $menuItem)
                                    <option value="{{ $key }}" @if ($model->parent_id === $key){{ 'selected' }}@endif>
                                        {{ $menuItem }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-12">
                        @error('parent_id')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                {{-- position --}}
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="position">{{config('app.entity.'.$entity.'.position')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="number" name="position" class="form__input form__input--large @error('position'){{ 'is-invalid' }}@enderror" id="position" value="{{ old('position') ?? $model->position }}">
                        </div>
                        <div class="col-xs-12">
                        @error('position')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="is_active">{{config('app.entity.'.$entity.'.is_active')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <label class="form__toggle @error('is_active'){{ 'is-invalid' }}@enderror">
                                <input type="hidden" name="is_active" value="0">
                                <input class="form__toggle-input" type="checkbox" id="is_active" name="is_active" value="1" @if ($model->is_active){{ 'checked' }}@endif>
                                <span class="form__toggle-icon"></span>
                            </label>
                        </div>
                        <div class="col-xs-12">
                        @error('is_active')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


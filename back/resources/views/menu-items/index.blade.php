@extends('layouts.app')

@section('title'){{ $titleIndex }}@endsection

@push('header')
    <h1 class="h2 reset-m">@yield('title')</h1>
    <ol class="breadcrumb">
        {!! $breadcrumbs !!}
    </ol>
@endpush

@section('content')
    <div class="panel">
        <div class="panel__head">
            <div class="panel__actions">
                @if (Route::has("menus.{$entity}.create"))
                <div class="panel__actions-item">
                @can('create', $menu)
                    <a href="{{ route("menus.{$entity}.create", $menu) }}" class="btn btn--medium-square-xs btn--green">
                        <i class="fas fa-plus btn__icon"></i>
                        <span class="btn__text btn__text--right xs-hide">{{config('app.entity.common.create')}}</span>
                    </a>
                @endcan
                </div>
                @endif
                @if (Route::has("menus.{$entity}.destroy"))
                <div class="panel__actions-item">
                @can('delete', $menu)
                    <button type="button" class="btn btn--medium-square-xs btn--orange js-delete-items" style="display: none;">
                        <i class="fas fa-trash-alt btn__icon"></i>
                        <span class="btn__text btn__text--right xs-hide">{{config('app.entity.common.delete')}}</span>
                    </button>
                @endcan
                </div>
                @endif
                <div class="panel__actions-item right">
                @if (Route::has("menus.{$entity}.restore"))
                @can('restore', $menu)
                    <a href="{{ route("menus.{$entity}.restore", $menu) }}" class="btn btn--medium-square-xs btn--white">
                        <i class="fas fa-trash-restore-alt btn__icon"></i>
                        <span class="btn__text btn__text--right xs-hide">{{config('app.entity.common.trash')}}</span>
                    </a>
                @endcan
                @endif
                </div>
                <div class="panel__actions-item">
                    <a href="#filter" class="btn btn--white btn--medium-square js-slide-toggle">
                        <i class="fas fa-filter"></i>
                        @if($filter['used'])
                            @if($models->isNotEmpty() && isset($models_count))
                                <span class="btn__text btn__text--right">{{ $models_count }}</span>
                            @endif
                        @endif
                    </a>
                </div>
                @if($filter['used'])
                <div class="panel__actions-item">
                    <a class="btn btn--medium-square btn--orange" href="{{ route("menus.{$entity}.filter", $menu) }}?method={{ $redirectRouteName }}&reset">
                        <i class="fas fa-filter"></i>
                        <span class="btn__reset-line"></span>
                    </a>
                </div>
                @endif
                @include("partial.fields")
            </div>
        </div>
        @include("{$entity}.filter")
        <div class="panel__body">
            @if (Route::has("menus.{$entity}.destroy"))
            <form action="{{ route("menus.{$entity}.destroy", $menu) }}" id="delete-form" method="post" enctype="multipart/form-data">
            @endif
                @method('DELETE')
                @csrf
                <div class="panel__table-wrap">
                    <table class="panel__table">
                        @if($models->isNotEmpty())
                        <thead>
                            <tr>
                                @if (Route::has("menus.{$entity}.destroy"))
                                <th class="check">
                                    <label class="form__checkbox">
                                        <input class="form__checkbox-input js-checks" type="checkbox">
                                        <span class="form__checkbox-icon"></span>
                                    </label>
                                </th>
                                @endif
                                <th class="id">
                                @if(in_array('id', $fieldsSorting))
                                    <a class="js-sort-btn" data-sort-column="id" data-sort-direction="{{ 'id' === $column ? $directions->diff([$direction])->first() : $directionDefault }}" data-entity="{{ $model->entity() }}" href="javascript:void(0);">
                                        {{config('app.entity.'.$entity.'.id')}}
                                        <i class="fas fa-sort{{ 'id' === $column ? $direction === 'asc' ? '-down' : '-up' : '' }}"></i>
                                    </a>
                                @else
                                        {{config('app.entity.'.$entity.'.id')}}
                                @endif
                                </th>
                                @foreach($model->fieldsSelected() as $field)
                                @if($field === 'id')
                                @elseif(in_array($field, $fieldsImages))
                                    <th class="img">{{config('app.entity.'.$entity.'.'.$field)}}</th>
                                @else
                                    <th class="@if (in_array($field, $fieldsFull)){{ 'full' }}@endif">
                                    @if(in_array($field, $fieldsSorting))
                                        <a class="js-sort-btn" data-sort-column="{{ $field }}" data-sort-direction="{{ $field === $column ? $directions->diff([$direction])->first() : $directionDefault }}" data-entity="{{ $model->entity() }}" href="javascript:void(0);">
                                            {{config('app.entity.'.$entity.'.'.$field)}}
                                            <i class="fas fa-sort{{ $field === $column ? $direction === 'asc' ? '-down' : '-up' : '' }}"></i>
                                        </a>
                                    @else
                                            {{config('app.entity.'.$entity.'.'.$field)}}
                                    @endif
                                    </th>
                                @endif
                                @endforeach
                                @if (Route::has("menus.{$entity}.destroy"))
                                <th>{{config('app.entity.common.actions')}}</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($models as $key => $model)
                                <tr data-id="{{ $model->id }}" data-model-full-name="{{ $modelFullName }}">
                                    @if (Route::has("menus.{$entity}.destroy"))
                                    <td class="check">
                                        <label class="form__checkbox">
                                            <input class="form__checkbox-input js-check" value="{{ $model->id }}" name="ids[]" type="checkbox">
                                            <span class="form__checkbox-icon"></span>
                                        </label>
                                    </td>
                                    @endif
                                    <td class="id">
                                        @if(in_array('id', $fieldsLinks) && auth()->user()->can('update', $menu))
                                            <a href="{{ route("menus.{$entity}.edit", [$menu, $model]) }}" class="btn btn--white btn--default">{{$model->id}}</a>
                                        @else
                                            {{ $model->id }}
                                        @endif
                                    </td>

                                    @if($model->getTable() === 'settings' && $model->type === 'image')
                                        @php(array_push($fieldsImages, 'value'))
                                    @endif

                                    @foreach($model->fieldsSelected() as $field)
                                    @if($field === 'id')
                                    @elseif(in_array($field, $fieldsImages))
                                        <td class="img">
                                        @if(in_array($field, $fieldsLinks) && auth()->user()->can('update', $menu))
                                            <a href="{{ route("menus.{$entity}.edit", [$menu, $model]) }}">
                                                @if(empty($model->{$field}))
                                                    <img src="{{ asset("assets/images/no-photo.jpg") }}" alt="{{ $model->{$field} }}">
                                                @else
                                                    <img src="{{ asset(image_path("{$model->$field}", 'thumbnail')) }}" alt="{{ $model->{$field} }}">
                                                @endif
                                            </a>
                                        @else
                                            @if(empty($model->{$field}))
                                                <img src="{{ asset("assets/images/no-photo.jpg") }}" alt="{{ $model->{$field} }}">
                                            @else
                                                <img src="{{ asset(image_path("{$model->$field}", __("app.{$entity}.{$field}") !== 'Фото широкое' ? 'thumbnail' : '')) }}" alt="{{ $model->{$field} }}">
                                            @endif
                                        @endif
                                        </td>
                                    @elseif(in_array($field, $fieldsCheckbox))
                                        <td class="full">
                                            <label class="form__toggle">
                                                <input class="form__toggle-input js-active-toggle" type="checkbox" name="{{ $field }}" {{ $model->{$field} ? 'checked' : '' }} @cannot('update', $menu){{ 'disabled' }}@endcannot>
                                                <span class="form__toggle-icon"></span>
                                            </label>
                                        </td>
                                    @else
                                        <td class="full">
                                        @if(in_array($field, $fieldsLinks) && auth()->user()->can('update', $menu))
                                            <a href="{{ route("menus.{$entity}.edit", [$menu, $model]) }}">
                                                @if (in_array($field, array_keys($fieldsRelationships)))
                                                    {!!  isset($model->{$fieldsRelationships[$field]}->name) ? $model->{$fieldsRelationships[$field]}->name : '---'  !!}
                                                @elseif (in_array($field, array_keys($fieldsConfig)))
                                                    {!!  isset(${$fieldsConfig[$field]}[$model->{$field}]) ? ${$fieldsConfig[$field]}[$model->{$field}] : '---'  !!}
                                                @else
                                                    {!!  $model->{$field}  !!}
                                                @endif
                                            </a>
                                        @else
                                            @if (in_array($field, array_keys($fieldsRelationships)))
                                                {!! isset($model->{$fieldsRelationships[$field]}->name) ? $model->{$fieldsRelationships[$field]}->name : '---' !!}
                                            @elseif (in_array($field, array_keys($fieldsConfig)))
                                                {!!  isset(${$fieldsConfig[$field]}[$model->{$field}]) ? ${$fieldsConfig[$field]}[$model->{$field}] : '---'  !!}
                                            @else
                                                {!! $model->{$field} !!}
                                            @endif
                                        @endif
                                        </td>
                                    @endif
                                    @endforeach
                                    @if (Route::has("menus.{$entity}.destroy"))
                                    <td>
                                        @includeIf("menus.{$entity}.show-btn")
                                        <button type="button" class="btn btn--orange btn--default-square js-delete-item">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                        @else
                        <tbody>
                            <tr>
                                <td colspan="{{ count($model->fieldsSelected()) }}" style="text-align: center; vertical-align: middle;">{{config('app.entity.common.elements_are_missing')}}</td>
                            </tr>
                        </tbody>
                        @endif
                    </table>
                </div>
            </form>
        </div>
        @if($models->hasPages())
        <div class="panel__footer">
            {{ $models->links("partial.pagination") }}
        </div>
        @endif
    </div>
    @include("modals.remove")
    @includeIf("{$entity}.modals")
@endsection

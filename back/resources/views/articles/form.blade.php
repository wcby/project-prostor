<div class="panel__body">
    <div class="panel__content">
        <div class="row form__group-wrap">
            <div class="col-xs-12">
                @if($model->exists)
                    <div class="row">
                        <div class="col-xs-12 col-sm-7">
                            <div class="form__group form__group--input">
                                {!! $photo !!}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="name">{{config('app.entity.'.$entity.'.name')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="text" name="name" class="form__input form__input--large @error('name'){{ 'is-invalid' }}@enderror" id="name" value="{{ old('name') ?? $model->name }}" required autofocus>
                        </div>
                        <div class="col-xs-12">
                        @error('name')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="url">{{config('app.entity.'.$entity.'.url')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="text" name="url" class="form__input form__input--large @error('url'){{ 'is-invalid' }}@enderror" id="url" value="{{ old('url') ?? $model->url }}">
                        </div>
                        <div class="col-xs-12">
                        @error('url')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="date">{{config('app.entity.'.$entity.'.date')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="text" name="date" class="form__input form__input--large form__date @error('date'){{ 'is-invalid' }}@enderror" id="date" value="{{ old('date') ?? $model->date }}">
                        </div>
                        <div class="col-xs-12">
                        @error('date')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="brief_description">{{config('app.entity.'.$entity.'.brief_description')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <textarea name="brief_description" class="form__tinymce @error('brief_description'){{ 'is-invalid' }}@enderror" id="brief_description">{{ old('brief_description') ?? $model->brief_description }}</textarea>
                        </div>
                        <div class="col-xs-12">
                        @error('brief_description')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="description">{{config('app.entity.'.$entity.'.brief_description')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <textarea name="description" class="form__tinymce @error('description'){{ 'is-invalid' }}@enderror" id="description">{{ old('description') ?? $model->description }}</textarea>
                        </div>
                        <div class="col-xs-12">
                        @error('description')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="is_favourite">{{config('app.entity.'.$entity.'.is_favourite')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <label class="form__toggle @error('is_active'){{ 'is-invalid' }}@enderror">
                                <input type="hidden" name="is_favourite" value="0">
                                <input class="form__toggle-input" type="checkbox" id="is_favourite" name="is_favourite" value="1" @if ($model->is_favourite){{ 'checked' }}@endif>
                                <span class="form__toggle-icon"></span>
                            </label>
                        </div>
                        <div class="col-xs-12">
                        @error('is_favourite')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="is_active">{{config('app.entity.'.$entity.'.is_active')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <label class="form__toggle @error('is_active'){{ 'is-invalid' }}@enderror">
                                <input type="hidden" name="is_active" value="0">
                                <input class="form__toggle-input" type="checkbox" id="is_active" name="is_active" value="1" @if ($model->is_active){{ 'checked' }}@endif>
                                <span class="form__toggle-icon"></span>
                            </label>
                        </div>
                        <div class="col-xs-12">
                        @error('is_active')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@includeIf("partial.meta-tags")
{{--@if($model->exists)--}}
{{--<div class="panel__head">--}}
{{--    <h4 class="reset-m">@lang("common.recommended")</h4>--}}
{{--</div>--}}
{{--<div class="panel__body">--}}
{{--    <div class="panel__content">--}}
{{--        <div class="row form__group-wrap">--}}
{{--            <div class="col-xs-12">--}}
{{--                <div class="form__group form__group--input">--}}
{{--                    <div class="row row--small row--ai-center">--}}
{{--                        <div class="col-xs-12">--}}
{{--                            <label class="form__label form__label--sm-left" for="recommended">Выбрать статью</label>--}}
{{--                        </div>--}}
{{--                        <div class="col-xs-12">--}}
{{--                            <input type="text" name="recommended" class="form__input form__input--large js-recommended-autocomplete" id="recommended" placeholder="Начните вводить текст">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                @includeIf("partial.recommended")--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endif--}}

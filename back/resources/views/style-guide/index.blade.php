@extends("layouts.app")

@section('title'){{ $titleIndex }}@endsection

@push('header')
    <h1 class="h2 reset-m">@yield('title')</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="http://spas.loc">Главная</a>
        </li>
        <li class="breadcrumb-item active">@yield('title')</li>
    </ol>
@endpush

@section('content')
    <div class="panel">
        <h3 class="reset-mt">Таблицы</h3>
        <div class="panel__head">
            <div class="panel__actions">
                <div class="panel__actions-item">
                    <a href="http://spas.loc/users/create" class="btn btn--medium-square-xs btn--green">
                        <i class="fas fa-plus btn__icon"></i>
                        <span class="btn__text btn__text--right xs-hide">Создать</span>
                    </a>
                </div>
                <div class="panel__actions-item">
                    <button type="button" class="btn btn--medium-square-xs btn--orange js-delete-items" style="display: none;">
                        <i class="fas fa-trash-alt btn__icon"></i>
                        <span class="btn__text btn__text--right xs-hide">Удалить</span>
                    </button>
                </div>
                <div class="panel__actions-item right">
                    <a href="http://spas.loc/users/restore" class="btn btn--medium-square-xs btn--white">
                        <i class="fas fa-trash-restore-alt btn__icon"></i>
                        <span class="btn__text btn__text--right xs-hide">Корзина</span>
                    </a>
                </div>
                <div class="panel__actions-item">
                    <a href="#filter" class="btn btn--white btn--medium-square js-slide-toggle">
                        <i class="fas fa-filter"></i>
                    </a>
                </div>
                <div class="panel__actions-item">
                    <div class="dropdown">
                        <a href="#" class="btn btn--white btn--medium-square dropdown__trigger">
                            <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown__menu dropdown__menu--right">
                            <form action="#" class="dropdown__form form" id="form-fields">
                                <input class="hidden" name="td[]" type="checkbox" value="id" checked="">
                                <label class="form__checkbox">
                                    <input class="form__checkbox-input" name="td[]" type="checkbox" value="avatar" checked="">
                                    <span class="form__checkbox-icon"></span>
                                    <span class="form__checkbox-label">Фото</span>
                                </label>
                                <label class="form__checkbox">
                                    <input class="form__checkbox-input" name="td[]" type="checkbox" value="email" checked="">
                                    <span class="form__checkbox-icon"></span>
                                    <span class="form__checkbox-label">Почта</span>
                                </label>
                                <label class="form__checkbox">
                                    <input class="form__checkbox-input" name="td[]" type="checkbox" value="password">
                                    <span class="form__checkbox-icon"></span>
                                    <span class="form__checkbox-label">Пароль</span>
                                </label>
                                <label class="form__checkbox">
                                    <input class="form__checkbox-input" name="td[]" type="checkbox" value="role_id">
                                    <span class="form__checkbox-icon"></span>
                                    <span class="form__checkbox-label">Роль</span>
                                </label>
                                <label class="form__checkbox">
                                    <input class="form__checkbox-input" name="td[]" type="checkbox" value="is_active" checked="">
                                    <span class="form__checkbox-icon"></span>
                                    <span class="form__checkbox-label">Статус</span>
                                </label>
                                <div class="dropdown__button">
                                    <button class="btn btn--default btn--green btn--full js-fields-save-btn" type="submit" data-entity="users">Применить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel__body">
            <form action="http://spas.loc/users" id="delete-form" method="post" enctype="multipart/form-data">
                <div class="panel__table-wrap">
                    <table class="panel__table">
                        <thead>
                            <tr>
                                <th class="check">
                                    <label class="form__checkbox">
                                        <input class="form__checkbox-input js-checks" type="checkbox">
                                        <span class="form__checkbox-icon"></span>
                                    </label>
                                </th>
                                <th class="id">ID</th>
                                <th class="img">Фото</th>
                                <th class="full">Почта</th>
                                <th class="full">Статус</th>
                                <th>Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr data-id="5" data-model-full-name="App\Models\User">
                                <td class="check">
                                    <label class="form__checkbox">
                                        <input class="form__checkbox-input js-check" value="5" name="ids[]" type="checkbox">
                                        <span class="form__checkbox-icon"></span>
                                    </label>
                                </td>
                                <td class="id">
                                    <a href="http://spas.loc/users/5/edit" class="btn btn--white btn--default">5</a>
                                </td>
                                <td class="img">
                                    <a href="http://spas.loc/users/5/edit">
                                        <img src="http://spas.loc/assets/images/no-photo.jpg" alt="">
                                    </a>
                                </td>
                                <td class="full">
                                    <a href="http://spas.loc/users/5/edit">vf@gn.com</a>
                                </td>
                                <td class="full">
                                    <label class="form__toggle">
                                        <input class="form__toggle-input js-active-toggle" type="checkbox" name="is_active" checked="" disabled>
                                        <span class="form__toggle-icon"></span>
                                    </label>
                                </td>
                                <td>
                                    <button type="button" class="btn btn--orange btn--default-square js-delete-item">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                            <tr data-id="4" data-model-full-name="App\Models\User">
                            <td class="check">
                                <label class="form__checkbox">
                                    <input class="form__checkbox-input js-check" value="4" name="ids[]" type="checkbox">
                                    <span class="form__checkbox-icon"></span>
                                </label>
                            </td>
                            <td class="id">
                                <a href="http://spas.loc/users/4/edit" class="btn btn--white btn--default">4</a>
                            </td>
                            <td class="img">
                                <a href="http://spas.loc/users/4/edit">
                                    <img src="http://spas.loc/assets/images/no-photo.jpg" alt="">
                                </a>
                            </td>
                            <td class="full">
                                <a href="http://spas.loc/users/4/edit">veadev@gmail.cm</a>
                            </td>
                            <td class="full">
                                <label class="form__toggle">
                                    <input class="form__toggle-input js-active-toggle" type="checkbox" name="is_active" checked="" disabled>
                                    <span class="form__toggle-icon"></span>
                                </label>
                            </td>
                            <td>
                                <button type="button" class="btn btn--orange btn--default-square js-delete-item">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
        <div class="panel__footer">
            <ul class="pagination">
                <li>
                    <a class="btn" href="http://spas.loc/users?page=2">«</a>
                </li>
                <li>
                    <a class="btn" href="http://spas.loc/users?page=1">1</a>
                    <a class="btn" href="http://spas.loc/users?page=2">2</a>
                    <span class="btn">3</span>
                    <a class="btn" href="http://spas.loc/users?page=4">4</a>
                </li>
                <li>
                    <a class="btn" href="http://spas.loc/users?page=4">»</a>
                </li>
            </ul>

        </div>


        <h3>Формы</h3>
        <div class="panel__body">
            <div class="panel__content">
                <div class="form__group-wrap">
                    <div class="form__group form__group--input">
                        <h4 class="reset-mt">Кнопки</h4>
                        <div class="row row--ai-center">
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn--large btn--green">Текст</a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn--medium btn--orange">Текст</a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn--default btn--red">Текст</a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn--large btn--blue">Текст</a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn--medium btn--gray">Текст</a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn--default btn--white">Текст</a>
                            </div>
                        </div>
                        <h4>Кнопки с иконками</h4>
                        <div class="row row--ai-center">
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn--large btn--green">
                                    <i class="fas fa-plus"></i>
                                    <span class="btn__text btn__text--right">Текст</span>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn--medium btn--orange">
                                    <i class="fas fa-plus"></i>
                                    <span class="btn__text btn__text--right">Текст</span>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn--default btn--red">
                                    <i class="fas fa-plus"></i>
                                    <span class="btn__text btn__text--right">Текст</span>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn--large btn--blue">
                                    <span class="btn__text btn__text--left">Текст</span>
                                    <i class="fas fa-plus"></i>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn--medium btn--gray">
                                    <span class="btn__text btn__text--left">Текст</span>
                                    <i class="fas fa-plus"></i>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn--default btn--white">
                                    <span class="btn__text btn__text--left">Текст</span>
                                    <i class="fas fa-plus"></i>
                                </a>
                            </div>
                        </div>
                        <h4>Поля форм</h4>
                        <div class="row form__group-wrap">
                            <div class="col-xs-12 col-sm-4">
                                <div class="form__group form__group--input">
                                    <label class="form__label" for="text-1">Label</label>
                                    <input class="form__input form__input--large" type="text" id="text-1" placeholder="Placeholder">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form__group form__group--input">
                                    <label class="form__label" for="select-1">Label</label>
                                    <select class="form__select form__select--large" name="name" id="select-1">
                                        <option value="">Выберите</option>
                                        <option value="1">Вариант 1</option>
                                        <option value="2">Вариант 2</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form__group form__group--input">
                                    <label class="form__label" for="toggle">Label</label>
                                    <div class="form__element form__element--large">
                                        <label class="form__toggle">
                                            <input class="form__toggle-input" id="toggle" type="checkbox" name="toggle">
                                            <span class="form__toggle-icon"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form__group form__group--input">
                                    <label class="form__label form__label--medium" for="text-1">Label</label>
                                    <input class="form__input form__input--medium" type="text" id="text-1" placeholder="Placeholder">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form__group form__group--input">
                                    <label class="form__label form__label--medium" for="select-2">Label</label>
                                    <select class="form__select form__select--medium" name="name" id="select-2">
                                        <option value="">Выберите</option>
                                        <option value="1">Вариант 1</option>
                                        <option value="2">Вариант 2</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4">
                                <div class="form__group form__group--input">
                                    <label class="form__label form__label--medium" for="toggle-2">Label</label>
                                    <div class="form__element form__element--medium">
                                        <label class="form__toggle">
                                            <input class="form__toggle-input" id="toggle-2" type="checkbox" name="toggle">
                                            <span class="form__toggle-icon"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4>Свойства</h4>
                        <div class="form__group-wrap">
                            <div class="row row--small">
                                <div class="col-xs-5 col-sm-5">
                                    <div class="form__group form__group--input">
                                        <input class="form__input form__input--medium form__date-time" type="text" placeholder="Placeholder">
                                    </div>
                                </div>
                                <div class="col-xs-5 col-sm-5">
                                    <div class="form__group form__group--input">
                                        <input class="form__input form__input--medium form__date-time" type="text" placeholder="Placeholder">
                                    </div>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <div class="form__group form__group--input">
                                        <button class="btn btn--medium-square btn--red hidden">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row row--small">
                                <div class="col-xs-5 col-sm-5">
                                    <div class="form__group form__group--input">
                                        <input class="form__input form__input--medium form__date-time" type="text" placeholder="Placeholder">
                                    </div>
                                </div>
                                <div class="col-xs-5 col-sm-5">
                                    <div class="form__group form__group--input">
                                        <input class="form__input form__input--medium form__date-time" type="text" placeholder="Placeholder">
                                    </div>
                                </div>
                                <div class="col-xs-2 col-sm-2">
                                    <div class="form__group form__group--input">
                                        <button class="btn btn--medium-square btn--red">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="form__group form__group--input">
                                <button class="btn btn--medium btn--green">
                                    <i class="fas fa-plus"></i>
                                    <span class="btn__text btn__text--right">Добавить</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends("layouts.app")

@section('title'){{ $titleIndex }}@endsection

@section('body-class'){{ 'animsition' }}@endsection

@section('body-id'){{ '' }}@endsection

@section('content')
    <div class="page-header page-header-bordered">
        <h1 class="page-title">@yield('title')</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="javascript:void(0)">
                    Главная
                </a>
            </li>
            <li class="breadcrumb-item active">
                Список
            </li>
        </ol>
    </div>
    <div class="page-content">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">@yield('title')</h3>
                <div class="page-header-actions">
                    <button type="button" class="btn btn-sm btn-outline btn-danger modal-trigger hide-btn js-delete-items-btn" data-toggle="modal" data-target="#remove">
                        <i class="fa fa-trash" aria-hidden="true"></i>&nbsp;
                        Удалить
                    </button>
                    <a href="javascript:void(0)" class="btn btn-sm btn-outline btn-success" data-toggle="tooltip" data-original-title="Создать">
                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
                        Создать
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive" style="text-align: center;">
                    <form action="" method="post" enctype="multipart/form-data">
                        <table class="table table-bordered" data-role="content" data-plugin="selectable" data-row-selectable="true">
                            <thead class="bg-blue-grey-100">
                            <tr>
                                <th style="width: 50px;">
                                <span class="checkbox-custom checkbox-primary">
                                  <input class="selectable-all js-checkbox-items" type="checkbox">
                                  <label></label>
                                </span>
                                </th>
                                <th>
                                    <div style="text-align: center;">
                                        Фото
                                    </div>
                                </th>
                                <th>
                                    <div style="text-align: center;">
                                        Название
                                    </div>
                                </th>
                                <th>
                                    <div style="text-align: center;">
                                        Дата
                                    </div>
                                </th>
                                <th>
                                    <div style="text-align: center;">
                                        Статус
                                    </div>
                                </th>
                                <th style="width: 150px;">
                                    <div style="text-align: center;">
                                        Действия
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="vertical-align: middle;">
                                <span class="checkbox-custom checkbox-primary">
                                  <input id="item_" value="" name="ids[]" class="selectable-item js-checkbox-item" type="checkbox">
                                  <label for="item_"></label>
                                </span>
                                </td>
                                <td style="vertical-align: middle;">
                                    <img src="{{ asset("assets/images/no_photo.png") }}" alt="" class="img-bordered rounded img-thumbnail" style="width: 100px;">
                                </td>
                                <td style="vertical-align: middle;"></td>
                                <td style="vertical-align: middle;"></td>
                                <td style="vertical-align: middle;">
                                    Активен / Неактивен
                                </td>
                                <td style="text-align: center; vertical-align: middle;">
                                    <div class="page-header-actions" style="top: inherit; right: 47px;">
                                        <a href="" class="btn btn-sm btn-icon btn-warning btn-outline" data-toggle="tooltip" data-original-title="Редактировать">
                                            <i class="icon fa-wrench" aria-hidden="true"></i>
                                        </a>
                                        <a href="" class="btn btn-sm btn-icon btn-warning btn-round btn-outline btn-danger js-delete-item-btn" data-toggle="tooltip" data-original-title="Удалить">
                                            <i class="icon fa-trash" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    Нет ни одного элемента!
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <ul class="pagination">
                            <li class="page-item disabled ">
                                <a class="page-link" href="javascript:void(0)" aria-label="Предыдущая">
                                    <span aria-hidden="true">«</span>
                                </a>
                            </li>
                            <li class="page-item active">
                                <a class="page-link" href="javascript:void(0)">
                                    1
                                    <span class="sr-only">
                                    (current)
                                </span>
                                </a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0)">
                                    2
                                    <span class="sr-only">
                                    (current)
                                </span>
                                </a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0)">
                                    3
                                    <span class="sr-only">
                                    (current)
                                </span>
                                </a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0)">
                                    4
                                    <span class="sr-only">
                                    (current)
                                </span>
                                </a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="javascript:void(0)" aria-label="Следующая">
                                    <span aria-hidden="true">»</span>
                                </a>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="js-remove-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close js-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        Удаление
                    </h4>
                </div>
                <div class="modal-body">
                    Выбранные элементы будут удалены. Вы уверены?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default js-cancel" data-dismiss="modal">
                        Отмена
                    </button>
                    <button type="button" class="btn btn-primary js-submit-delete-form">
                        Удалить
                    </button>
                </div>
            </div><!-- /.modals-content -->
        </div><!-- /.modals-dialog -->
    </div><!-- /.modals -->
@endsection

@push('vendor-styles')@endpush

@push('custom-styles')@endpush

@push('vendor-scripts')@endpush

@push('custom-scripts')@endpush

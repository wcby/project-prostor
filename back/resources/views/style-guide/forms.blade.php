@extends("layouts.app")

@section('title'){{ $titleIndex }}@endsection

@section('body-class'){{ 'animsition' }}@endsection

@section('body-id'){{ '' }}@endsection

@section('content')
    <div class="page-header page-header-bordered">
        <h1 class="page-title">@yield('title')</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="javascript:void(0)">
                    Главная
                </a>
            </li>
            <li class="breadcrumb-item active">
                Создать - редактировать
            </li>
        </ol>
    </div>

    <div class="page-content">
        <div class="panel">
            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
            <div class="panel-heading">
                <h3 class="panel-title">@yield('title')</h3>
                <div class="page-header-actions">
                    <a href="javascript:void(0)" class="btn btn-sm btn-outline btn-warning" data-toggle="tooltip" data-original-title="Назад">
                        <i class="fa fa-reply" aria-hidden="true"></i>
                        Назад
                    </a>
                    <button type="submit" class="btn btn-sm btn-outline btn-success">
                        <i class="fa fa-check" aria-hidden="true"></i>
                        Сохранить
                    </button>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="form-group row">
                        <label class="col-md-2 form-control-label" for="name1">
                            Название
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="name" class="form-control" id="name1" required>
                        </div>
                    </div>

                    <div class="form-group row has-danger">
                        <label class="col-md-2 form-control-label" for="name2">
                            Название
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="name" class="form-control" id="name2" required>
                        </div>
                        <span class="offset-md-2 col-md-10 help-block form-control-label">
                            Ошибка
                        </span>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-2 form-control-label" for="is_active">
                            Статус
                        </label>
                        <div class="col-md-10">
                            <div class="checkbox-custom checkbox-primary">
                                <input id="is_active" checked="checked" name="is_active" type="checkbox">
                                <label></label>
                            </div>
                        </div>
                    </div>

                </div><!-- /.box-body -->
            </div>
            </form>
        </div>
    </div>

@endsection

@push('vendor-styles')@endpush

@push('custom-styles')@endpush

@push('vendor-scripts')@endpush

@push('custom-scripts')@endpush

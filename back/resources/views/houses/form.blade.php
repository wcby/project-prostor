<div class="panel__body">
    <div class="panel__content">
        <div class="row form__group-wrap">
            <div class="col-xs-12">
                <div class="form__group form__group--input">
                    <input type="hidden" name="type_premise_id" value="2">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left"
                                   for="name">{{config('app.entity.'.$entity.'.name')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="text" name="name"
                                   class="form__input form__input--large @error('name'){{ 'is-invalid' }}@enderror"
                                   id="name" value="{{ old('name') ?? $model->name }}" required autofocus>
                        </div>
                        <div class="col-xs-12">
                            @error('name')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="category_premise_id">
                                {{config('app.entity.'.$entity.'.category_premise_id')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <select name="category_premise_id" id="category_premise_id" class="form__select form__select--medium">
                                <option value="">Выбрать</option>
                                @foreach($types as $type)
                                    <option value="{{ $type->id }}" @if ($model->category_premise_id === $type->id)
                                        {{ 'selected' }}
                                        @endif>
                                        {{ $type->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-12">
                            @error('category_premise_id')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left"
                                   for="count">{{config('app.entity.'.$entity.'.count')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="number" name="count"
                                   class="form__input form__input--large @error('count'){{ 'is-invalid' }}@enderror"
                                   id="count" value="{{ old('count') ?? $model->count }}" required autofocus>
                        </div>
                        <div class="col-xs-12">
                            @error('count')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left"
                                   for="additional">{{config('app.entity.'.$entity.'.additional')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <div class="content">
                                <div >
                                    @foreach($additionalsType as $key => $type)
                                        <p>{{$type->name}}</p>
                                        @forelse($type->additionals as $additional)
                                            <div class="wrap_check col-xs-12 col-sm-4">
                                                <input type="checkbox"
                                                       id="additional-{{$additional->id}}"
                                                       name="additionals[]"
                                                       value="{{$additional->id}}"
                                                @if($additionalsChecked){{ in_array($additional->id, $additionalsChecked) ? 'checked' : '' }}@endif
                                                >
                                                <label for="additional-{{$additional->id}}">{{$additional->name}}</label>
                                            </div>
                                        @empty
                                            <span>Нет эдементов</span>
                                        @endforelse
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            @error('additional')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left"
                                   for="price">{{config('app.entity.'.$entity.'.price')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="number" name="price"
                                   class="form__input form__input--large @error('price'){{ 'is-invalid' }}@enderror"
                                   id="price" value="{{ old('price') ?? $model->price }}" required autofocus>
                        </div>
                        <div class="col-xs-12">
                            @error('price')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                @includeIf('partial.images', ['images' => $model->images])
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="is_active">{{config('app.entity.'.$entity.'.is_active')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <label class="form__toggle @error('is_active'){{ 'is-invalid' }}@enderror">
                                <input type="hidden" name="is_active" value="0">
                                <input class="form__toggle-input" type="checkbox" id="is_active" name="is_active" value="1" @if ($model->is_active){{ 'checked' }}@endif>
                                <span class="form__toggle-icon"></span>
                            </label>
                        </div>
                        <div class="col-xs-12">
                            @error('is_active')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@includeIf("partial.meta-tags")

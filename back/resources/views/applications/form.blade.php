<div class="panel__body">
    <div class="panel__content">
        <div class="row form__group-wrap">
            <div class="col-xs-12">
                @if($clients->first())
                    <div class="form__group form__group--input">
                        <div class="row row--small row--ai-center">
                            <div class="col-xs-12 col-sm-2 text-sm-right">
                                <label class="form__label form__label--sm-left"
                                       for="client_id">{{config('app.entity.'.$entity.'.client_id')}}</label>
                            </div>
                            <div class="col-xs-12 col-sm-10">
                                <select name="client_id" id="client_id" class="form__select form__select--medium">
                                    <option value="">Выбрать</option>
                                    @foreach($clients as $client)
                                        <option value="{{ $client->id }}" @if ($model->client_id === $client->id)
                                            {{ 'selected' }}
                                            @endif>
                                            {{ $client->name}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xs-12">
                                @error('client_id')
                                <div class="form__message form__message--error">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                @else
                    <div class="form__group form__group--input">
                        <div class="row row--small row--ai-center">
                            <div class="col-xs-12 col-sm-2 text-sm-right">
                                <label class="form__label form__label--sm-left" for="client_name">Имя клиента</label>
                            </div>
                            <div class="col-xs-12 col-sm-10">
                                <input type="text" name="client[name]"
                                       class="form__input form__input--large"
                                       id="client_name" value="" required autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="form__group form__group--input">
                        <div class="row row--small row--ai-center">
                            <div class="col-xs-12 col-sm-2 text-sm-right">
                                <label class="form__label form__label--sm-left" for="client_phone">Номер клиента</label>
                            </div>
                            <div class="col-xs-12 col-sm-10">
                                <input type="text" name="client[phone]"
                                       class="form__input form__input--large"
                                       id="client_phone" value="" required autofocus>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left"
                                   for="adult">{{config('app.entity.'.$entity.'.adult')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="number" name="adult"
                                   class="form__input form__input--large @error('adult'){{ 'is-invalid' }}@enderror"
                                   id="adult" value="{{ old('adult') ?? $model->adult }}" required autofocus>
                        </div>
                        <div class="col-xs-12">
                            @error('adult')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="child">
                                {{config('app.entity.'.$entity.'.child')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="number" name="child"
                                   class="form__input form__input--large"
                                   id="child" value="{{ old('child') ?? $model->child }}">
                        </div>
                        <div class="col-xs-12">
                            @error('child')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="comment">
                                {{config('app.entity.'.$entity.'.comment')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <textarea class="form__input" name="comment" id="comment" cols="30"
                                      rows="10">
                                {{ old('comment') ?? $model->comment }}
                            </textarea>
                        </div>
                        <div class="col-xs-12">
                            @error('comment')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="total">
                                {{config('app.entity.'.$entity.'.total')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="text" name="total"
                                   class="form__input form__input--large"
                                   id="total" value="{{ old('total') ?? $model->total }}">
                        </div>
                        <div class="col-xs-12">
                            @error('total')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="pay">
                                {{config('app.entity.'.$entity.'.pay')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="text" name="pay"
                                   class="form__input form__input--large"
                                   id="pay" value="{{ old('pay') ?? $model->pay }}">
                        </div>
                        <div class="col-xs-12">
                            @error('pay')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="payment_id">
                                {{config('app.entity.'.$entity.'.payment_id')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input
                                class="form__input form__input--large @error('payment_id'){{ 'is-invalid' }}@enderror"
                                type="text" id="payment_id" name="payment_id"
                                value="{{ old('payment_id') }}" disabled="disabled">
                        </div>
                        <div class="col-xs-12">
                            @error('payment_id')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@includeIf("partial.meta-tags")

<nav class="sidebar__nav">
    <button class="sidebar__close" type="button">Закрыть меню</button>
    <div class="sidebar__title">Панель управления</div>
    <ul class="sidebar__menu">
        @each("partial.sidebar.item", $sidebar, 'item')
        @if (app()->isLocal())
        <li>
            <a href="{{ url("/style-guide") }}">
                <i class="fas fa-toolbox sidebar__menu-icon"></i>
                Руководство по стилю
            </a>
        </li>
        @endif
    </ul>
</nav>

<div class="panel__body">
    <div class="panel__content">
        <div class="row form__group-wrap">
            <div class="col-xs-12">
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="key">{{config('app.entity.'.$entity.'.key')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="text" name="key" class="form__input form__input--large @error('key'){{ 'is-invalid' }}@enderror" id="key" value="{{ old('key') ?? $model->key }}" required autofocus>
                        </div>
                        <div class="col-xs-12">
                            @error('key')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            @if($model->exists)
                <input type="hidden" name="type" value="{{ old('type') ?? $model->type }}">
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="value">{{config('app.entity.'.$entity.'.value')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                        @if($model->type === 'image')
                            @includeIf("partial.{$entity}.image", compact('model'))
                        @elseif($model->type === 'textarea')
                            @includeIf("partial.{$entity}.textarea", compact('model'))
                        @else
                            @includeIf("partial.{$entity}.input", compact('model'))
                        @endif
                        </div>
                        <div class="col-xs-12">
                        @error('value')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>

            @else
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="type">{{config('app.entity.'.$entity.'.type')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <select name="type" id="type" class="form__select form__select--large @error('type'){{ 'is-invalid' }}@enderror">
                                <option value="">{{config('app.entity.common.select')}}</option>
                                @foreach ($types as $typeKey => $typeName)
                                <option value="{{ $typeKey }}" @if ((string) $model->type === (string) $typeKey || (string) $typeKey === (string) old('type')){{ 'selected' }}@endif>{{ $typeName }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-12">
                        @error('type')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
            @endif
            </div>
        </div>
    </div>
</div>

<div class="panel__body">
    <div class="panel__content">
        <div class="row form__group-wrap">
            <div class="col-xs-12">
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left"
                                   for="name">{{config('app.entity.'.$entity.'.name')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <input type="text" name="name"
                                   class="form__input form__input--large @error('name'){{ 'is-invalid' }}@enderror"
                                   id="name" value="{{ old('name') ?? $model->name }}" required autofocus>
                        </div>
                        <div class="col-xs-12">
                            @error('name')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="icon">
                                {{config('app.entity.'.$entity.'.icon')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <select name="icon" id="icon" class="form__select form__select--medium" required>
                                <option value="">Выбрать</option>
                                @foreach($icons as $key => $icon)
                                    <option value="{{ $key }}" @if ($model->icon === $key)
                                        {{ 'selected' }}
                                        @endif>
                                        {{ $icon }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-12">
                            @error('icon')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="additional_type_id">
                                {{config('app.entity.'.$entity.'.additional_type_id')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <select name="additional_type_id" id="additional_type_id" class="form__select form__select--medium" required>
                                <option value="">Выбрать</option>
                                @foreach($types as $type)
                                    <option value="{{ $type->id }}" @if ($model->additional_type_id === $type->id)
                                        {{ 'selected' }}
                                        @endif>
                                        {{ $type->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-xs-12">
                            @error('additional_type_id')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="is_active">{{config('app.entity.'.$entity.'.is_active')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <label class="form__toggle @error('is_active'){{ 'is-invalid' }}@enderror">
                                <input type="hidden" name="is_active" value="0">
                                <input class="form__toggle-input" type="checkbox" id="is_active" name="is_active" value="1" @if ($model->is_active){{ 'checked' }}@endif>
                                <span class="form__toggle-icon"></span>
                            </label>
                        </div>
                        <div class="col-xs-12">
                            @error('is_active')
                            <div class="form__message form__message--error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@includeIf("partial.meta-tags")

<div class="row row--jc-space-between">
    <div class="col-xs-12 col-sm-6">© {{ now()->year }} Административное приложение</div>
    <div class="col-xs-12 col-sm-6 text-sm-right">
        Разработано: <a href="https://dalab.ru">Dalab.ru</a>
    </div>
</div>

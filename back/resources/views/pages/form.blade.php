<div class="panel__body">
    <div class="panel__content">
        <div class="row form__group-wrap">
            <div class="col-xs-12">
                @include('partial.fields.input', [
                                            'type' => 'text',
                                            'field_name' => 'name',
                                            'field_value' => $model->name,
                                            'required' => true,
                                            'autofocus' => true
                                            ]
                                            )
                @include('partial.fields.input', [
                                            'type' => 'text',
                                            'field_name' => 'slug',
                                            'field_value' => $model->url,
                                            'required' => true
                                            ])

                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="description">
                                {{config('app.entity.'.$entity.'.description')}}
                            </label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <textarea name="description" class="form__tinymce @error('description'){{ 'is-invalid' }}@enderror" id="description">{{ old('description') ?? $model->description }}</textarea>
                        </div>
                        <div class="col-xs-12">
                        @error('description')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
                <div class="form__group form__group--input">
                    <div class="row row--small row--ai-center">
                        <div class="col-xs-12 col-sm-2 text-sm-right">
                            <label class="form__label form__label--sm-left" for="is_active">{{config('app.entity.'.$entity.'.is_active')}}</label>
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <label class="form__toggle @error('is_active'){{ 'is-invalid' }}@enderror">
                                <input type="hidden" name="is_active" value="0">
                                <input class="form__toggle-input" type="checkbox" id="is_active" name="is_active" value="1" @if ($model->is_active){{ 'checked' }}@endif>
                                <span class="form__toggle-icon"></span>
                            </label>
                        </div>
                        <div class="col-xs-12">
                        @error('is_active')
                            <div class="form__message form__message--error">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@includeIf("partial.meta-tags")

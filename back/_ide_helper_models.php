<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Article
 *
 * @property int $id
 * @property string $url Url
 * @property string $date Дата
 * @property string|null $photo Фото
 * @property int|null $is_favourite Избранное
 * @property int|null $is_active Статус
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ArticleI18n[] $entities
 * @property-read int|null $entities_count
 * @property-read mixed $brief_description
 * @property-read mixed $column_sorting
 * @property-read mixed $description
 * @property-read mixed $direction_sorting
 * @property-read mixed $meta_description
 * @property-read mixed $meta_keywords
 * @property-read mixed $meta_title
 * @property-read mixed $name
 * @property-read int $total_records
 * @property-read \App\Models\MetaTag|null $metaTags
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultModel active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Article onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereIsFavourite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Article whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Article withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Article withoutTrashed()
 */
	class Article extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ArticleI18n
 *
 * @property int $id
 * @property int $entity_id ID основной таблицы
 * @property string $locale Локаль
 * @property string|null $name Название
 * @property string|null $brief_description Краткое описание
 * @property string|null $description Описание
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleI18n active()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleI18n newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleI18n newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleI18n order()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleI18n orderDesc()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleI18n query()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleI18n whereBriefDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleI18n whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleI18n whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleI18n whereEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleI18n whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleI18n whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleI18n whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleI18n whereUpdatedAt($value)
 */
	class ArticleI18n extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Banner
 *
 * @property int $id
 * @property string $url Url ссылки
 * @property string|null $photo Картинка баннера
 * @property int|null $position Порядок показа
 * @property int|null $is_active Активность баннера
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BannerI18n[] $entities
 * @property-read int|null $entities_count
 * @property-read mixed $column_sorting
 * @property-read mixed $direction_sorting
 * @property-read mixed $title
 * @property-read int $total_records
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultModel active()
 * @method static \Illuminate\Database\Eloquent\Builder|Banner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Banner newQuery()
 * @method static \Illuminate\Database\Query\Builder|Banner onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Banner query()
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Banner whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|Banner withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Banner withoutTrashed()
 */
	class Banner extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\BannerI18n
 *
 * @property int $id
 * @property int $entity_id ID основной таблицы
 * @property string $locale Локаль
 * @property string|null $title Заголовок
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|BannerI18n active()
 * @method static \Illuminate\Database\Eloquent\Builder|BannerI18n newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BannerI18n newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BannerI18n order()
 * @method static \Illuminate\Database\Eloquent\Builder|BannerI18n orderDesc()
 * @method static \Illuminate\Database\Eloquent\Builder|BannerI18n query()
 * @method static \Illuminate\Database\Eloquent\Builder|BannerI18n whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerI18n whereEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerI18n whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerI18n whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerI18n whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerI18n whereUpdatedAt($value)
 */
	class BannerI18n extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Category
 *
 * @property int $id
 * @property int|null $is_active Статус
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CategoryI18n[] $entities
 * @property-read int|null $entities_count
 * @property-read mixed $column_sorting
 * @property-read mixed $direction_sorting
 * @property-read mixed $name
 * @property-read int $total_records
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultModel active()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Query\Builder|Category onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Category withoutTrashed()
 */
	class Category extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\CategoryI18n
 *
 * @property int $id
 * @property int $entity_id ID основной таблицы
 * @property string $locale Локаль
 * @property string|null $name Название
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryI18n active()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryI18n newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryI18n newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryI18n order()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryI18n orderDesc()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryI18n query()
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryI18n whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryI18n whereEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryI18n whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryI18n whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryI18n whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CategoryI18n whereUpdatedAt($value)
 */
	class CategoryI18n extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\DefaultModel
 *
 * @property-read mixed $column_sorting
 * @property-read mixed $direction_sorting
 * @property-read int $total_records
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultModel active()
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultModel query()
 */
	class DefaultModel extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\DefaultUserModel
 *
 * @property-read mixed $column_sorting
 * @property-read mixed $direction_sorting
 * @property-read mixed $total_records
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultUserModel active()
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultUserModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultUserModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultUserModel query()
 */
	class DefaultUserModel extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Image
 *
 * @property int $id
 * @property string $image_type
 * @property int $image_id
 * @property string $path
 * @property int $position
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $image
 * @method static \Illuminate\Database\Eloquent\Builder|Image newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Image newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Image query()
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereImageType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereUpdatedAt($value)
 */
	class Image extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Language
 *
 * @property int $id
 * @property string $locale Языковой стандарт
 * @property string|null $name Наименование языка
 * @property string|null $code Код языка
 * @property int|null $is_default Язык по умолчанию
 * @property int|null $is_active Статус языка (активный/неактивный)
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read mixed $column_sorting
 * @property-read mixed $direction_sorting
 * @property-read int $total_records
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultModel active()
 * @method static \Illuminate\Database\Eloquent\Builder|Language newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Language newQuery()
 * @method static \Illuminate\Database\Query\Builder|Language onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Language query()
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Language whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Language withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Language withoutTrashed()
 */
	class Language extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Menu
 *
 * @property int $id
 * @property string|null $name Наименование меню
 * @property string $alias Техническое наименование меню
 * @property int|null $is_active Активность меню
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read mixed $column_sorting
 * @property-read mixed $direction_sorting
 * @property-read int $total_records
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MenuItem[] $menuItem
 * @property-read int|null $menu_item_count
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultModel active()
 * @method static \Illuminate\Database\Eloquent\Builder|Menu newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Menu newQuery()
 * @method static \Illuminate\Database\Query\Builder|Menu onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Menu query()
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Menu whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Menu withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Menu withoutTrashed()
 */
	class Menu extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\MenuItem
 *
 * @property int $id
 * @property int|null $menu_id ID меню
 * @property string $url Url подменю
 * @property int|null $parent_id Родитель подменю
 * @property int|null $position Позиция подменю
 * @property int|null $is_active Активность подменю
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|MenuItem[] $children
 * @property-read int|null $children_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MenuItemI18n[] $entities
 * @property-read int|null $entities_count
 * @property-read mixed $column_sorting
 * @property-read mixed $direction_sorting
 * @property-read mixed $name
 * @property-read int $total_records
 * @property-read MenuItem|null $parent
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultModel active()
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem newQuery()
 * @method static \Illuminate\Database\Query\Builder|MenuItem onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereMenuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItem whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|MenuItem withTrashed()
 * @method static \Illuminate\Database\Query\Builder|MenuItem withoutTrashed()
 */
	class MenuItem extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\MenuItemI18n
 *
 * @property int $id
 * @property int $entity_id ID основной таблицы
 * @property string $locale Локаль
 * @property string|null $name Наименование подменю
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItemI18n active()
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItemI18n newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItemI18n newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItemI18n order()
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItemI18n orderDesc()
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItemI18n query()
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItemI18n whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItemI18n whereEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItemI18n whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItemI18n whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItemI18n whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MenuItemI18n whereUpdatedAt($value)
 */
	class MenuItemI18n extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\MetaTag
 *
 * @property int $id
 * @property string $meta_type
 * @property int $meta_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MetaTagI18n[] $entities
 * @property-read int|null $entities_count
 * @property-read mixed $column_sorting
 * @property-read mixed $direction_sorting
 * @property-read mixed $meta_description
 * @property-read mixed $meta_keywords
 * @property-read mixed $meta_title
 * @property-read int $total_records
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $meta
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultModel active()
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTag newQuery()
 * @method static \Illuminate\Database\Query\Builder|MetaTag onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTag query()
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTag whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTag whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTag whereMetaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTag whereMetaType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTag whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|MetaTag withTrashed()
 * @method static \Illuminate\Database\Query\Builder|MetaTag withoutTrashed()
 */
	class MetaTag extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\MetaTagI18n
 *
 * @property int $id
 * @property int $entity_id ID основной таблицы
 * @property string $locale Локаль
 * @property string|null $meta_title Мета тэг title
 * @property string|null $meta_keywords Мета тэг keywords
 * @property string|null $meta_description Мета тэг description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTagI18n active()
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTagI18n newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTagI18n newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTagI18n order()
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTagI18n orderDesc()
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTagI18n query()
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTagI18n whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTagI18n whereEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTagI18n whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTagI18n whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTagI18n whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTagI18n whereMetaKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTagI18n whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MetaTagI18n whereUpdatedAt($value)
 */
	class MetaTagI18n extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Page
 *
 * @property int $id
 * @property string $url Url
 * @property int|null $is_active Статус
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PageI18n[] $entities
 * @property-read int|null $entities_count
 * @property-read mixed $column_sorting
 * @property-read mixed $description
 * @property-read mixed $direction_sorting
 * @property-read mixed $meta_description
 * @property-read mixed $meta_keywords
 * @property-read mixed $meta_title
 * @property-read mixed $name
 * @property-read int $total_records
 * @property-read \App\Models\MetaTag|null $metaTags
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultModel active()
 * @method static \Illuminate\Database\Eloquent\Builder|Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Page newQuery()
 * @method static \Illuminate\Database\Query\Builder|Page onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Page whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|Page withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Page withoutTrashed()
 */
	class Page extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PageI18n
 *
 * @property int $id
 * @property int $entity_id ID основной таблицы
 * @property string $locale Локаль
 * @property string|null $name Название
 * @property string|null $description Описание
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PageI18n active()
 * @method static \Illuminate\Database\Eloquent\Builder|PageI18n newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageI18n newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PageI18n order()
 * @method static \Illuminate\Database\Eloquent\Builder|PageI18n orderDesc()
 * @method static \Illuminate\Database\Eloquent\Builder|PageI18n query()
 * @method static \Illuminate\Database\Eloquent\Builder|PageI18n whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageI18n whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageI18n whereEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageI18n whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageI18n whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageI18n whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PageI18n whereUpdatedAt($value)
 */
	class PageI18n extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Permission
 *
 * @property int $id
 * @property string $type
 * @property string $group
 * @property string $action
 * @property string|null $name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read mixed $column_sorting
 * @property-read mixed $direction_sorting
 * @property-read int $total_records
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|Permission active()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission newQuery()
 * @method static \Illuminate\Database\Query\Builder|Permission onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission order()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission orderDesc()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Permission withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Permission withoutTrashed()
 */
	class Permission extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Role
 *
 * @property int $id
 * @property string $name
 * @property string $alias
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read mixed $column_sorting
 * @property-read mixed $direction_sorting
 * @property-read int $total_records
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultModel active()
 * @method static \Illuminate\Database\Eloquent\Builder|Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role newQuery()
 * @method static \Illuminate\Database\Query\Builder|Role onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereAlias($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Role withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Role withoutTrashed()
 */
	class Role extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Setting
 *
 * @property int $id
 * @property string $type Тип поля
 * @property string $key Ключ
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SettingI18n[] $entities
 * @property-read int|null $entities_count
 * @property-read mixed $column_sorting
 * @property-read mixed $direction_sorting
 * @property-read int $total_records
 * @property-read mixed $value
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultModel active()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting newQuery()
 * @method static \Illuminate\Database\Query\Builder|Setting onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting query()
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Setting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Setting withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Setting withoutTrashed()
 */
	class Setting extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SettingI18n
 *
 * @property int $id
 * @property int $entity_id ID основной таблицы
 * @property string $locale Локаль
 * @property string|null $value Значение
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SettingI18n active()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingI18n newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingI18n newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingI18n order()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingI18n orderDesc()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingI18n query()
 * @method static \Illuminate\Database\Eloquent\Builder|SettingI18n whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingI18n whereEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingI18n whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingI18n whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingI18n whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SettingI18n whereValue($value)
 */
	class SettingI18n extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string|null $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $phone
 * @property string $password
 * @property string|null $avatar
 * @property int $role_id
 * @property int $is_active
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read mixed $column_sorting
 * @property-read mixed $direction_sorting
 * @property-read mixed $total_records
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\Role|null $role
 * @method static \Illuminate\Database\Eloquent\Builder|DefaultUserModel active()
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 */
	class User extends \Eloquent {}
}

